angular.module('webui.services.settings', [])
.value('$fileSettings', {
  "dir": {
    val: '',
    desc: "The directory to store the downloaded file."
  },
  "header": {
    val: '',
    desc: "Append HEADER to HTTP request header.",
    multiline: true,
  },
  "http-user": {
    val: '',
    desc: "Set HTTP username.",
  },
  "http-passwd": {
    val: '',
    desc: "Set HTTP password.",
  }
})
.value('$globalSettings', {
  "max-concurrent-downloads": {
    val: 5,
    desc: "Set maximum number of parallel downloads for every static (HTTP/FTP) URI, torrent and metalink. See also --split option. Default: 5"
  },
  "max-overall-download-limit": {
    val: '0',
    desc: "Set max overall download speed in bytes/sec. 0 means unrestricted. You can append K or M (1K = 1024, 1M = 1024K). To limit the download speed per download, use --max-download-limit option. Default: 0."
  }
});