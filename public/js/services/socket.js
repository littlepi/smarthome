angular.module('webui.services.socket', [])
.factory('$socket', [function () {
  var socket = null;
  return {
    connect: function(url, opts) {
      if (socket !== null) {
        socket.disconnect(true);
      }
      socket = io.connect(url, opts);
      tt = socket;
    },
    on: function(eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        callback.apply(socket, args);
      });
    },
    emit: function(eventName, data, callback) {
      //TODO, consider message ack
      socket.emit(eventName, data, function () {
        var args = arguments;
        if (callback) {
          callback.apply(socket, args);
        }
      });
    },
    disconnect: function() {
      socket.disconnect(true);
    },
    connected: function() {
      return socket!==null && socket.connected;
    },
    once: function(eventName, callback) {
      socket.once(eventName, function () {  
        var args = arguments;
        callback.apply(socket, args);
      });
    },
  };
}]);
