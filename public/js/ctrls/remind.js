var parseRule = function(rule) {
	if (rule === null || rule.trim() === "") {
		return null;
	}
	var r = rule.split(/[ ]+/);
	if (r.length!=7) {
		return null;
	}
	var rrule = {};
	rrule.bysecond = parseInt(r[0]);
	
	//MINUTELY
	if (r[1].indexOf('\/') == -1 && r[1].trim()!=="*") {
		rrule.byminute = parseInt(r[1]);
	}
	else if (r[1].trim()!=="*"){
		rrule.freq = RRule.MINUTELY;
		rrule.interval = parseInt(r[1].substr(r[1].indexOf('\/')+1));
		return rrule;
	}
	
	//HOURLY
	if (r[2].indexOf('\/') == -1 && r[2].trim()!=="*") {
		rrule.byhour = parseInt(r[2]);
	}
	else if (r[2].trim()!=="*"){
		rrule.freq = RRule.HOURLY;
		rrule.interval = parseInt(r[2].substr(r[2].indexOf('\/')+1));
		return rrule;
	}
	
	//DAILY
	if (r[3].indexOf('\/') == -1 && r[3].trim()!=="*") {
		rrule.bymonthday = parseInt(r[3]);
	}
	else if (r[3].trim()!=="*"){
		rrule.freq = RRule.DAILY;
		rrule.interval = parseInt(r[3].substr(r[3].indexOf('\/')+1));
		return rrule;
	}
	
	//MONTHLY
	if (r[4].indexOf('\/') == -1 && r[4].trim()!=="*") {
		rrule.bymonth = parseInt(r[4]);
	}
	else if (r[4].trim()!=="*"){
		rrule.freq = RRule.MONTHLY;
		rrule.interval = parseInt(r[4].substr(r[4].indexOf('\/')+1));
		return rrule;
	}
	
	//WEEKLY
	if (r[5].trim() == "MON-FRI") {
		rrule.byweekday = [RRule.MO, RRule.TU, RRule.WE, RRule.TH, RRule.FR];
		rrule.freq = RRule.WEEKLY;
		rrule.interval = 1;
		return rrule;
	}
	else if (r[5].trim() !== "*") {
		var byweekday = [];
		r[5].split(",").forEach(function(d){
			switch (d.trim()) {
				case "MON":
					byweekday.push(RRule.MO);
					break;
				case "TUE":
					byweekday.push(RRule.TU);
					break;
				case "WED":
					byweekday.push(RRule.WE);
					break;
				case "THU":
					byweekday.push(RRule.TH);
					break;
				case "FRI":
					byweekday.push(RRule.FR);
					break;
				case "SAT":
					byweekday.push(RRule.SA);
					break;
				case "SUN":
					byweekday.push(RRule.SU);
					break;
				default:
					break;
			}
		});
		rrule.byweekday = byweekday;
		rrule.freq = RRule.WEEKLY;
		rrule.interval = 1;
		return rrule;
	}
	
	//YEARLY
	if (r[6].indexOf('\/') == -1 && r[6].trim()!=="*") {
		//rrule.bymonth = parseInt(r[6]);
	}
	else if (r[6].trim()!=="*"){
		rrule.freq = RRule.YEARLY;
		rrule.interval = parseInt(r[6].substr(r[6].indexOf('\/')+1));
		return rrule;
	}
	
	return null;
};

angular
.module('webui.ctrls.remind', [
	'webui.services.configuration', 'webui.services.modals',
	'webui.services.rpc', 'webui.services.rpc.helpers',
	'webui.services.socket'
])
.controller('RemindCtrl', [
	'$rootScope',
	'$scope', '$modals',
	'$rpc', '$rpchelpers',
	'$socket', 'calendarConfig',
function(
	rootScope,
	scope, modals,
	rpc, rhelpers,
	socket, calendarConfig
) {
	scope.addRemind = function() {
		modals.invoke(
			'remind', _.bind(scope.saveRemind, scope)
		);
	};
	
	scope.saveRemind = function(start, end, rule, job) {
		// TODO, form validate
		var target = "job";
		var d = {s: start, e: end, r: rule, j: job};
		socket.emit("i", {t: target, d: d});
		
		// TODO, move this part out.
		socket.once("job.add", function(data) {
			console.log(data);
			data = data.d;
			var event = {
				title: JSON.parse(data.j).d.text,
				startsAt: new Date(parseInt(data.s)*1000),
				//endsAt: new Date((parseInt(start)+3600)*1000), // by default, event will last for one hour.
				color: calendarConfig.colorTypes.info
			};
			var rrule = parseRule(data.r);
			if (rrule!==null) {
				rrule.dtstart = new Date(parseInt(data.s)*1000);
				if (data.e !== null && data.e.trim()!=="") {
					rrule.until = new Date(parseInt(end)*1000);
				}
				event.rrule = rrule;
			}
			else { // non repeat event, only occur once.
				//event.rrule = {freq: RRule.MINUTELY, until: event.startsAt, interval: 1, bysecond: 0}; //TODO remove?
			}
			rootScope.p.events.push(event);
			scope.applyEvents();
		});
	};
	
	scope.applyEvents = function() {
		scope.events = [];
		rootScope.p.events.forEach(function(event) {
			console.log(event);
			if (event.hasOwnProperty("rrule") && event.rrule.freq !== RRule.MINUTELY) {
				var rule = new RRule(angular.extend({}, event.rrule, {}));
				rule.between(moment(scope.viewDate).startOf(scope.calendarView).toDate(), moment(scope.viewDate).endOf(scope.calendarView).toDate()).forEach(function(date) {
					scope.events.push(angular.extend({}, event, {
						startsAt: new Date(date)
					}));
				});
			}
			else if (!event.hasOwnProperty("rrule")) {
				scope.events.push(event);
			}
		});
	};
	
	
	/* calendar */
	scope.$watchGroup([
		'calendarView',
		'viewDate'
	], scope.applyEvents);
	
	calendarConfig.showTimesOnWeekView = true;
	//calendarConfig.dateFormatter = 'angular';

	//scope.events = [];
	scope.calendarView = 'month';
	scope.viewDate = moment().startOf('month').toDate();
	scope.cellIsOpen = true;
	
}]);
