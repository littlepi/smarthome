var parseFiles = function(files, cb) {
  var cnt = 0;
  var txts = [];
  var onload = function(res) {
    var txt = res.target.result;
    txts.push(txt.split(',')[1]);
    cnt--;
    if (!cnt) {
      cb(txts);
    }
  };
  _.each(files, function(file) {
    cnt++;
    console.log('starting file reader');
    var reader = new FileReader();
    reader.onload = onload;
    reader.onerror = function(err) {
      // return error
      // TODO: find a better way to propogate error upstream
      console.log('got back error', err);
      cb([]);
    };
    reader.readAsDataURL(file);
  });
};

angular
.module('webui.ctrls.modal', [
  'ui.bootstrap',
  'webui.services.deps', 'webui.services.modals', 'webui.services.rpc',
  'webui.services.configuration'
])
.controller('ModalCtrl', [
  '$_', '$scope', '$uibModal', "$modals", '$rpc','$fileSettings', '$downloadProps',
  function(_, scope, $uibModal, modals, rpc, fsettings, dprops) {

  scope.getUris = {
    open: function(cb) {
      var self = this;
      this.uris = "";
      this.downloadSettingsCollapsed = true;
      //this.advancedSettingsCollapsed = true;
      this.settings = {};
      this.fsettings = _.cloneDeep(fsettings);
      this.cb = cb;
      
      // fill in default download properties
      _.forEach(dprops, function(p) {
        self.settings[p] = self.fsettings[p];
        delete self.fsettings[p];
      });

      this.inst = $uibModal.open({
        templateUrl: "getUris.html",
        scope: scope,
        size: "lg"
      });
      this.inst.result.then(function() {
        delete self.inst;
        if (self.cb) {
          var settings = {};
          // no need to send in default values, just the changed ones
          for (var i in self.settings) {
            if (fsettings[i].val != self.settings[i].val)
                settings[i] = self.settings[i].val;
          }
          for (var i in self.fsettings) {
            if (fsettings[i].val != self.fsettings[i].val)
                settings[i] = self.fsettings[i].val;
          }

          console.log('sending settings:', settings);
          self.cb(self.parse(), settings);
        }
      },
      function() {
        delete self.inst;
      });
    },
    parse: function() {
      return _
        .chain(this.uris.trim().split(/\r?\n/g))
        .map(function(d) { return d.trim().split(/\s+/g) })
        .filter(function(d) { return d.length })
        .value();
    }
  };

  scope.selectFiles = {
    open: function(files, cb) {
      var self = this;
      this.files = _.cloneDeep(files);
      this.inst = $uibModal.open({
        templateUrl: "selectFiles.html",
        scope: scope,
        size: "lg"
      });

      this.inst.result.then(function() {
        delete self.inst;

        if (cb) {
          cb(self.files);
        }
      },
      function() {
        delete self.inst;
      });
    }
  };

  _.each(['getTorrents', 'getMetalinks'], function(name) {
    scope[name] = {
      open: function(cb) {
        var self = this;
        this.files = [];
        this.collapsed = true;
        this.settings = {};
        this.fsettings = _.cloneDeep(fsettings);

        // fill in default download properties
        _.forEach(dprops, function(p) {
          self.settings[p] = self.fsettings[p];
          delete self.fsettings[p];
        });

        this.inst = $uibModal.open({
          templateUrl: name + ".html",
          scope: scope,
          size: "lg",
        });

        this.inst.result.then(function() {
          delete self.inst;
          if (cb) {
            parseFiles(self.files, function(txts) {
              var settings = {};

              // no need to send in default values, just the changed ones
              for (var i in self.settings) {
                if (fsettings[i].val != self.settings[i].val)
                    settings[i] = self.settings[i].val;
              }
              for (var i in self.fsettings) {
                if (fsettings[i].val != self.fsettings[i].val)
                    settings[i] = self.fsettings[i].val;
              }
              cb(txts, settings);
            });
          }
        },
        function() {
          delete self.inst;
        });
      }
    };
  });
  
  scope.remind = {
    open: function(cb) {
      var self = this;
      
      this.dtStart = new Date(new Date().setSeconds(0)); //set second=0
      this.dtEnd = null;
      this.cronExpression = '0 0 1 */1 * * *';
      this.cronOptions = {
        cronFormat: 'quartz'
      };
      this.isCronDisabled = false;
      
      this.cronText = "";
      this.isRepeat = false;
      //this.activeTab = "daily";
      this.endRepeat = false;
      
      this.inst = $uibModal.open({
        templateUrl: "addRemind.html",
        scope: scope,
        size: "lg",
      });
      this.inst.result.then(function() {
        delete self.inst;
        if (cb) {
          // TODO, do validate
          var start = parseInt(self.dtStart.getTime()/1000).toString();
          var end = null;
          var rule = self.cronExpression;
          if (self.dtEnd!==null && self.isRepeat && self.endRepeat) {
             end = parseInt(self.dtEnd.getTime()/1000).toString();
          }
          if (!self.isRepeat) {
            rule = null; //TODO, fix bug, if rule is null, event repeat every minute in node-schedule!
          }
          var job = JSON.stringify({id: start, c: [0, 2], d: {text: self.cronText}});
          cb(start, end, rule, job);
        }
      },
      function() {
        delete self.inst;
      });
    }
  };

  _.each([
    'getUris', 'getTorrents', 'getMetalinks', 'selectFiles',
    'remind'
  ], function(name) {
    modals.register(name, function() {
      if (scope[name].inst) {
        // Already open.
        return;
      }
      var args = Array.prototype.slice.call(arguments, 0);
      scope[name].open.apply(scope[name], args);
    });
  });

}]);
