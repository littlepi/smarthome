angular
.module('webui.ctrls.download', [
	'webui.services.configuration', 'webui.services.modals',
	'webui.services.rpc', 'webui.services.rpc.helpers',
	'webui.services.utils'
])
.controller('DownloadCtrl', [
	'$scope', '$modals',
	'$rpc', '$rpchelpers',
	'$utils',
	function(
		scope, modals,
		rpc, rhelpers,
		utils
	) {

	scope.isFeatureEnabled = function(f) { return rhelpers.isFeatureEnabled(f) };

	
	// total number of downloads, updates dynamically as downloads are
	// stored in scope
	scope.totalDownloads = 0;
	
	scope.downloadFilter = "";
	scope.downloadFilterCommitted = "";
	
	scope.onDownloadFilter = function() {
		if (scope.downloadFilterTimer) {
			clearTimeout(scope.downloadFilterTimer);
		}
		scope.downloadFilterTimer = setTimeout(function() {
			delete scope.downloadFilterTimer;
			if (scope.downloadFilterCommitted !== scope.downloadFilter) {
				scope.downloadFilterCommitted = scope.downloadFilter;
				scope.$digest();
			}
		}, 500);
	};
	
	
	// actual downloads used by the view
	scope.getDownloads = function() {
		var downloads = [];
		if (scope.filterActive) {
			if (!scope.filterSpeed) {
				downloads = _.filter(scope.active, function (e) {
					return !+e.uploadSpeed && !+e.downloadSpeed;
				});
			}
			else {
				downloads = scope.active;
			}
		}
		else if (scope.filterSpeed) {
			downloads = _.filter(scope.active, function (e) {
				return +e.uploadSpeed || +e.downloadSpeed;
			});
		}
		if (scope.filterWaiting) {
			downloads = downloads.concat(_.filter(scope.waiting, function (e) {
				return e.status == "waiting";
			}));
		}
		if (scope.filterPaused) {
			downloads = downloads.concat(_.filter(scope.waiting, function (e) {
				return e.status == "paused";
			}));
		}
		if (scope.filterError) {
			downloads = downloads.concat(_.filter(scope.stopped, function (e) {
				return e.status == "error";
			}));
		}
		if (scope.filterComplete) {
			downloads = downloads.concat(_.filter(scope.stopped, function (e) {
				return e.status == "complete";
			}));
		}
		if (scope.filterRemoved) {
			downloads = downloads.concat(_.filter(scope.stopped, function (e) {
				return e.status == "removed";
			}));
		}

		downloads = scope.filterDownloads(downloads);

		scope.totalDownloads = downloads.length;

		downloads = downloads.slice( (scope.currentPage - 1) * scope.pageSize );
		downloads.splice( scope.pageSize );

		return downloads;
	};
	
	scope.filterDownloads = function(downloads) {
		if (!scope.downloadFilterCommitted) {
			return downloads;
		}
		var filter = scope.downloadFilterCommitted.
			replace(/[{}()\[\]\\^$.?]/g, "\\$&").
			replace(/\*/g, ".*").
			replace(/\./g, ".");
		filter = new RegExp(filter, "i");
		return _.filter(downloads, function(d) {
			if (filter.test(d.name)) return true;
			return _.filter(d.files, function(f) {
				return filter.test(f.relpath);
			}).length;
		});
	};

	scope.clearFilter = function() {
		scope.downloadFilter = scope.downloadFilterCommitted = "";
	};

	scope.resetFilters = function() {
		scope.filterSpeed =
			scope.filterActive =
			scope.filterWaiting =
			scope.filterComplete =
			scope.filterError =
			scope.filterPaused =
			scope.filterRemoved =
			true;
		scope.clearFilter();
		scope.persistFilters();
	};
	
	scope.runningFilters = function() {
		scope.filterSpeed =
			scope.filterActive =
			scope.filterWaiting =
			scope.filterError =
			scope.filterPaused =
			scope.filterRemoved =
			true;
		scope.filterComplete = false;
		scope.filterError = false;
		scope.clearFilter();
		scope.persistFilters();
	};

	scope.completeFilters = function() {
		scope.filterSpeed =
			scope.filterActive =
			scope.filterWaiting =
			scope.filterPaused =
			scope.filterRemoved =
			false;
		scope.filterComplete = true;
		scope.filterError = true;
		scope.clearFilter();
		scope.persistFilters();
	};

	scope.persistFilters = function() {
		var o = JSON.stringify({
			s: scope.filterSpeed,
			a: scope.filterActive,
			w: scope.filterWaiting,
			c: scope.filterComplete,
			e: scope.filterError,
			p: scope.filterPaused,
			r: scope.filterRemoved
		});
		utils.setCookie("aria2filters", o);
	};

	scope.loadFilters = function() {
		var o = JSON.parse(utils.getCookie("aria2filters"));
		if (!o) {
			scope.resetFilters();
			return;
		}
		scope.filterSpeed = !!o.s;
		scope.filterActive = !!o.a;
		scope.filterWaiting = !!o.w;
		scope.filterComplete = !!o.c;
		scope.filterError = !!o.e;
		scope.filterPaused = !!o.p;
		scope.filterRemoved = !!o.r;
	};

	scope.loadFilters();

	// manage download functions
	scope.forcePauseAll = function() {
		rpc.once('forcePauseAll', []);
	};

	scope.purgeDownloadResult = function() {
		rpc.once('purgeDownloadResult', []);
	};

	scope.unpauseAll = function() {
		rpc.once('unpauseAll', []);
	};
	
	scope.addUris = function() {
		modals.invoke(
			'getUris', _.bind(rhelpers.addUris, rhelpers)
		);
	};

	scope.addMetalinks = function() {
		modals.invoke(
			'getMetalinks', _.bind(rhelpers.addMetalinks, rhelpers)
		);
	};

	scope.addTorrents = function() {
		modals.invoke(
			'getTorrents', _.bind(rhelpers.addTorrents, rhelpers)
		);
	};
	
	// judge action button show or not
	scope.activeCount = function() {
		return scope.$parent.active.length;
	};
	scope.waitingCount = function() {
		return scope.$parent.waiting.length;
	};
	scope.stoppedCount = function() {
		return scope.$parent.stopped.length;
	};


	//scope.changeCSettings = function() {
	//	modals.invoke(
	//		'connection', rpc.getConfiguration(), _.bind(rpc.configure, rpc)
	//	);
	//}

	//scope.changeGSettings = function() {
	//	rpc.once('getGlobalOption', [], function(data) {
	//		var starred = utils.getCookie('aria2props');
	//		if (!starred || !starred.indexOf) starred = [];
	//		var vals = data[0];
	//		var settings = {};
	//
	//		// global settings divided into
	//		// file settings + some global specific
	//		// settings
	//
	//		_.forEach([fsettings, gsettings], function(sets) {
	//			for (var i in sets) {
	//				if (gexclude.indexOf(i) != -1) continue;
	//
	//				settings[i] = _.cloneDeep(sets[i]);
	//				settings[i].starred = starred.indexOf(i) != -1;
	//			}
	//		});
	//
	//		for (var i in vals) {
	//			if (i in gexclude) continue;
	//
	//			if (!(i in settings)) {
	//				settings[i] = { name: i, val: vals[i], desc: '', starred: starred.indexOf(i) != -1 };
	//			}
	//
	//			else {
	//				settings[i].val = vals[i];
	//			}
	//		}
	//
	//		modals.invoke(
	//			'settings', _.cloneDeep(settings),
	//			filter('translate')('Global Settings'),
	//			filter('translate')('Save'),
	//			function(chsettings) {
	//
	//				var sets = {};
	//				var starred = [];
	//				for (var i in chsettings) {
	//					// no need to change default values
	//					if (settings[i].val != chsettings[i].val)
	//						sets[i] = chsettings[i].val
	//
	//					if (chsettings[i].starred) {
	//						starred.push(i);
	//					}
	//				};
	//
	//				console.log('saving aria2 settings:', sets);
	//				console.log('saving aria2 starred:', starred);
	//
	//				rpc.once('changeGlobalOption', [sets]);
	//				utils.setCookie('aria2props', starred);
	//			});
	//	});
	//};

	//scope.showServerInfo = function() {
	//	modals.invoke(
	//		'server_info'
	//	);
	//};

	//scope.showAbout = function() {
	//	modals.invoke(
	//		'about'
	//	);
	//};

	//scope.shutDownServer = function () {
	//    rpc.once('shutdown', []);
	//};
}]);
