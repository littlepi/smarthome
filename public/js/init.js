var webui = angular.module('webui', [
  'webui.services.utils', 'webui.services.deps', 'webui.services.base64',
  'webui.services.configuration', 'webui.services.rpc',
  'webui.services.modals', 'webui.services.alerts',
  'webui.services.settings', 'webui.services.settings.filters',
  'webui.services.socket',
  'webui.filters.bytes','webui.filters.url',
  'webui.directives.fselect',
  'webui.ctrls.main', 'webui.ctrls.modal', 'webui.ctrls.alert',
  'webui.ctrls.props',
  'webui.ctrls.download',
  'webui.ctrls.remind',
  'webui.ctrls.control',
  'ngRoute',
  // external deps
  'ui.bootstrap',
  // translate
  'pascalprecht.translate',
  'angular-cron-gen',
  'mwl.calendar',
  'uiSwitch'
]);

function mergeTranslation(translation, base) {
	for (var i in base) {
		if (!base.hasOwnProperty(i)) {
			continue;
		}

		if (!translation[i] || !translation[i].length) {
			translation[i] = base[i];
		}
	}

	return translation;
}

webui.config(['$locationProvider', '$translateProvider', '$routeProvider', function ($locationProvider, $translateProvider, $routeProvider) {
  $locationProvider.hashPrefix('');
  $translateProvider
    .translations('en_US', translations.en_US)
    .translations('zh_CN', mergeTranslation(translations.zh_CN, translations.en_US))
    .useSanitizeValueStrategy('escapeParameters')
    .determinePreferredLanguage();
  $routeProvider
    .when('/', {
      templateUrl: 'home.html'
    })
	  .when('/download', {
      templateUrl: 'download.html'
    })
    .when('/remind', {
      templateUrl: 'remind.html'
    })
    .when('/control', {
      templateUrl: 'control.html'
    })
    .when('/help', {
      templateUrl: 'help.html'
    })
    .when('/settings', {
      templateUrl: 'settings.html'
    })
    .when('/login', {
      templateUrl: 'login.html'
    })
    .otherwise({
      redirectTo: '/'
    });
}]);

angular.element(document).ready(function() {
  angular.bootstrap(document, ['webui']);
});
