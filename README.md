# Little Pi #

test URI

http://down.sandai.net/thunder9/Thunder9.1.30.714.exe

## nginx setup tips ##


```
upstream io_nodes {
  server 192.168.0.102:5000;
  server 192.168.0.103:5000;
  hash $arg_p;
}

server {
	location / {
		# First attempt to serve request as file, then
		# as directory, then fall back to displaying a 404.
		#try_files $uri $uri/ =404;
		proxy_set_header Upgrade $http_upgrade;
		proxy_set_header Connection "upgrade";
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_set_header Host $host;
		proxy_http_version 1.1;
		#proxy_pass http://io_nodes;
		if ($arg_p) {
			proxy_pass http://io_nodes;
		}
	}
}

```

Then, request with socket.io client:

```
var socket = io.connect("http://" + location.host + "/i", {query: "p=aaa102" });
```

TODO: judge if arg_p exists.
