var utils = require("./utils");

var piSocketMap = {}; // cache pid->pisocketid mapping
var uiSocketMap = {}; // cache pid->uisocketid mapping

var io = require("socket.io")();
io.on("connection", function(socket) {
  var query = socket.handshake.query;
  var pid = (query.hasOwnProperty("p")) ? query.p : null;
  if (pid === null) {
    if (utils.isPi()) {
      socket.emit("init", process.env.MAC_ADDR); // init pid on page
    }
    else {
      //disconnect socket along with the its transport connection
      socket.disconnect(true);
    }
  }
  var sid = socket.id;
  //console.log(socket.handshake.query);
  //console.log(piSocketMap);
  //socket.use(function(packet, next){
  //  console.log(packet);
  //  next();
  //});
  socket.on("pi-register", function() {
    piSocketMap[pid] = sid;
    //if (!utils.isPi()) {
    //  console.log("piSocketMap[" +pid + "] = " + socket.id);
    //}
  });
  //ui ping
  socket.on("i", function(data){
    if (!piSocketMap.hasOwnProperty(pid)) {
      //socket.disconnect(true);
      socket.emit("offline");
    }
    else {
      if (!uiSocketMap.hasOwnProperty(pid)) {
        uiSocketMap[pid] = socket.id;
        data.con = true; // tell pi that there is a new ui connection
        socket.to(piSocketMap[pid]).emit("po", data);
      }
      else if (uiSocketMap[pid] != socket.id) {
        // only allow one connection from ui, so have to disconnect previous connection
        io.sockets.connected[uiSocketMap[pid]].disconnect(true);
        uiSocketMap[pid] = socket.id;
        data.con = true; // tell pi that there is a new ui connection
        socket.to(piSocketMap[pid]).emit("po", data);
      }
      else {
        // consider using "volatile" here because pi might be disconnected or there are other connecting problem
        //console.log("piSocketMap[" +pid + "] = " + piSocketMap[pid]);
        socket.volatile.to(piSocketMap[pid]).emit("po", data);
      }
    }
  });
  //pi ping
  socket.on("pi", function(data) {
    var target = data.t;
    if (data.t == "download") {
      data = data.d;
    }
    else if (data.t == "wechat") {
      console.log(data);
      //TODO, send wechat to user
    }
    //console.log("uiSocketMap[" +pid + "] = " + socket.id);
    socket.to(uiSocketMap[pid]).emit(target, data);
  });
  socket.on("disconnect", function() {
    //console.log("[DCON]sid=" + socket.id);
    if (uiSocketMap.hasOwnProperty(pid) && uiSocketMap[pid] == socket.id) {
      //console.log("remove uiSocketMap[" +pid + "]");
      delete uiSocketMap[pid];
      socket.to(piSocketMap[pid]).emit("po", {con: false}); // tell pi that there is no ui connection
    }
    if (piSocketMap.hasOwnProperty(pid) && piSocketMap[pid] == socket.id) {
      //console.log("remove piSocketMap[" +pid + "] = " + sid);
      delete piSocketMap[pid];
    }
  });
});

module.exports = io;
