var spawn = require('child_process').spawn;
var Sound = require('aplay');

var WAV_LOCATION = __dirname.substr(0, __dirname.lastIndexOf('/')) + '/wav/';
var WAV_FILE_SUFFIX = ".wav";
var ttsExec = "tts_sample";
var _process = null;

function textToSpeech(filename, text, voice) {
  if (typeof(filename) === 'undefined' || typeof(text) === 'undefined' || text === "") return;
  if (filename.indexOf(WAV_FILE_SUFFIX) == -1) {
    filename += WAV_FILE_SUFFIX;
  }
  if (_process !== null) _process.kill('SIGTERM');
  _process = spawn(ttsExec, [ WAV_LOCATION+filename, text ]);
  _process.on('exit', function (code, sig) {
    if (code !== null && sig === null) {
      console.log("generate file: "+ filename);
      //Complete
    }
  });
}

function playWav(filename) {
  if (typeof(filename) === 'undefined') return;
  if (filename.indexOf(WAV_FILE_SUFFIX) == -1) {
    filename += WAV_FILE_SUFFIX;
  }
  new Sound().play(WAV_LOCATION+filename);
}

function test() {
  textToSpeech("123", "I love you!");
  playWav("123");
}

module.exports = {
  test: test,
  toWav: textToSpeech,
  play: playWav
};