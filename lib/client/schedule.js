var nodeSchedule = require("node-schedule");
var tts = require("./service/tts");
//var redisConn = require("./redisConn");
var redis = require("redis");
var pub = redis.createClient();

var schedules = {};

function test() {
  var startTime = Date.now();
  var start = parseInt(startTime/1000).toString();
  var end = parseInt((startTime + 122000)/1000).toString();
  var rule = "34 */1 * * * * *";
  var job_object = {id: start, c: [0, 1, 2], d: {text: "准备睡觉吧"}};
  var job = JSON.stringify(job_object);
  console.log("test");
  
  //start = "1494083555";
  //end = null;
  //rule = '6 0/1 * 1/1 * ? *';
  //job_object = {c: [0, 1], d: {text: "Please sleep now!"}};
  //job = JSON.stringify(job_object);
  var data = {s: start, e: end, r: rule, j: job};
  addSchedule(data);
  //setTimeout(function() {
  //  cancelSchedule(start);
  //}, 3000);
}

function initSchedules () {
  // read persisted schedules and addSchedule
}

function addSchedule(data, callback) {
  var start = data.s, end = data.e, rule = data.r, job = data.j;
  var jobId = start; // assume start and end are String, such as parseInt(aDate.getTime()/1000).toString()
  var startDate = (start===null || start===null || start.trim()==="") ? new Date() : new Date(parseInt(start)*1000);
  //startDate = new Date(); // TODO remove
  var endDate = (typeof(end) === "undefined" || end===null || end.trim()==="") ? null : new Date(parseInt(end)*1000);
  var cronRule = (typeof(rule) === "undefined" || rule===null) ? "": rule;
  console.log(startDate, endDate, cronRule, job);
  var j;
  if (cronRule === "") {
    j= nodeSchedule.scheduleJob(startDate, function(){
      //console.log("do job");
      //redisConn.publishToJob(job);
      pub.publish("job", job);
    });
  }
  else {
    j= nodeSchedule.scheduleJob({ start: startDate, end: endDate, rule: cronRule}, function(){
      //console.log("do job");
      //redisConn.publishToJob(job);
      pub.publish("job", job);
    });
  }
  //redisConn.persistJob();
  
  //TODO judge if need to generate audio
  tts.toWav(start, "您有一份备忘："+JSON.parse(job).d.text);
  schedules[jobId] = j;
  if (callback) {
    callback(data);
  }
}

function cancelSchedule(jobId) {
  var h = schedules[jobId];
  if (schedules.hasOwnProperty(jobId)) {
    h.cancel();
    delete(schedules[jobId]);
  }
}

module.exports = {
  test: test,
  init: initSchedules,
  add: addSchedule,
  cancel: cancelSchedule
};