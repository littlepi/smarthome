var schedule = require("./schedule");

var hasConn = false; // judge if there is any ui client
var sendToRemote = false; // if false, send to local, else if true, send to remote server
var socketClients = {};

var rpcSocket; // rpc Websocket client (connect to aria2)

function initPiSocket(ioHostPort) {
  var pid = process.env.MAC_ADDR;
  var piSocket = require("socket.io-client")("http://"+ioHostPort, {query: "p="+pid});
  if (ioHostPort == process.env.REMOTE_APP_ADDR) {
    socketClients.remote = piSocket;
  }
  else {
    socketClients.local = piSocket;
  }
  piSocket.on("connect", function () {
    piSocket.emit("pi-register");
  });
  //pi pong
  piSocket.on("po", function(data) {
    if (data.hasOwnProperty("con")) { // if data.con is true, has ui connection; otherwise has no ui connection
      hasConn = data.con;
    }
    sendToRemote = (piSocket.io.engine.hostname != "127.0.0.1") ? true : false;
    //var pid = process.env.MAC_ADDR;
    var target = data.t;
    var ping = {t: target, d: {}}; // dont need t?
    if (target == "download") {
      rpcSocket.send(JSON.stringify(data));
    }
    else {
      if (target == "all") {
        ping.d = {ip: process.env.IP_ADDR+":"+process.env.PORT};
        piSocket.emit("pi", ping);
      }
      else if (target == "job") {
        data = data.d;
        console.log(data);
        schedule.add(data, function(d) {
          ping.t = "job.add";
          ping.d = d;
          piSocket.emit("pi", ping);
        });
      }
      //piSocket.emit("pi", ping);
    }
  });

}

function hasUiConn() {
  return hasConn;
}

function getPiSocket() {
  return sendToRemote ? socketClients.remote : socketClients.local;
}

function getRemotePiSocket() {
  return socketClients.remote;
}

function initRpcSocket() {
  rpcSocket = new (require('ws'))('ws://127.0.0.1:6800/jsonrpc');
  rpcSocket.onmessage = function (e) {
    //console.log('message recieved from aria2: ', e.data);
    //w.send(e.data);
    //console.log(e.data);
    //var parsed = JSON.parse(e.data);
    //var pid = process.env.MAC_ADDR;
    var target = "download";
    var ping = {t: target, d: e.data}; // dont need t?
    //var to = sendToRemote ? socketClients.remote : socketClients.local;
    var to = getPiSocket();
    //console.log(sendToRemote ? "send to remote" : "send to local");
    to.emit('pi', ping);
  };
}

module.exports = {
  init: function () {
    initPiSocket(process.env.REMOTE_APP_ADDR);
    initPiSocket("127.0.0.1" + ":" + process.env.PORT);
    initRpcSocket();
  },
  hasUiConn: hasUiConn,
  getPiSocket: getPiSocket,
  getRemotePiSocket: getRemotePiSocket
};