var redis = require("redis");
var job = require("./job");

//var REDIS_URL = process.env.REDIS_URL;
var CHANNELS = {
  JOB: "job"
};

var pub = redis.createClient();
var sub = redis.createClient();

function initChannels() {
  sub.on('message', function(channel, msg) {
    if (channel == CHANNELS.JOB) {
      job.process(msg);
    }
    //console.log("channel: " + channel + ", msg: " + msg);
  });
  sub.subscribe('job');
}

function publishToChannel(channel, msg) {
  pub.publish(channel, msg);
}

function getSubClient() {
  return sub;
}

function getPubClient() {
  return pub;
}

module.exports = {
  init: initChannels,
  //publish: publishToChannel,
  //publishToJob: function(msg) {
  //  publishToChannel(CHANNELS.JOB, msg);
  //},
  getSub: getSubClient,
  getPub: getPubClient
};
