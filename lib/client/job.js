var socketsConn = require("./socketsConn");
var tts = require("./service/tts");

var CATEGORY = {
  UI: 0,
  WECHAT: 1,
  AUDIO: 2,
  CONTROL: 3
};

// job is string, "{"t":[1,2,3],"d":{"text":"Please sleep now!"}}";
function processJob(job) {
  console.log("--"+(new Date()).toString());
  var j = {};
  try {
    j = JSON.parse(job);
  }
  catch (e) {
    console.error(e);
  }
  var id = (j.hasOwnProperty("id")) ? j.id : "";
  var c = (j.hasOwnProperty("c")) ? j.c : [];
  var d = (j.hasOwnProperty("d")) ? j.d : {};
  if (c && c.length>0) {
    for (var i=0; i<c.length; i++) {
      switch (c[i]) {
        case CATEGORY.UI:
          var hasConn = socketsConn.hasUiConn();
          if (!hasConn) {
            console.log("no ui connection");
            break;
          }
          var socket = socketsConn.getPiSocket();
          socket.emit("pi", {t: "job", d: d.text});
          console.log("send to ui: " + d.text);
          break;
        case CATEGORY.WECHAT:
          var remoteSocket = socketsConn.getRemotePiSocket();
          remoteSocket.emit("pi", {t: "wechat", d: d.text});
          console.log("send to wechat: " + d.text);
          break;
        case CATEGORY.AUDIO:
          console.log("send to audio: " + d.text);
          tts.play(id);
          break;
        case CATEGORY.CONTROL:
          console.log("send to control: " + d.text);
          break;
        default:
          console.log(d.text);
      }
    }
  }
}

module.exports = {
  process: processJob
};