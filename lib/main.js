module.exports = function () {
  if (require('./utils').isPi()) {
    require('./client')();
  }
  else {
    require('./server')();
  }
  //var schedule = require('./client/schedule');
  ////var notify = {t: 1, d: "Go for a walk"};
  //schedule.addByDate(new Date(new Date().getTime()+2000));
  //schedule.addByInterval(4);
};