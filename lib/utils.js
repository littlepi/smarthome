var utils = {
  isPi:  function() {
    return require("os").hostname() == "raspberrypi";
  }
};

module.exports = utils;