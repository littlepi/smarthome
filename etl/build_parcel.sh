#!/bin/bash

#update version as upgrade
source ./version.sh
let "build_index=$(date +"%y%j%H%M")"
parcel_version=${parcel_version}.b${build_index}

PARCEL_NAME=cim_etl_oozie
PARCEL_DIRNAME=${PARCEL_NAME}-${parcel_version}

function build_os_parcel {
	echo "start build parcel "${PARCEL_NAME}", parcel version is "${parcel_version}", cim etl oozie version is "${cim_etl_oozie_version}" suffix:"${suffix}
	suffix=$1
	PARCEL_FILENAME=${PARCEL_DIRNAME}-${suffix}.parcel 
	parcel_target=parcel_target/${suffix}
	tmpdir_root=${parcel_target}/tmp
	tmpdir=${tmpdir_root}/${PARCEL_DIRNAME}

	#clean tmp folder
	echo "clean tmp files ${parcel_target}/*"
	rm -rf ${parcel_target}/*

	#mkdir if not exist
	mkdir -p ${tmpdir}

	#cp files to tmp to make parcel
	mkdir -p ${tmpdir}/meta
	cp -r oozie ${tmpdir}/
	cp -r meta/* ${tmpdir}/meta

	#build env script
	echo "build env script"
	env_script=${tmpdir}/meta/cim_etl_oozie-env.sh
	mv ${env_script}.template ${env_script}
	sed -i 's/%parcel_dirname%/'${PARCEL_DIRNAME}'/g' $env_script

	#build parcel.json
	echo "build parcel.json"
	parcel_json=${tmpdir}/meta/parcel.json
	mv ${parcel_json}.template ${parcel_json}
	sed -i 's/%parcel_version%/'${parcel_version}'/g' $parcel_json
	sed -i 's/%cim_etl_oozie_version%/'${cim_etl_oozie_version}'/g' $parcel_json

	#build parcel
	echo "build parcel"
	tar -C ${tmpdir_root} -zcf  ${parcel_target}/${PARCEL_FILENAME} ${PARCEL_DIRNAME} --owner=root --group=root

	#build parcel.sha
	echo "build parcel.sha"
	sha1sum ${parcel_target}/${PARCEL_FILENAME} | awk '{print $1}' > ${parcel_target}/${PARCEL_FILENAME}.sha 

	#build manifest file
	echo "build manifest file"
	manifest=${parcel_target}/manifest.json
	cp manifest-cim_etl_oozie.json.template $manifest 
	sed -i 's/%parcelName%/'${PARCEL_FILENAME}'/g' $manifest 
	sed -i 's/%pkg_name%/'${PARCEL_NAME}'/g' $manifest 
	sed -i 's/%pkg_version%/'${cim_etl_oozie_version}'/g' $manifest 
	sed -i 's/%component_version%/'${cim_etl_oozie_version}'/g' $manifest 

	dateval=$(date +"%s000")
	sed -i 's/%lastUpdated%/'${dateval}'/g' $manifest 

	hashvalue=$(cat ${parcel_target}/${PARCEL_FILENAME}.sha) 
	sed -i 's/%hash%/'${hashvalue}'/g' $manifest 

	rm -rf ${tmpdir_root}
	echo "build successful"
}

#el6: Redhat Enterprise Linux 6 and clones (CentOS, Scientific Linux, etc)
build_os_parcel el6

#el7: Redhat Enterprise Linux 7 and clones (CentOS, Scientific Linux, etc)
build_os_parcel el7
