# IVSG CIM Data Engine Oozie ETL Application

## Version

0.0.1.1, 2015-06-24

## Author and Contact

Yinxin Xia <yixia@cisco.com>

## Description

IVSG CIM Data Engine Oozie ETL Application will run by Oozie in period to load raw data from HBase and insert into Impala Parquet table after processing and transferring.
Also it will initialize the Impala tables.

## Deployment to CIM CCS

### Upload the package to CM-HUE server in CCS by SSH

1.  Prepare a local Linux environment

2.  Default IP Address of CIM Hue Server:
    173.37.46.222

3.  Download the installation package and SSH Key
    The public Share folder:
    https://cisco.app.box.com/files/0/f/3681219973/Doc_for_share

    a. SSH Key of "cim" user: "ivsg-cim.pem";
    b. Installation package: "cim_etl_oozie.tar.gz"

4.  Upload the package to CCS from local Linux
    $ chmod 600 ivsg-cim.pem
    $ scp -i ivsg-cim.pem cim_etl_oozie.tar.gz cim@173.37.46.222:~

### Submit ETL Task to Oozie

1.  Login CIM Hue Server by SSH
    $ ssh -i ivsg-cim.pem cim@173.37.46.222

2.  Decompressing the package:
    $ tar xvf cim_etl_oozie.tar.gz
    $ cd cim_etl_oozie

3.  Edit "oozie/job.properties" if necessary

4.  Submit ETL task to Oozie
    a. Stop old one in Hue:
       Go to http://173.37.46.222:8888/oozie/list_oozie_coordinators/
       And kill the task named "cim-etl-coord"

    b. Submit new task to Oozie
    $ ./run --oozie http://cm-hue-01.novalocal:11000/oozie [--destroy] [--auto]
	"--destroy": If you want to destroy the old databases add "--destroy" option;
    "--auto": is used to set up the start time of ETL automatically, this will ignore the ‘start’ time set in job.properties;

### Debug and Simple Trouble Shooting

1.  Check overall status of Oozie ETL application
	http://173.37.46.222:8888/oozie/list_oozie_coordinators/

2.  Check status of each round of task workflow
    http://173.37.46.222:8888/oozie/list_oozie_workflows/

3.  Oozie Log
	a. ETL job running logs:
	   The ETL job running logs are located at /var/log/oozie folder on each oozie server.
	b. Oozie coordinator, workflow and action log:
       Using Hue, under "Workflows"->"Dashboards"-> "Coordinators", click the coordinator job, then click the workflow and the action, the related logs can be found in turn.
       As the java actions within oozie workflows are processed as a map job on yarn, all the action logs (stderr, stdout or syslog) can also be found on yarn JobHistory. The link is provided in the action configuring under the oozie GUI(http://haproxy_address:11000).

4.  Impala Log
    “/var/log/impalad/impalad.ERROR.*”

