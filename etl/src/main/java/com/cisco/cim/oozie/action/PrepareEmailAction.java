package com.cisco.cim.oozie.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Properties;
import org.apache.log4j.Logger;
import com.cisco.cim.oozie.util.DateProcessor;
import com.cisco.cim.oozie.util.HDFSAccessor;

/**
 * The Action class used for oozie action 'make-email-decision'.
 * Used to control the frequecy that error emails are sent.
 * @author donwu
 *
 */
public class PrepareEmailAction {
    private static final Logger logger = Logger.getLogger(PrepareEmailAction.class);
    private static final String LASTMAILDATE = "lastmail";
    private static final String SENDEMAIL = "SENDEMAIL";
    private static final String DONOTSEND = "CANCELEMAIL";
    
    public static void main(String[] args) throws Exception {
        try {
            String configFileURL = args[0];
            String nominal = args[1];
            String freqInMinutes = args[2];
            
            long freq = Integer.valueOf(freqInMinutes) * 60 * 1000;
            
            HDFSAccessor accessor = new HDFSAccessor();
            Properties props = accessor.readProperties(configFileURL);
            String lastMail = props.getProperty(LASTMAILDATE);
            logger.info("Current nominal time is " + nominal + ", Last mail sent at " + lastMail);
            
            long lastMailTimestamp = DateProcessor.parseDateFromString(lastMail).getTime();
            long nominalTimestamp = DateProcessor.parseDateFromString(nominal).getTime();
            
            String oozieProp = System.getProperty("oozie.action.output.properties");
            if (oozieProp != null) {
                File file = new File(oozieProp);
                Properties selfProps = new Properties();
                if (nominalTimestamp < lastMailTimestamp) {
                    selfProps.setProperty("email.decision", DONOTSEND);
                    logger.info("Current time " + nominal + " is later than last email sent time " + lastMail + ", process ignored.");
                }
                else if (nominalTimestamp - lastMailTimestamp < freq) {
                    selfProps.setProperty("email.decision", DONOTSEND);
                    logger.info("It's not yet " + freqInMinutes + " minutes since last email sent at " + lastMail + ", email will not be sent.");
                }
                else {
                    logger.info("Email will be sent.");
                    selfProps.setProperty("email.decision", SENDEMAIL);
                    props.setProperty(LASTMAILDATE, nominal);
                    accessor.writeProperties(props, configFileURL, "etl settings");
                }
                OutputStream os = new FileOutputStream(file);
                selfProps.store(os, "");
                os.close();
            }
            else {
                logger.info("System property oozie.action.output.properties not defined");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
    
}
