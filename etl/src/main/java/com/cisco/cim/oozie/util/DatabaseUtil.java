package com.cisco.cim.oozie.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DatabaseUtil {

	private DBManager dbm = null;

	private DatabaseUtil() {

	}

	private static class DatabaseUtilHolder {
		private final static DatabaseUtil instance = new DatabaseUtil();
	}

	public static DatabaseUtil getInstance() {
		return DatabaseUtilHolder.instance;
	}

	public void setDBManager(DBManager dbmanager) throws Exception {
		dbm = dbmanager;
		dbm.getConnections();
	}

	public void impalaCmd(String cmd) throws SQLException {
		try {
			dbm.executeImpalaCmd(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JSONArray getTableList(String DBName) {
		ArrayList<String> as = new ArrayList<String>();
		JSONArray rsArray = null;
		try {
			ResultSet rs = dbm.executeImpalaCmdQuery("show tables in "+DBName);
			rsArray = handle(rs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rsArray;
	}

	
	public void refresh() {
		String cmd = "invalidate metadata";
		try {
			dbm.executeImpalaCmd(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void dropTable(String tableName) {
		String cmd = "drop table if exists " + tableName;
		try {
			dbm.executeImpalaCmd(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void createDataBase(String dbName) {
		String cmd = "create database if not exists " + dbName;
		try {
			dbm.executeImpalaCmd(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getCityPartitionsByYearMonth(String tableName,int year,int month) {	
		JSONArray rsArray = null;
		ArrayList<String> result = new ArrayList<String>();
		String cmd = "show partitions " + tableName;
		try {
			ResultSet rs = dbm.executeImpalaCmdQuery(cmd);
			rsArray = handle(rs);
			Iterator<Object> it = rsArray.iterator();
			while (it.hasNext()) {
				JSONObject jsonObj = (JSONObject) it.next();
				if(jsonObj.getString("year").equals(String.valueOf(year)) 
						&& jsonObj.getString("month").equals(String.valueOf(month))){
					result.add(jsonObj.getString("city"));
				}			
			}
		} catch (Exception e) {
			if(e.getMessage().contains("Table is not partitioned")){
				return null;
			}
			e.printStackTrace();
		}	
		return result;
	}
	
	public void dupTable(String tableName,String bakupTableName) {		
		String cmd = "create table if not exists " + bakupTableName + " like " + tableName;
		try {
			dbm.executeImpalaCmd(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void copyDataByPart(String sourceTable,String targetTable,JSONObject partObj,boolean useHive) {		
		JSONObject tableStruct  = getTableCols(sourceTable);
		String commonColStr = ((JSONArray) tableStruct.get("commonCols")).join(",").replace("\"", "");
		String partColStr = ((JSONArray) tableStruct.get("partitionCols")).join(",").replace("\"", "");
		String cmd = "insert into " + targetTable + " partition(" + partColStr;
		cmd += ") select " + commonColStr + "," + partColStr + " from " + sourceTable + " where ";
		Iterator iterator = partObj.keys();
		String key = "";
		String value = "";
		while (iterator.hasNext()) {
			key = (String) iterator.next();
			if (partObj.get(key) instanceof Integer) {
				cmd += key + "=" + Integer.toString(partObj.getInt(key)) + " and ";
			} else if (partObj.get(key) instanceof String) {
				cmd += key + "=\"" + partObj.getString(key) + "\" and ";
			}
		}
		cmd = cmd.substring(0, cmd.length() - 4);
		System.out.println(cmd);
		try {
			if(useHive){
				dbm.executeHiveCmd("set hive.exec.dynamic.partition.mode=nonstrict");
				dbm.executeHiveCmd(cmd);
			}else{
				dbm.executeImpalaCmd(cmd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deletePart(String tableName,JSONObject partObj) {
		String cmd = "alter table " + tableName + " drop partition(";
        String key = "";
        Iterator iterator = partObj.keys();
        while(iterator.hasNext()){
            key = (String) iterator.next();
            if (partObj.get(key) instanceof Integer){
            	cmd += key + "=" +  Integer.toString(partObj.getInt(key))+ ",";
            }else if (partObj.get(key) instanceof String){
            	cmd += key + "=\"" + partObj.getString(key) + "\",";
            }       
       }	
       cmd = cmd.substring(0, cmd.length()-1)+") purge";
       System.out.println(cmd);
	   try {
			dbm.executeImpalaCmd(cmd);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public JSONObject getTableCols(String tableName) {
		JSONArray rsArray = null;
		ResultSet rs = null;
		ArrayList<String> commonCols= new ArrayList<String>();
		ArrayList<String> partCols= new ArrayList<String>();
	
		try {
			//dbm.executeImpalaCmd("show partitions "+ tableName);
			rs = dbm.executeImpalaCmdQuery("describe "+ tableName);
			rsArray = handle(rs);
			System.out.println(rsArray);
			Iterator<Object> it = rsArray.iterator();
			while (it.hasNext()) {
				JSONObject ob = (JSONObject) it.next();
				commonCols.add(ob.getString("name"));
			}
			rs = dbm.executeImpalaCmdQuery("show create table "+ tableName);
			rsArray = handle(rs);
			String createSQL = ((JSONObject)rsArray.get(0)).getString("result");
			String partStr= createSQL.replace("\n","").replace("PARTITIONED BY (", "##").replace(")STORED AS", "##").split("##")[1];
			partStr = partStr.replace("INT", "").replace("STRING", "").replace(" ", "");
			for(String s : partStr.split(",")){
				commonCols.remove(s);
				partCols.add(s);
			}
		} catch (Exception e) {
			return null;
		}
		JSONObject result = new JSONObject();;
		result.put("commonCols", commonCols);
		result.put("partitionCols", partCols);	
		return result;
	}
	
	private JSONArray handle(ResultSet rs) throws SQLException {
		JSONArray jsonArray = new JSONArray();
		ResultSetMetaData rsmd = rs.getMetaData();
		while (rs.next()) {
			JSONObject obj = new JSONObject();
			getType(rs, rsmd, obj);
			jsonArray.put(obj);
		}
		return jsonArray;
	}
	private void getType(ResultSet rs, ResultSetMetaData rsmd, JSONObject obj) throws SQLException {
		int total_rows = rsmd.getColumnCount();
		for (int i = 0; i < total_rows; i++) {
			String columnName = rsmd.getColumnLabel(i + 1);
			if (obj.has(columnName)) {
				columnName += "1";
			}
			try {
				switch (rsmd.getColumnType(i + 1)) {
				case java.sql.Types.ARRAY:
					obj.put(columnName, rs.getArray(columnName));
					break;
				case java.sql.Types.BIGINT:
					obj.put(columnName, rs.getInt(columnName));
					break;
				case java.sql.Types.BOOLEAN:
					obj.put(columnName, rs.getBoolean(columnName));
					break;
				case java.sql.Types.BLOB:
					obj.put(columnName, rs.getBlob(columnName));
					break;
				case java.sql.Types.DOUBLE:
					obj.put(columnName, rs.getDouble(columnName));
					break;
				case java.sql.Types.FLOAT:
					obj.put(columnName, rs.getFloat(columnName));
					break;
				case java.sql.Types.INTEGER:
					obj.put(columnName, rs.getInt(columnName));
					break;
				case java.sql.Types.NVARCHAR:
					obj.put(columnName, rs.getNString(columnName));
					break;
				case java.sql.Types.VARCHAR:
					obj.put(columnName, rs.getString(columnName));
					break;
				case java.sql.Types.TINYINT:
					obj.put(columnName, rs.getInt(columnName));
					break;
				case java.sql.Types.SMALLINT:
					obj.put(columnName, rs.getInt(columnName));
					break;
				case java.sql.Types.DATE:
					obj.put(columnName, rs.getDate(columnName));
					break;
				case java.sql.Types.TIMESTAMP:
					obj.put(columnName, rs.getTimestamp(columnName));
					break;
				default:
					obj.put(columnName, rs.getObject(columnName));
					break;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		System.setProperty("java.security.krb5.realm", "CIM.IVSG.AUTH");
		System.setProperty("java.security.krb5.kdc", "cm-hue-01.novalocal");
		Configuration conf = new Configuration();
		conf.set("hadoop.security.authentication", "Kerberos");
		UserGroupInformation.setConfiguration(conf);
		DBManager dbm = new DBManager(
				"jdbc:hive2://cm-hue-01.novalocal:10000/;principal=hive/cm-hue-01.novalocal@CIM.IVSG.AUTH",
				"jdbc:hive2://cm-hue-01.novalocal:21050/;principal=impala/cm-hue-01.novalocal@CIM.IVSG.AUTH",
				"/Users/chao/workspace/etl_230.keytab");

		DatabaseUtil dbutil = DatabaseUtil.getInstance();
		dbutil.setDBManager(dbm);
		JSONArray tableArray = dbutil.getTableList("cimdata");
		//System.out.println(tableList);
		//dbutil.dupTable("cimdata.light_state","cim_bak.light_state");
		//dbutil.copyDataByPart("cimdata.light_state", "cim_bak.light_state","month","8");
		//dbutil.getTableCols("cimdata.light_state");
		System.out.println(dbutil.getCityPartitionsByYearMonth("cimdata.light_state", 2017,9));
		
		JSONObject partObj = new JSONObject();
		partObj.put("year", 2017);
		partObj.put("month", 9);
		partObj.put("city", "Beijing");
		//dbutil.deletePart("cimdata.light_state", partObj);
		
		Iterator<Object> it = tableArray.iterator();
		int count = 0;
		while (it.hasNext()) {
			count += 1;
			if(count > 8)
				break;
			JSONObject ob = (JSONObject) it.next();
			//System.out.println(ob.get("name"));
			//dbu.showCreate(ob.getString("name"));
			//dbutil.getTableCols("light_state");
		}
		
		

	}

}
