package com.cisco.cim.oozie.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import com.cisco.cim.jsonparser.Schema;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;

/**
 * This class is called directly from oozie/run script
 */
public class InitialOperation {
    private static ETL etl;
    private static String DROP_OPERATION = "drop";
    private static String SETUP_START_OPERATION = "generatestart";
    private static String CREATE_ADMIN_ROLE = "createadminrole";
    private static String CREATE_CIM_SELECT_ROLE = "createcimselectrole";
    private static String JOB_PROPERTY_STARTTIME = "processstart";
    private static String LOCATIONS_NOT_EXIST = "NOTEXIST";
    private static String GENERATE_KEYTAB_FILE = "generatekeytab";

    public static void main(String[] args) throws Exception {
        try {
            String op = args[0];
            
            Schema schema = new Schema();
            Map<String, Map<String, Object>> top = schema.getHBaseTables();
            
            Configuration conf = new Configuration();
            conf.set("hadoop.security.authentication", "Kerberos");
            UserGroupInformation.setConfiguration(conf);
            if (op.equalsIgnoreCase(DROP_OPERATION) || op.equalsIgnoreCase(SETUP_START_OPERATION)) {
                String hiveURL = args[1];
                String impalaURL = args[2];
                String keytabFile = args[3];
                DBManager dbm = new DBManager(hiveURL, impalaURL, keytabFile);
                etl = new ETL(dbm, top);
            }
            // drop all databases
            if (op.equalsIgnoreCase(DROP_OPERATION)) {
                etl.dropAllDatabases();
            }
            // set 'start' property automatically in job.properties file
            else if (op.equalsIgnoreCase(SETUP_START_OPERATION)) {
                String propertyFile = args[4];
                String tz = args[5];
                etl.initLocations(top);
                String initTimestamp = etl.getInitTime();
                String start = "";
                if (initTimestamp.equalsIgnoreCase(LOCATIONS_NOT_EXIST)) {
                    Date currentDate = new Date();
                    long currentTimestamp = currentDate.getTime();
                    start = DateProcessor.parseDateStringFromLong(currentTimestamp, TimeZone.getTimeZone(tz));
                }
                else {
                    start = DateProcessor.parseDateStringFromLong(Long.parseLong(initTimestamp), TimeZone.getTimeZone(tz));
                }
                String cmdStr = "sed -i 's/^"+ JOB_PROPERTY_STARTTIME +"=.*/" + JOB_PROPERTY_STARTTIME + "="+ start +"/' " + propertyFile;
                CLITool.executeLinuxCommand(cmdStr, 10);
            }
            // create admin role of all databases and assign to specific group
            else if (op.equalsIgnoreCase(CREATE_ADMIN_ROLE)) {
                String serverName = args[1];
                String groupName = args[2];
                String hiveURL = args[3];
                String keytabFile = args[4];
                UserManager.createAdminRoleForGroup(serverName, groupName, hiveURL, keytabFile);
            }
            // create role that has select privilege of 'cim' database and assign to specific group
            else if (op.equalsIgnoreCase(CREATE_CIM_SELECT_ROLE)) {
                String groupName = args[1];
                String hiveURL = args[2];
                String keytabFile = args[3];
                UserManager.createCIMSelectRoleForGroup(groupName, hiveURL, keytabFile);
            }
            // automatically generate keytab file and sync to all hosts within cluster
            else if (op.equalsIgnoreCase(GENERATE_KEYTAB_FILE)) {
                String kdc = args[1];
                String hueHost = args[2];
                String allHosts = args[3];
                String keytabFile = args[4];
                File f = new File(keytabFile);
                String keytabPath = f.getParent()+"/";
                
                List<String> principalGlobs = new ArrayList<String>();
                principalGlobs.add("impala*");
                principalGlobs.add("hive*");
                principalGlobs.add("oozie*");
                principalGlobs.add("hdfs");
                principalGlobs.add("cloudera-scm/admin");
                
                String cmdPrepare = "rm -rf "+ keytabPath + ";mkdir -p " + keytabPath;
                String cmdDone = "chown -R oozie:oozie " + keytabPath + "; chmod 400 " + keytabFile;
                if (kdc.equalsIgnoreCase(hueHost)) {
                    CLITool.executeLinuxCommand(cmdPrepare, 10);
                    for (String principal : principalGlobs) {
                        String cmdStr = "/usr/sbin/kadmin.local -q 'ktadd -norandkey -k "+ keytabFile +" -glob "+ principal +"'";
                        CLITool.executeLinuxCommand(cmdStr, 10);
                    }
                    CLITool.executeLinuxCommand(cmdDone, 10);
                }
                else {
                    CLITool.executeRSHCommandAsRootByOozie(cmdPrepare, kdc);
                    for (String principal : principalGlobs) {
                        String cmdStr = "/usr/sbin/kadmin.local -q 'ktadd -norandkey -k "+ keytabFile +" -glob "+ principal +"'";
                        CLITool.executeRSHCommandAsRootByOozie(cmdStr, kdc);
                    }
                    CLITool.executeRSHCommandAsRootByOozie(cmdDone, kdc);
                    String cmdSync = "rm -rf "+ keytabPath + ";mkdir -p "+ keytabPath +";chown -R oozie:oozie " + keytabPath + ";sudo -u oozie /usr/bin/rsync -avz --rsh='/usr/bin/rsh -l root' "+ kdc +":"+ keytabPath +" " + keytabPath;
                    CLITool.executeLinuxCommand(cmdSync, 10);
                }
                List<String> hostList = Arrays.asList(allHosts.replaceAll(" ", "").split(","));
                for (String host : hostList) {
                    if (!host.equalsIgnoreCase(hueHost)) {
                        UserManager.syncKeytabFileToHost(keytabPath, keytabFile, host);
                    }
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (etl != null) {
                    etl.closeConnections();
                }
            } catch (Exception e) {
                System.err.println("Error happens when close connection");
            }
        }
    }
}

