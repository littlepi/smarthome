package com.cisco.cim.oozie.util;

import org.apache.log4j.Logger;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class CLITool {
    private static final Logger logger = Logger.getLogger(CLITool.class);
    public static String OUTPUT = "";
    
    /**
     * Decide whether a Process is alive
     * @param p the process to judge
     * @return whether a Process is alive
     */
    public static boolean isAlive(Process p) {
        try {
            p.exitValue();
            return false;
        } catch (IllegalThreadStateException e) {
            return true;
        }
    }
    
    /**
     * Transfer command output from stream to string
     * @param inputStream the input stream
     * @return the transfered string
     * @throws Exception
     */
    private static String output(InputStream inputStream) throws Exception {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(inputStream));
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line + System.getProperty("line.separator"));
            }
        } finally {
            br.close();
        }
        return sb.toString();
    }

    /**
     * Execute command locally
     * @param cmdStr the command string
     * @param timeoutInSecond the timeout set for the command, in seconds
     * @return the execution result
     * @throws Exception
     */
    public static boolean executeLinuxCommand(String cmdStr, int timeoutInSecond) throws Exception {
        logger.info("Execute Shell command: " + cmdStr);
        long now = System.currentTimeMillis();
        long finish = now + 1000L * timeoutInSecond;
        String[] cmd = { "bash", "-c", cmdStr };
        ProcessBuilder pb = new ProcessBuilder(cmd);
        pb.redirectErrorStream(true);
        Process process = pb.start();
        while (isAlive(process)) {
            Thread.sleep(400);
            if (System.currentTimeMillis() > finish) {
                process.destroy();
                throw new Exception("Process timeout for command:"+cmdStr);
            }
        }
        OUTPUT = output(process.getInputStream());
        logger.info(OUTPUT);
        if (process.waitFor() == 0) {
            logger.info("Command Success!");
            return true;
        } else {
            logger.info("Command Failed!");
            return false;
        }
    }

    /**
     * Execute command remotely
     * @param cmdStr the command string
     * @param host the host that execute the command by RSH
     * @return the execution result
     * @throws Exception
     */
    public static boolean executeRSHCommandAsRoot(String cmdStr, String host)
            throws Exception {
        return executeLinuxCommand("rsh -l root " + host + " \"" + cmdStr + "\"", 30);
    }
    
    /**
     * Execute command remotely. Called by oozie user
     * @param cmdStr the command string
     * @param host the host that execute the command by RSH
     * @return the execution result
     * @throws Exception
     */
    public static boolean executeRSHCommandAsRootByOozie(String cmdStr, String host)
            throws Exception {
        return executeLinuxCommand("sudo -u oozie rsh -l root " + host + " \"" + cmdStr + "\"", 30);
    }

}
