package com.cisco.cim.oozie.util;

public class UserManager {
    public static String USER_PREFIX = "user_";
    public static String GROUP_PREFIX = "group_";
    public static String ROLE_PREFIX = "role_";
    private static String HUE_IMPALA_GROUPNAME = "group_hue_impala"; //the groupname for users that have acess to impala in hue
    private static String USER_PW_SETUP = "cisco123";
    public static String HUE_CMD_BIN = "/opt/cloudera/parcels/CDH/lib/hue/build/env/bin/hue";
    public static String HUE_ADMIN_PRINCIPAL = "cloudera-scm/admin";
    public static String TMP_HUE_PYTHON_FILE = "/tmp/etl.py";
    private static String ADMIN_ROLE_NAME = "role_admin";
    private static String CIM_SELECT_ROLE_NAME = "role_cim_select";
    
    /**
     * Add a user and group (based on database name) on the host
     * @param citydb the database name
     * @param hostname the host name
     * @return operating result
     * @throws Exception
     */
    public static boolean addUserGroupToHost(String citydb, String hostname) throws Exception {
        boolean s1 = CLITool.executeRSHCommandAsRoot("/usr/sbin/groupadd -f " + HUE_IMPALA_GROUPNAME, hostname);
        String username = USER_PREFIX + citydb;
        String groupname = GROUP_PREFIX + citydb;
        boolean s2 = CLITool.executeRSHCommandAsRoot("/usr/sbin/groupadd -f " + groupname, hostname);
        boolean s3 = false;
        if (s1 && s2) {
            s3 = CLITool.executeRSHCommandAsRoot("id " + username, hostname);
            boolean userNotExist = CLITool.OUTPUT.toLowerCase().contains("no such user");
            if (!userNotExist) {
                return true;
            }
            if (s3) {
                s3 = CLITool.executeRSHCommandAsRoot("/usr/sbin/useradd " + username + " -g " + groupname + " -G "+ groupname +"," + HUE_IMPALA_GROUPNAME, hostname);
            }
        }
        return s3;
    }
    
    /**
     * Add a principal to the KDC
     * @param principal the pricipal name
     * @param hostname the host name of the KDC
     * @param keytab the keytab file which contains the admin user
     * @return operating result
     * @throws Exception
     */
    public static boolean addUserPrincipalToKDC(String principal, String hostname, String keytab) throws Exception {
        String getprincCMD = "'getprinc "+ principal+ "'";
        boolean s1 = CLITool.executeRSHCommandAsRoot("/usr/bin/kadmin -p "+ HUE_ADMIN_PRINCIPAL +" -kt "+ keytab +" -q "+ getprincCMD, hostname);
        boolean principalNotExist = CLITool.OUTPUT.contains("Principal does not exist");
        boolean s2 = false;
        if (s1) {
            if (!principalNotExist) {
                return true;
            }
            String passwd = USER_PW_SETUP;
            String addprincCMD = "'addprinc -pw "+ passwd + " " + principal+"'";
            s2 = CLITool.executeRSHCommandAsRoot("/usr/bin/kadmin -p "+ HUE_ADMIN_PRINCIPAL +" -kt "+ keytab +" -q "+ addprincCMD, hostname);
        }
        return s2;
    }
    
    /**
     * Create select role of specific database and assign to the generated user and group
     * @param citydb the database name
     * @param keytab the keytab file
     * @param hiveURL the URL used to connect to Hive
     * @return operating result
     * @throws Exception
     */
    public static boolean addRoleBasedAcessControl(String citydb, String keytab, String hiveURL) throws Exception {
        String dbname = citydb;
        String rolename = ROLE_PREFIX + citydb;
        String groupname = GROUP_PREFIX + citydb;
        DBManager dbm = new DBManager(hiveURL, null, keytab);
        dbm.getHiveConnection();
        boolean roleExist = dbm.isRoleExist(rolename);
        if (roleExist) {
            dbm.executeHiveCmd("drop role "+ rolename);
        }
        dbm.executeHiveCmd("create role "+ rolename);
        dbm.executeHiveCmd("grant select on database "+ dbname +" to role "+ rolename);
        dbm.executeHiveCmd("grant role "+ rolename +" to group " + groupname);
        dbm.closeHiveConnection();
        return true;
    }
    
    /**
     * Sync user and group of a host with Hue
     * @param citydb the database name
     * @param hostname the host name
     * @return operating result
     * @throws Exception
     */
    public static boolean syncUserGroupWithHue(String citydb, String hostname) throws Exception {
        String syncCMD = HUE_CMD_BIN + " useradmin_sync_with_unix";
        boolean s1 = CLITool.executeRSHCommandAsRoot(syncCMD, hostname);
        boolean s2 = false;
        if (s1) {
            String username = USER_PREFIX + citydb;;
            String passwd = USER_PW_SETUP;
            String createUserPythonScript = "from django.contrib.auth.models import User\n" +
                    "name = \'\\'\'"+ username + "\'\\'\'\n" +
                    "passwd = \'\\'\'"+ passwd + "\'\\'\'\n" +
                    "user = User.objects.get(username=name)\n" +
                    "if user is None:\n" +
                    "    User.objects.create_user(name, password=passwd)\n" +
                    "else:\n" +
                    "    user.set_password(passwd)\n" +
                    "    user.save()\n";//Last '\n' should not be ignored!
            s2 = CLITool.executeRSHCommandAsRoot("echo -e '" + createUserPythonScript + "'>"+ TMP_HUE_PYTHON_FILE, hostname);
            if (s2) {
                s2 = CLITool.executeRSHCommandAsRoot(HUE_CMD_BIN +" shell < " + TMP_HUE_PYTHON_FILE, hostname);
                CLITool.executeRSHCommandAsRoot("rm -rf " + TMP_HUE_PYTHON_FILE, hostname);
            }
        }
        return s2;
    }
    
    /**
     * Create admin role for all database and assign to specific group
     * @param serverName the server name of the database on Hive
     * @param groupName the specific group name
     * @param hiveURL the URL used to connect to Hive
     * @param keytab the keytab file that contains hive pricipal
     * @return operating result
     * @throws Exception
     */
    public static boolean createAdminRoleForGroup(String serverName, String groupName, String hiveURL, String keytab) throws Exception {
        String rolename = ADMIN_ROLE_NAME;
        DBManager dbm = new DBManager(hiveURL, null, keytab);
        dbm.getHiveConnection();
        boolean roleExist = dbm.isRoleExist(rolename);
        if (!roleExist) {
            dbm.executeHiveCmd("create role "+ rolename);
            dbm.executeHiveCmd("grant all on server "+ serverName +" to role " + rolename);
        }
        dbm.executeHiveCmd("grant role "+ rolename +" to group " + groupName);
        dbm.closeHiveConnection();
        return true;
    }
    
    /**
     * Create admin role for all database and assign to specific group
     * @param groupName the specific group name
     * @param hiveURL the URL used to connect to Hive
     * @param keytab the keytab file that contains hive pricipal
     * @return operating result
     * @throws Exception
     */
    public static boolean createCIMSelectRoleForGroup(String groupName, String hiveURL, String keytab) throws Exception {
        String rolename = CIM_SELECT_ROLE_NAME;
        DBManager dbm = new DBManager(hiveURL, null, keytab);
        dbm.getHiveConnection();
        boolean roleExist = dbm.isRoleExist(rolename);
        if (roleExist) {
            dbm.executeHiveCmd("drop role "+ rolename);
        }
        dbm.executeHiveCmd("create role "+ rolename);
        dbm.executeHiveCmd("grant select on database cim to role " + rolename);
        dbm.executeHiveCmd("grant role "+ rolename +" to group " + groupName);
        dbm.closeHiveConnection();
        return true;
    }
    
    /**
     * Add a group to Hue which has access to Impala module in Hue
     * @param hostname the host name
     * @return operating result
     * @throws Exception
     */
    public static boolean addImpalaAccessGroupInHue(String hostname) throws Exception {
        boolean s = false;
        String createUserPythonScript = "from useradmin.models import HuePermission,GroupPermission\n" +
                "from django.contrib.auth.models import Group\n" +
                "group = Group.objects.get(name=\'\\'\'"+ HUE_IMPALA_GROUPNAME + "\'\\'\')\n" +
                "perm = HuePermission.objects.get(app=\'\\'\'impala\'\\'\', action=\'\\'\'access\'\\'\')\n" +
                "GroupPermission.objects.create(group=group, hue_permission=perm)\n";//Last '\n' should not be ignored!
        s = CLITool.executeRSHCommandAsRoot("echo -e '" + createUserPythonScript + "'>"+ TMP_HUE_PYTHON_FILE, hostname);
        if (s) {
            s = CLITool.executeRSHCommandAsRoot(HUE_CMD_BIN +" shell < " + TMP_HUE_PYTHON_FILE, hostname);
            CLITool.executeRSHCommandAsRoot("rm -rf " + TMP_HUE_PYTHON_FILE, hostname);
        }
        return s;
    }
    
    /**
     * Sync the keytab file with another host
     * @param keytabPath the folder of the keytab file
     * @param keytabFile the keytab file to sync
     * @param hostname the host name to sync with
     * @return operating result
     * @throws Exception
     */
    public static boolean syncKeytabFileToHost(String keytabPath, String keytabFile, String hostname) throws Exception {
        String cmdPrepare = "rm -rf "+ keytabPath + ";mkdir -p "+ keytabPath +";chown -R oozie:oozie " + keytabPath;
        boolean s1 = CLITool.executeRSHCommandAsRootByOozie(cmdPrepare, hostname);
        boolean s2 = false;
        if (s1) {
            String cmdSync = "sudo -u oozie /usr/bin/rsync -avz --rsh='/usr/bin/rsh -l root' "+ keytabPath + " " + hostname +":"+ keytabPath;
            s2 = CLITool.executeLinuxCommand(cmdSync, 30);
        }
        return s2;
    }

}
