package com.cisco.cim.oozie.action;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.cisco.cim.oozie.util.HDFSAccessor;
import com.cisco.cim.oozie.util.UserManager;
import org.apache.log4j.Logger;
/**
 * The Action class used for oozie action 'check-user'.
 * Used to add the user&group are generated for a specific city.
 * Also it will check the principal in KDC, roles in Hive and user&group in Hue 
 * @author donwu
 *
 */
public class CheckUserAction {
    private static final String CHECKEDCITYLIST = "checkedcities";
    private static final String TOCHECKCITYLIST = "tocheck";
    private static final Logger logger = Logger.getLogger(CheckUserAction.class);
  
    public static void main(String[] args) throws Exception {
        String configFileURL = args[0];
        String allHosts = args[1];
        String keytabFile = args[2];
        String hueHost = args[3];
        String hiveURL = args[4];
        
        HDFSAccessor accessor = new HDFSAccessor();
        Properties configProperties = accessor.readProperties(configFileURL);
        try {
            String checkedCities = configProperties.getProperty(CHECKEDCITYLIST);
            String uncheckedCities = configProperties.getProperty(TOCHECKCITYLIST);
            List<String> cityList = Arrays.asList(uncheckedCities.replaceAll(" ", "").split(","));
            List<String> hostList = Arrays.asList(allHosts.replaceAll(" ", "").split(","));
            String checked = null;
            for (String city : cityList) {
                if (city.length()==0) {
                    continue;
                }
                boolean addToHost = false;
                boolean addToKDC = false;
                boolean addRBAC = false;
                boolean addToHue = false;
                // Add a user and group (based on the database name) on the host
                for (String host : hostList) {
                    if (host.length()==0) {
                        continue;
                    }
                    logger.info("Check user&group for city "+ city +" on host "+ host+"..");
                    addToHost = UserManager.addUserGroupToHost(city, host);
                    if (!addToHost) {
                        throw new Exception("Error in checking user&group for city "+ city +" from host "+ host);
                    }
                    logger.info("Check done!");
                }
                // Add a principal to the KDC
                if (addToHost) {
                    logger.info("Check principal for city "+ city +" on KDC..");
                    String principal = UserManager.USER_PREFIX+city;
                    addToKDC = UserManager.addUserPrincipalToKDC(principal, hueHost, keytabFile);
                    if (!addToKDC) {
                        throw new Exception("Error in checking user principal "+ principal +" on KDC from host "+ hueHost);
                    }
                }
                // Create select role of specific database and assign to the generated user and group
                if (addToKDC) {
                    logger.info("Create Role to do RBAC for city "+ city +"..");
                    addRBAC = UserManager.addRoleBasedAcessControl(city, keytabFile, hiveURL);
                    if (!addRBAC) {
                        throw new Exception("Error in creating role to do RBAC for city "+ city);
                    }
                }
                // Sync user and group of a host with Hue
                if (addToHost && addToKDC && addRBAC) {
                    logger.info("Sync user&group for city "+ city +" with Hue..");
                    addToHue = UserManager.syncUserGroupWithHue(city, hueHost);
                    if (!addToHue) {
                        throw new Exception("Error in syncing user&group "+ UserManager.USER_PREFIX +city +" with Hue from host "+ hueHost);
                    }
                    else {
                        if (checked == null) {
                            checked = city;
                        }
                        else {
                            checked+=","+ city;
                        }
                    }
                }
            }
            // Add a group to Hue which has access to Impala module in Hue
            boolean addImpalaAccessGroup = UserManager.addImpalaAccessGroupInHue(hueHost);
            if (!addImpalaAccessGroup) {
                throw new Exception("Error in adding impala access group in Hue from host " + hueHost);
            }
            if (checked !=null) {
                logger.info("Successfully checked cities:" + checked);
                if (!checkedCities.equals("null")) {
                    checked = checkedCities + "," + checked;
                }
                configProperties.setProperty(CHECKEDCITYLIST, checked);
                configProperties.setProperty(TOCHECKCITYLIST, "null");
                accessor.writeProperties(configProperties, configFileURL, "etl settings");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {

        }
    }
    
}
