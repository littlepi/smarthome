package com.cisco.cim.oozie.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

//import javax.security.auth.*;
//import javax.security.auth.login.*;






import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;

import com.cisco.cim.jsonparser.Schema;
import com.cisco.cim.oozie.util.DBManager;
import com.cisco.cim.oozie.util.ETL;

public class ETLTest {

    public static void main(String[] args) throws Exception {
        Schema schema = new Schema();
        Map<String, Map<String, Object>> top = schema.getHBaseTables();
        System.out.println(top);
        
        
        
        System.setProperty("java.security.krb5.realm", "CIM.IVSG.AUTH");
        System.setProperty("java.security.krb5.kdc", "cm-hue-01.novalocal");
        Configuration conf = new Configuration();
        conf.set("hadoop.security.authentication", "Kerberos");
        UserGroupInformation.setConfiguration(conf);
        
        DBManager dbm = new DBManager("jdbc:hive2://cm-hue-01.novalocal:10000/;principal=hive/cm-hue-01.novalocal@CIM.IVSG.AUTH",
                "jdbc:hive2://cm-hue-01.novalocal:21050/;principal=impala/cm-hue-01.novalocal@CIM.IVSG.AUTH",
                "/Users/chao/workspace/etl_230.keytab");




        String startTime = "1510623800000";
        String endTime =   "1512323800000";
        
        ETL etl = new ETL(dbm, top);
        //etl.dropAllDatabases();
        etl.initTables(top,"null");
        etl.ETLloop(top,"", startTime,endTime,endTime,endTime,"" );
        //String a =etl.getInitTime();
        //etl.initLocations(top);
      
    }
    
}
