package com.cisco.cim.oozie.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateProcessor {
    private static String dateFormatGMT = "yyyy-MM-dd'T'HH:mmZ";

    /**
     * Decide whether a String represents a valid TimeZone
     * @param tzId the String of TimeZone ID
     * @return whether the String represents a valid TimeZone
     */
    public static Boolean isValidTimeZone(String tzId) {
        TimeZone tz = TimeZone.getTimeZone(tzId);
        return tz.getID().equals(tzId);
    }

    /**
     * Parse a String to Date
     * @param s the String to parse
     * @return the parsed Date
     * @throws Exception
     */
    public static Date parseDateFromString(String s) throws Exception {
        DateFormat df = new SimpleDateFormat(dateFormatGMT);
        return df.parse(s);
    }
    
    /**
     * Parse a long value to the Date in String
     * @param l the value to parse
     * @param tz the time zone
     * @return the parsed result
     * @throws Exception
     */
    public static String parseDateStringFromLong(long l, TimeZone tz) throws Exception {
        Date date = new Date(l);
        DateFormat df = new SimpleDateFormat(dateFormatGMT);
        df.setTimeZone(tz);
        return df.format(date);
    }

}
