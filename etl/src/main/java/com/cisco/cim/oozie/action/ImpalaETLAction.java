package com.cisco.cim.oozie.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import com.cisco.cim.jsonparser.Schema;
import com.cisco.cim.oozie.util.DBManager;
import com.cisco.cim.oozie.util.ETL;
import com.cisco.cim.oozie.util.HDFSAccessor;
import com.cisco.cim.oozie.util.DateProcessor;
import com.cisco.cim.oozie.util.RoundOperation;

import org.apache.log4j.Logger;


public class ImpalaETLAction {
    private static final Logger logger = Logger.getLogger(ImpalaETLAction.class);
    private static final String PROCESSEDDATE = "processstart";
    private static int FREQ = 10 * 60 * 1000;
    private static int SKIPFREQ = 10 * 60 * 1000;
    private static int MAXPROCESS = 100000;
    private static String TIME_ZONE = "GMT+0800";
    private static ETL etl;
    private static Map<String, Map<String, Object>> top;
    private static final String CHECKEDCITYLIST = "checkedcities";
    private static final String TOCHECKCITYLIST = "tocheck";
    private static final String CHECKUSER = "DOCHECK";
    private static final String DONOTCHECK = "DONOTCHECK";
    private static final String AGGREGATIONTIME = "aggregationtime";
    private static final String AGGREGATIONRESERVEDTIME = "aggregationreservedtime";
    private static final String MAXHOURSPERROUND = "maxhourlytimesperround";
    private static final String MAXRECEIVEPERROUND = "maxnormaltimesperround";
    private static final String ROLLINGTIME = "rollingtime";
    private static final String AUTO = "auto";
    private static final String FIRST = "first";
    
    /**
     * The start time of the hourly table handling is set according to the minimal sampletimestamp 
     * @return the start sample timestamp
     * @throws Exception
     */
    private static Long getInitSampleTime() throws Exception {
	return etl.getInitSampleTime() - 3600*1000;    
    }


    
    /**
     * Check with the city list in database and get the unchecked city list
     * @param checkedcitylist the checked city list
     * @return the unchecked city list formatted in String
     * @throws Exception
     */
    private static String getUncheckedCityInDB(String checkedcitylist) throws Exception {
       	String dataDB = etl.getDataDBName();
	if (checkedcitylist.equals("null")) {
		String ret = dataDB.toLowerCase();
		return ret;
	} else {
		String ret = null;
		List<String> cl = Arrays.asList(checkedcitylist.replaceAll(" ", "").split(","));
			if (!cl.contains(dataDB.toLowerCase())) {
				ret = dataDB.toLowerCase();
			}
		return ret;
	}
    }
    
    
    private static String round(long startTime,String rollingMonth,DBManager dbm) throws Exception{
    	Calendar calUTC = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    	calUTC.setTime(new Date( startTime  -  24*3600*1000));
    	int year = calUTC.get(Calendar.YEAR);
    	int month = calUTC.get(Calendar.MONTH) + 1;
    	int ryear = Integer.parseInt(rollingMonth.substring(0,4));
    	int rmonth = Integer.parseInt(rollingMonth.substring(4,6));
    	if(year*12 + month -1 > ryear*12 + rmonth ){
    		if(rmonth == 12){
    			RoundOperation.round(ryear+1, 1 , dbm);		
    			return String.valueOf(ryear+1) + "01";
    		}else{
    			RoundOperation.round(ryear, rmonth+1 , dbm);
    			return String.valueOf(ryear+1) + String.format("%02d",rmonth+1);
    		}
    	}
    	return "";
    } 
    
    
    public static void main(String[] args) throws Exception{ 
    	
        String configFileURL = args[0];
        String end = args[1];
        String hiveURL = args[2];
        String impalaURL = args[3];
        String freq = args[4];
        String skipFreq = args[5];
        String maxProcess = args[6];
        String timeZone = args[7];
        String keytabFile = args[8];
        String resourceDir = args[9];
        String configFileURLBak = configFileURL + ".bak";

        HDFSAccessor accessor = new HDFSAccessor();
        Properties configProperties = accessor.readProperties(configFileURL);
        if(configProperties.isEmpty())
        configProperties = accessor.readProperties(configFileURLBak);
 
        
        String rollingtime = configProperties.getProperty(ROLLINGTIME);
        if(rollingtime == null){
        	SimpleDateFormat df = new SimpleDateFormat("yyyyMM");
        	Calendar ca = Calendar.getInstance();
        	ca.setTime(new Date());  
        	ca.add(Calendar.MONTH, -2); 
        	Date lastMonth = ca.getTime(); 
        	rollingtime = df.format(lastMonth);
        	configProperties.setProperty(ROLLINGTIME, rollingtime);
        } 
        
        String ooziePropFile = System.getProperty("oozie.action.output.properties");
        File oozieFile = new File(ooziePropFile);
        Properties oozieProps = new Properties();
        if (ooziePropFile == null) {
            throw new Exception("System property oozie.action.output.properties not defined");
        }
        try {
            if (freq != null) {
                FREQ = Integer.valueOf(freq)*60*1000;
            }
            if (skipFreq != null) {
                SKIPFREQ = Integer.valueOf(skipFreq)*60*1000;
            }
            if (maxProcess != null) {
                MAXPROCESS = Integer.valueOf(maxProcess);
            }
            if (timeZone != null) {
                TIME_ZONE = timeZone;
            }
            
            Date currentDate = new Date();
            long currentTimestamp = currentDate.getTime();
            String current = DateProcessor.parseDateStringFromLong(currentTimestamp, TimeZone.getTimeZone(TIME_ZONE));
            
            String start = configProperties.getProperty(PROCESSEDDATE);
            long startTimestamp = DateProcessor.parseDateFromString(start).getTime();
            
            
            long endTimestamp;
            if(startTimestamp == 0){
            	endTimestamp = DateProcessor.parseDateFromString(end).getTime();
            }else{
            	endTimestamp = startTimestamp + FREQ;
            }	   

            String aggr = configProperties.getProperty(AGGREGATIONTIME);
            String aggrReservedStr = configProperties.getProperty(AGGREGATIONRESERVEDTIME);
            int aggrReserved = Integer.parseInt(aggrReservedStr);
            String maxHoursStr = configProperties.getProperty(MAXHOURSPERROUND);
            int maxHours = Integer.parseInt(maxHoursStr);
            String maxReceiveStr = configProperties.getProperty(MAXRECEIVEPERROUND);
            int maxReceive = Integer.parseInt(maxReceiveStr);
            String autoStr = configProperties.getProperty(AUTO);
            String firsttime = configProperties.getProperty(FIRST);
            long aggrTimestamp = DateProcessor.parseDateFromString(aggr).getTime();
            if (endTimestamp < currentTimestamp) {
                Schema schema = new Schema();
                top = schema.getHBaseTables();
                DBManager dbm = new DBManager(hiveURL, impalaURL, keytabFile);
                String rollingMonth = "";
                try{
                	rollingMonth = round(startTimestamp,rollingtime,dbm);
                }catch(Exception e){
                	logger.error(e.getMessage());
                }
               	
                if(!rollingMonth.equals("")){
                	configProperties.setProperty(ROLLINGTIME, rollingMonth);
                }
                etl = new ETL(dbm, top);
                // init databases and tables
                etl.initTables(top,firsttime);
                // If the process is falling behind, decide whether to skip some loops to speed up the ETL
                if ( currentTimestamp - endTimestamp > FREQ ) {
                    logger.info("End time before update: " + end + "(" + endTimestamp + ")");
                    int skipMaxTimes = maxReceive; //Force jumping out of loop
                    while ( skipMaxTimes>0 ) {
                        skipMaxTimes--;
                        if (endTimestamp + SKIPFREQ < currentTimestamp) {
                            endTimestamp += SKIPFREQ;
                        }
                        else if ( endTimestamp + FREQ < currentTimestamp ) {
                            endTimestamp += FREQ;
                        }
                        else {
                            break;
                        }
                    }
                }
                end = DateProcessor.parseDateStringFromLong(endTimestamp, TimeZone.getTimeZone(TIME_ZONE));
                logger.info("End time is updated to " + end + "(" + endTimestamp + ")");
                
                //Decide whether to check user on all hosts
                String uncheckedret = getUncheckedCityInDB(configProperties.getProperty(CHECKEDCITYLIST));
                if ( uncheckedret != null) {
                    oozieProps.setProperty("check.decision", CHECKUSER);
                    configProperties.setProperty(TOCHECKCITYLIST, uncheckedret);
                    logger.info("Will check user for:" + uncheckedret);
                }
                else {
                    oozieProps.setProperty("check.decision", DONOTCHECK);
                    logger.info("Will not check user");
                }
                OutputStream os = new FileOutputStream(oozieFile);
                oozieProps.store(os, "impala-etl action settings");
                os.close();
               
                // Calculate start and end time for houly aggregation and enter the ETL loop
                if(aggrTimestamp == 0 || autoStr.equals("true")){
                	aggrTimestamp = (long)(Math.floor(getInitSampleTime()/3600/1000)*3600*1000);
                }
                Long aggrEndTime = aggrTimestamp + 3600*1000;
                int count = maxHours;
                if (endTimestamp - aggrEndTime >= 1000*aggrReserved) {
                	while(endTimestamp - aggrEndTime >= 1000*aggrReserved && count > 0){
                		aggrEndTime += 3600*1000;
                		count -= 1;
                	}
                	aggrEndTime -= 3600*1000;
                    	configProperties.setProperty(AGGREGATIONTIME, DateProcessor.parseDateStringFromLong(aggrEndTime, TimeZone.getTimeZone(TIME_ZONE)));
                	
                	etl.ETLloop(top, resourceDir,Long.toString(startTimestamp), Long.toString(endTimestamp), Long.toString(aggrTimestamp),Long.toString(aggrEndTime),firsttime);
                }
                else {
                	etl.ETLloop(top, resourceDir,Long.toString(startTimestamp), Long.toString(endTimestamp), Long.toString(-1) , Long.toString(-1),firsttime);
                }
                configProperties.setProperty(FIRST, "false");
                configProperties.setProperty(PROCESSEDDATE, end);
                accessor.writeProperties(configProperties, configFileURL, "etl settings");
                java.util.concurrent.TimeUnit.SECONDS.sleep(2);
                accessor.writeProperties(configProperties, configFileURLBak, "etl settings");
                logger.info("Workflow launches at " + current + ". Data is processed with start: " + start + "(" +startTimestamp + "), end: " + end + "(" + endTimestamp + ")");
            
                
            }
            else {
                logger.info("Workflow launches at " + current + ". End time is later than current time, process ignored.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            try {
                if (etl != null ) {
                    etl.closeConnections();
                }
            } catch (Exception e) {
                System.err.println("Error happens when close connection");
            }
        }
    }
    
}