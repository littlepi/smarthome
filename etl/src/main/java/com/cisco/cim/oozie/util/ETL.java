package com.cisco.cim.oozie.util;


import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.Map.Entry;


public class ETL {
    
	private static DBManager dbm; 
    private static String HBASESUFFIX = "_hbase";
    private static String LOCATIONSUFFIX = "_location";
    private static String PERIODSUFFIX = "_period";
    private static String BEFOREPREFIX = "before_";
    private static String DATEID = "datelist";
    private static String DATEIDHALF = "datelist_half";
    private static String DESTPREFIX = "nodest_";
    
    private static String locationTable = "locations";
    private static String locationID = "locationId";
    private static String dimensionID = "dimensionid";
    private static String offset = "timezoneoffset";
    private static String beforeDimensionID = "dimensionid_before";
    private static String afterDimensionID = "dimensionid";
    private static String defaultDB = "cim";
    private static String dataDB = "cimdata";
    private static String startTime = "";
    private static String stopTime = "";
    private static String aggStartTime;
    private static String aggStopTime;
    private static String firsttime;
    private static boolean aggFlag = true;
    private static boolean autoFlag = false;
    
    private static String familyName = "d";
    private static String POWERCONSUMPTION = "powerconsumption";
    
    private static int GAPTIME = 60;
    private static String TIMEZONE = "GMT+8";
    
    private static List<String> snsIncidentType =  new ArrayList<String>(){{
        add("Intrusion");
        add("TrainPlatformIntrusion");
        add("PersonIdentified");
        add("SuspiciousObject");
        add("Graffiti");
        add("Garbage");
        add("CameraHealth");
        add("Loitering");
        add("VehicleIdentified");
        add("Queue");
        add("SlipAndFall");
        add("FireAndSmoke");
        add("Fire");
        add("Smoke");
        add("Panic");
        add("CitizenAlert");
        add("Overstay");
        add("Keyword");
        add("Sentiment");
        add("Influencer");
        add("Crime");
        add("Environment");
        add("Theft");  
    }};
    
    private static List<String> parkingIncidentType =  new ArrayList<String>(){{
        add("NoParkingZoneViolation");
        add("PoorlyParkedViolation");
        add("ParkingPotentialExpiry");
        add("FareExpendedViolation");
        add("PotentialFareExpiry");
        add("ParkingFull");
        add("ParkingEmpty");
        add("LimitExceededViolation");
    }};
    
    private static List<String> trafficIncidentType =  new ArrayList<String>(){{
        add("Speeding");
        add("WrongWay");
        add("OverCrowding");
        add("Accident");
        add("VehicleStopped");
    }};
    
    private static List<String> mobilityIncidentType =  new ArrayList<String>(){{
        add("Speeding");
        add("WrongWay");
        add("OverCrowding");
    }};
    
    private static List<String> wasteIncidentType =  new ArrayList<String>(){{
        add("WasteBinCollectionMissedIncident");
        add("RidershipPathChangedIncident");
        add("WasteBinOverheatedIncident");
        add("WasteBinOverturnedIncident");
    }};
    
    private static List<String> environmentIncidentType =  new ArrayList<String>(){{
        add("ThresholdReached");
    }};
    
    
    private static List<String> commonEntry = new ArrayList<String>(){{
        add("Locations");
        add("LocationHierarchy");
        add("DateID");
    }};
    
    private static List<String> skipEntry = new ArrayList<String>(){{
        add("PersonalDevice");
    }};
    
    
    private static List<String> cityList = new ArrayList<String>();
    private static List<String> cityListForHourly = new ArrayList<String>();
    private static List<String> cityListForHalfHourly = new ArrayList<String>();
    
    private static List<String> newCityList = new ArrayList<String>();
    private static List<String> DBList = new ArrayList<String>();
    private static List<String> aggStateList = new ArrayList<String>();
    Map<String,String> aggStateMap = new HashMap<String, String>(); 
    private static List<String> aggUpdateList = new ArrayList<String>();
    Map<String,String> aggUpdateMap = new HashMap<String, String>(); 
    Map<String,String> hierarchyMap = new HashMap<String, String>(){};
    
    private static List<String> aggEventList = new ArrayList<String>(){{
    	add("parkingspot_event_hourly");
    	add("parkingarea_event_hourly");
    	add("roadsegment_event_hourly");
    }};
    private static List<String> eventHourlyList = new ArrayList<String>(){{
    	add("incident_event");
    	add("genericevent_event");
    	add("parkingincident_event");
    	add("trafficincident_event");
    	add("wasteincident_event");
    	add("environmentincident_event");
    	add("mobilityincident_event");
    }};
    
    private static List<String> stateHourlyList = new ArrayList<String>(){{
    	add("wastecollectionridership_state");
    }};
    
    Map<String,String> cityMap = new HashMap<String, String>();
    Map<String,String> aggEventMap = new HashMap<String, String>(){{
    	put("parkingspot_event_hourly_tables","noparkingzoneviolation,parkingpotentialexpiry,limitexceededviolation,fareexpendedviolation,potentialfareexpiry");
    	put("parkingspot_event_hourly_ids","parkingspotid,parkingspaceid");
    	put("parkingspot_event_hourly_linkid","parkingspotid");
    	put("parkingarea_event_hourly_tables","noparkingzoneviolation,limitexceededviolation,fareexpendedviolation");
    	put("parkingarea_event_hourly_ids","parkingareaid,parkingspaceid");
    	put("parkingarea_event_hourly_linkid","parkingareaid");
    	put("roadsegment_event_hourly_tables","accident,congestion,wrongway,speeding,vehiclestopped,constructionwork");
    	put("roadsegment_event_hourly_ids","roadsegmentid");
    	put("roadsegment_event_hourly_linkid","roadsegmentid");
    }}; 
    Map<String,String> eventHourlyMap = new HashMap<String, String>(){{
    	put("incident_event","incidenttype,thirdpartyid,severity,tenantid");
    	put("parkingincident_event","incidenttype,thirdpartyid,severity,parkingspotid,parkingspaceid,parkingareaid,tenantid");
    	put("trafficincident_event","incidenttype,thirdpartyid,severity,roadsegmentid,roadsegmentlaneid,tenantid");
    	put("mobilityincident_event","incidenttype,thirdpartyid,severity,roadsegmentid,roadsegmentlaneid,tenantid");
    	put("wasteincident_event","incidenttype,thirdpartyid,severity,tenantid");
    	put("environmentincident_event","incidenttype,thirdpartyid,severity,tenantid");
    	put("genericevent_event","eventtype,source,thirdpartyid,severity,tenantid");
    }};
    
    Map<String,String> stateHourlyMap = new HashMap<String, String>(){{
    	put("wastecollectionridership_state","distance,totalwastecollected,binsvisited");
    }};
    Map<String,String> timezoneMap = new HashMap<String, String>(); 
    
    private static List<String> connectValues = new ArrayList<String>(){{
    	add("sid:string");
    	add("entityId:string");
    	add("entityType:string");
    	add("name:string");
    	add("sampletimestamp:bigint");
    	add("receivetimestamp:bigint");
    	add("destroytimestamp:bigint");
    	
    }};
    private static List<String> aggEventValues = new ArrayList<String>(){{
    	add("locationid:string");
    	add("city:string");
    	
    	add("timeid:int");
    	add("year:int");
    	add("month:int");
    	add("week:int");
    	add("weekday:int");
    	add("monthweek:int");
    	add("day:int");
    	add("hour:int");	
    }};
    private static List<String> aggCommonValues = new ArrayList<String>(){{
    	add("sid:string");
    	add("tenantid:string");
    	add("count:int");
    	add("dimensionid:bigint");
    	addAll(aggEventValues);
    }};
    private static List<String> aggUpdateValues = new ArrayList<String>(){{
    	add("_true_session_sum:bigint");
    	add("_true_session_max:bigint");
    	add("_true_session_min:bigint");
    	add("_false_session_sum:bigint");
    	add("_false_session_max:bigint");
    	add("_false_session_min:bigint");
    	add("_delta_true_count:int");
    	add("_delta_false_count:int");
    	add("_true_session_count_0_5:int");
    	add("_true_session_count_5_30:int");
    	add("_true_session_count_30_60:int");
    	add("_true_session_count_60_120:int");
    	add("_true_session_count_120_:int");
    	add("_false_session_count_0_5:int");
    	add("_false_session_count_5_30:int");
    	add("_false_session_count_30_60:int");
    	add("_false_session_count_60_120:int");
    	add("_false_session_count_120_:int");
    }};
    private static List<String> aggUpdateNumber = new ArrayList<String>(){{
    	add("0,5");
    	add("5,30");
    	add("30,60");
    	add("60,120");
    	add("120,-1");
    }}; 
    private static List<String> locList = new ArrayList<String>(){{
    	add("city");
    }};    
	List<String> timeList = new ArrayList<String>(){{
		add("hour");
		add("day");
		add("weekday");
		add("week");
		add("monthweek");
		add("month");
		add("year");
	}};
    Map<String,String> tableFlags = new HashMap<String, String>(); 
   
    private static List<String> extraStateTableList= new ArrayList<String>(){{
    }};
    Map<String,String> extraStateTableMap= new HashMap<String, String>(){{
    }};
    
    private static List<String> dimSeq = new ArrayList<String>(){{
        add("cim.ParkingSpace");
        add("cim.ParkingSpot");
        add("cim.Light");
        add("cim.RoadSegment");
        add("cim.EnvironmentSensor");
        add("cim.MobilityROI");
        add("cim.MobilityPOM");
        add("cim.SpatialMobilityStats");
        add("cim.DirTemporalMobilityStats");
        add("cim.WasteDumpSpace");
        add("cim.WasteSpace");
        add("cim.WasteBin");      
        add("cim.WasteCollectionTrip");
        add("cim.WasteCollectionRidership");
        add("cim.TransitTrip");      
        add("cim.Stop");      
        add("cim.StopTimes");      
        add("cim.Zone");      
        add("cim.Client");      
        add("cim.FareRules");      
        add("cim.TransitRidership");      
        add("cim.Agency");      
        add("cim.Route");      
        add("cim.Segment");      
        add("cim.Driver");      
        add("cim.Vehicle");      
        add("cim.ServiceCalendar");      
        add("cim.ServiceCalendarException");      
    }};
    
    
     
    /**
	 * 
	 * @param    dbmanager 	used to connect to impala,hive and call api to run sql command
	 * 			schema	    describe the structure and element of the tables         	
	 * @return   none 
	 * constructor function
	 *
	 */ 
    public ETL(DBManager dbm,Map<String, Map<String, Object>> schema) throws Exception {  
        this.dbm = dbm;
     	dbm.getConnections();
        
        this.defaultDB = ((String)schema.keySet().toArray()[0]).split("\\.")[0];
        
    }

    public String getDataDBName(){
    	return this.dataDB;
    }
    
    /**
     * 
	 * @param    none
	 * @return   city list 
	 * get the citylist(only city-named database) and databaselist(city-named database and default database) from the locations table
	 *
	 */ 
    
    public List<String> getCities() throws Exception{
        DBList.clear();
        cityList.clear();
        impalaCmd("invalidate metadata");
        
        boolean exist = dbm.isTableInDB(this.locationTable, this.defaultDB);
        if(!exist){
            return null;
        }
        String querySQL = "select distinct city from " + this.defaultDB + ".locations" ;
        ResultSet rs = dbm.executeImpalaCmdQuery(querySQL);
        while (rs.next()) {
        	cityMap.put(rs.getString(1).replace(" ", "_").replace("'", ""), rs.getString(1));
            cityList.add( rs.getString(1));
        }
        return cityList; 
    }

    
    /**
	 * 
	 * @param    none
	 * @return   the minimal receivetimestamp in the location table
	 * the start time of the etl process is set according to the minimal recevetimestamp in auto mode
	 *
	 */ 
    public String getInitTime() throws Exception{
    	return Long.toString(System.currentTimeMillis() - 3600*1000);
    }

    
    /**
	 * 
	 * @param    tableName
	 * @return   the minimal sampletimestamp in the table
	 * 	the start time of the hourly table handling is set according to the minimal sampletimestamp 
	 *
	 */ 
    public Long getInitSampleTime() throws Exception{
    		return (long) 0;
    }
   
    
    /**
	 * 
	 * @param    tablename
	 * @return   get the maximal sampletimestamp in the table
	 * 
	 * the maximal sampletimestamp is used to control the stop time of the hourly table handing
	 * the stop time of the hourly table handling should less than the maximal sampletimestamp
	 *
	 */ 
    public Long getMaxSampleTime(String tableName) throws Exception{
    	boolean exist = dbm.isTableInDB(tableName, this.defaultDB);
    	String table = this.defaultDB + "." + tableName;
    	if(!exist){
    		return (long) 0;
    	}
        String querySQL = "select max(sampleTimestamp) from " + table;
        ResultSet rs = dbm.executeImpalaCmdQuery(querySQL);
        rs.next();
        String result =  rs.getString(1);
        if(result == null)
            return (long) 0;
        return Long.parseLong(result);
    }
    
    public boolean isLocationHierarchySyncd() throws Exception{
        String querySQL = "select count(*) from cimdata.locations_hierarchy";
        ResultSet rs = dbm.executeImpalaCmdQuery(querySQL);
        rs.next();
        String result =  rs.getString(1);
        int dataLocationNumber =  Integer.parseInt(result);
        querySQL = "select count(*) from (select distinct entitytype,ancestorid,length,tenantid,entityid from "
        		 + "(select entitytype,ancestorid,length,tenantid,entityid from cimdata.locations_hierarchy "
        		 + " union select entitytype,ancestorid,length,tenantid,entityid from cim.locations_hierarchy) "
        		 + "as A ) as B ";
        rs = dbm.executeImpalaCmdQuery(querySQL);
        rs.next();
        result =  rs.getString(1);
        int commonLocationNumber =  Integer.parseInt(result);
        if(dataLocationNumber == commonLocationNumber){
        	return true;
        }else{
        	return false;
        }
    }
    
    public void setTimezoneMap() throws Exception{
    	String tableName = "locations";
    	boolean exist = dbm.isTableInDB(tableName, this.defaultDB);
    	String table = this.defaultDB + "." + tableName;
    	if(!exist){
    		return;
    	}
        String querySQL = "select distinct city,timezone from " + table;
        ResultSet rs = dbm.executeImpalaCmdQuery(querySQL);
        while(rs.next()){
        	this.timezoneMap.put( rs.getString(1), rs.getString(2));
        }
    }
    
    public Long getPersonalDeviceSampleTime(String type) throws Exception{
    	String tableName = "personaldevice_update_hbase";
    	boolean exist = dbm.isTableInDB(tableName, this.defaultDB);
    	String table = this.defaultDB + "." + tableName;
    	if(!exist){
    		return (long) 0;
    	}
        String querySQL = "select max(sampleTimestamp) from " + table;
        if(type.equals("current")){
        		querySQL +=  " where receivetimestamp >=" + this.startTime + " and receivetimestamp < " + this.stopTime;
        }else{
        	querySQL +=  " where receivetimestamp < " + this.startTime;
        }
        ResultSet rs = dbm.executeImpalaCmdQuery(querySQL);
        rs.next();
        String result =  rs.getString(1);
        if(result == null)
            return (long) 6000000;
        return Long.parseLong(result);
    }
    /**
	 * 
	 * @param    tableName
	 * 			 starttime
	 * 			 stopttime
	 * @return   the number of message of the table with recevetimestamp between starttime and stoptime 
	 *
	 */ 
    public int getMsgNumber(String tableName, String startTime, String stopTime) throws Exception{
    	boolean exist = dbm.isTableInDB(tableName, this.defaultDB);
    	String table = this.defaultDB + "." + tableName;
    	if(!exist){
    		return -1;
    	}
        String querySQL = "select count(*) from " + table +" where " ;
        querySQL+= table +".receiveTimestamp>="+startTime + " and "+table + ".receiveTimestamp<" + stopTime ;
        ResultSet rs = dbm.executeImpalaCmdQuery(querySQL);
        rs.next();
        String result =  rs.getString(1);
        if(result == null)
            return 0;
        return Integer.parseInt(result);	
    }

    /**
	 * 
	 * @param    tableName
	 * @param	 starttime
	 * @param	 stopttime
	 * @return   the number of message of the table with sampletimestamp between starttime and stoptime 
	 *
	 */ 
    public int getMsgSampleNumber(String tableName, String startTime, String stopTime) throws Exception{
    	boolean exist = dbm.isTableInDB(tableName, this.defaultDB);
    	String table = this.defaultDB + "." + tableName;
    	if(!exist){
    		return -1;
    	}
        String querySQL = "select count(*) from " + table +" where " ;
        querySQL+= table +".sampleTimestamp>="+startTime + " and "+table + ".sampleTimestamp<" + stopTime ;
        ResultSet rs = dbm.executeImpalaCmdQuery(querySQL);
        rs.next();
        String result =  rs.getString(1);
        if(result == null)
            return 0;
        return Integer.parseInt(result);	
    }
    
    
    private void impalaCmd(String cmd) throws SQLException{ 	
    	if(cmd.equals("")){
    		return;
    	}
        try{
        	dbm.executeImpalaCmd(cmd);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void hiveCmd(String cmd) throws SQLException{
        try{
        	dbm.executeHiveCmd(cmd);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    
    /**
	 * 
	 * @param   DBname
	 * @param	tablename
	 * @param	propertyMap			value map
	 * @param	mode		parquet table or not
	 * @return  none
	 *
	 */ 
    private void createImpalaTable(String DBName,String tableName, Map<String, String> propertyMap,String mode) throws ClassNotFoundException, SQLException{
    	tableName = tableName.replace(".", "_");
        if(mode.equals("parquet")){
        	propertyMap.remove("city");
        	propertyMap.remove("year");
        	propertyMap.remove("month");
        }else if(mode.equals("incident")){
        	propertyMap.remove("incidentType");
        }
        String cmd = "create table if not exists "+DBName + "." + tableName + "(";
        Set tmpSet = propertyMap.entrySet();         
        Iterator tmpIter = tmpSet.iterator();          
        
        while(tmpIter.hasNext()){
            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tmpIter.next();
            cmd += tmpEntry.getKey() + " " + tmpEntry.getValue() + ",";
        }
        
        cmd = cmd.substring(0,cmd.length()-1);
        cmd += ")"; 
        if(mode.equals("parquet")){
            cmd += " partitioned by (year int,month int,city string) stored as parquet ";
        }else if(mode.equals("roiid")){ 	
        	cmd += " partitioned by (roiid string) stored as parquet ";
        }else if(mode.equals("incident")){
        	cmd += " partitioned by (incidenttype string) stored as parquet ";
        }
        this.impalaCmd(cmd);  
        if(tableName.equals("Incident_event_period") || tableName.equals("Incident_event")){
        	this.impalaCmd(cmd.replaceAll("Incident_event", "parkingincident_event")); 
        	this.impalaCmd(cmd.replaceAll("Incident_event", "mobilityincident_event")); 
        	this.impalaCmd(cmd.replaceAll("Incident_event", "trafficincident_event")); 
        	this.impalaCmd(cmd.replaceAll("Incident_event", "wasteincident_event")); 
        	this.impalaCmd(cmd.replaceAll("Incident_event", "environmentincident_event")); 
        }
    }

    
    /**
	 * 
	 * @param   DBName
	 * @param 	tablename
	 * @param	rowkey	
	 * @param	propertyMap			 value map
	 * @param	mode				 "single" means not append "_hbase" to tablename  
	 * @return   none
	 *
	 */ 
    private void createHiveTable(String tableName, List<String> rowkey ,Map<String, String> propertyMap,String mode) throws ClassNotFoundException, SQLException{
        String hiveTableName = tableName.replace(".", "_");  
    	if(!mode.equals("single")){
        	 hiveTableName += this.HBASESUFFIX;
    	}
    	if(hiveTableName.split("_").length <= 2 && hiveTableName.endsWith("_hbase") 
    			&& !propertyMap.containsKey("d:locationId")){
    		propertyMap.put("d:locationId", "string");
    	}   	
    	
        String cmd = "create external table if not exists " + this.defaultDB + "." + hiveTableName + " ( " ;
        cmd += rowkey.get(0) + " " + rowkey.get(1) + ",";
        Set tmpSet = propertyMap.entrySet();         
        Iterator tmpIter = tmpSet.iterator();          
        
        while(tmpIter.hasNext()){
            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tmpIter.next();
            cmd += tmpEntry.getKey().split(":")[1] + " " + tmpEntry.getValue() + ",";
        }
        cmd = cmd.substring(0,cmd.length()-1);
        cmd += ") ROW FORMAT SERDE 'org.apache.hadoop.hive.hbase.HBaseSerDe' STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler' WITH SERDEPROPERTIES ('hbase.columns.mapping' = ':key,";
        
        tmpIter = tmpSet.iterator();          
        
        while(tmpIter.hasNext()){
            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tmpIter.next();
            cmd += tmpEntry.getKey() ;
            if(!tmpEntry.getValue().equals("string")){
                cmd += "#b" ;
            }
            cmd += ",";
        }
        cmd = cmd.substring(0,cmd.length()-1);
        cmd += "') TBLPROPERTIES('hbase.table.name' = '" + this.defaultDB + "."+ tableName + "')";
        this.hiveCmd(cmd);
    }   
    
        
    /**
	 * 
	 * @param    schema
	 * @return   none
	 * create databases; construct all table creating sqls and run them
	 * 
	 */ 
    public void initTables(Map<String, Map<String, Object>> schema,String firsttime) throws Exception{	
    	
	    Set genSet = schema.entrySet();         
	    Iterator genIter = genSet.iterator();          
	    String dbname = ((Map.Entry<String, Object>)schema.entrySet().toArray()[0]).getKey().split("\\.")[0];
	    this.createDatabase(this.defaultDB);
	    this.createDatabase(this.dataDB);

	    impalaCmd("invalidate metadata");
	    
	    //this iteration : get aggregation table list and corresponding values list
	    while(genIter.hasNext()){
	        Map.Entry<String, Object> entry=(Map.Entry<String, Object>)genIter.next();    
	        if(commonEntry.contains(entry.getKey().split("\\.")[1])){
	        	continue;
	        }
	              
	        String entryName = entry.getKey();
	        Map facMap =  ((Map)(((Map)entry.getValue()).get("factor")));
	        Map stateMap = (Map) facMap.get(entryName + ".state");
	        Map tmpStateMap;
	        
	        if(facMap != null){
	        	Iterator<Map.Entry<String, Object>> factorEntry = facMap.entrySet().iterator(); 
	        	while (factorEntry.hasNext()) { 
	        		Map.Entry<String, Object> myEntrys = factorEntry.next();
	        		if(myEntrys.getKey().startsWith(entry.getKey() + ".state." )){
	        			tmpStateMap=(Map)myEntrys.getValue();
	                    Set tmpStateSet =tmpStateMap.entrySet();
	                    Iterator iter = tmpStateSet.iterator();          
	                     
	                    int aggCount = 0;
	                    while(iter.hasNext()){
	                         Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)iter.next();
	                             if (((Map)tmpEntry.getValue()).keySet().contains("aggregate")){
	                                     aggCount += 1;
	                             }
	                    }
	                    if(aggCount > 0)
	                    	this.aggStateList.add(myEntrys.getKey()); 
	                    }
	        	}
	        } 
	        if(stateMap != null){
	        	 Set stateSet =stateMap.entrySet();
	        	 Iterator iter = stateSet.iterator();          
	             
	        	 int aggCount = 0;
	             while(iter.hasNext()){
	                 Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)iter.next();
	            	 if (((Map)tmpEntry.getValue()).keySet().contains("aggregate")){
	            		 aggCount += 1;
	            	 }
	             }
	             if(aggCount > 0)
	            	 this.aggStateList.add(entryName+".state"); 
	        }
	        Map updateMap = (Map) facMap.get(entryName + ".update"); 
	        if(updateMap != null){
	        	 Set updateSet =updateMap.entrySet();
	        	 Iterator iter = updateSet.iterator();                       
	        	 int aggCount = 0;
	        	 int aggNoStateCount = 0;
	        	 int aggStateCount = 0;
	        	 String stateValues = "";
	             while(iter.hasNext()){
	                 Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)iter.next();
	                 Map tmpMap = (Map)tmpEntry.getValue();
	            	 if (tmpMap.keySet().contains("aggregate")){
	            		 if(tmpMap.get("aggregate").equals("state")){
	            			 stateValues += tmpEntry.getKey() + ":" + tmpMap.get("type") + ",";
	            			 aggStateCount += 1;
	            		 }else if(tmpMap.get("aggregate").equals("update")){
	            			 aggNoStateCount += 1;
	            		 }else if (tmpMap.get("aggregate").equals("all")){
	            			 aggNoStateCount += 1;
	            			 stateValues += tmpEntry.getKey() + ":" + tmpMap.get("type") + ",";
	            			 aggStateCount += 1;
	            		 } 
	            		 aggCount += 1;
	            	 }                 
	             }
	             if(aggCount > 0){
	            	 if(aggNoStateCount > 0){
	            		 this.aggUpdateList.add(entryName+".update"); 
	            	 }
	            	 if(aggStateCount >0){
	            		 this.extraStateTableList.add(entryName);
	            		 this.extraStateTableMap.put(entryName, stateValues.substring(0,stateValues.length()-1));
	            	 }
	             }
	        }
	        
	    }
	   
	    //generate skip entry (locations dataid locationhierarchy) tables in default db
	    genIter = genSet.iterator();          
	    while(genIter.hasNext()){
	        Map.Entry<String, Object> entry=(Map.Entry<String, Object>)genIter.next();    
	        if(this.commonEntry.contains(entry.getKey().split("\\.")[1])){
	        		Map dimMap =  ((Map)(((Map)entry.getValue()).get("dimension")));
	        		Map tagMap =  ((Map)(((Map)entry.getValue()).get("tag")));         
	        		Map tagNullMap =  ((Map)(((Map)entry.getValue()).get("tag:null")));         
	                Set dimSet =  ((Map)(((Map)entry.getValue()).get("dimension"))).entrySet();         
	                Iterator dimIter = dimSet.iterator();          
	                String entryName = entry.getKey();
	                while(dimIter.hasNext()){
	                        Map.Entry<String, Object> dimEntry=(Map.Entry<String, Object>)dimIter.next(); 
	                        this.initDimension(dimEntry,tagMap,tagNullMap,"single");
	                }
	        }
	    }
	    
	    impalaCmd("invalidate metadata");
	
	    
	    
	    //generate skip entry (locations dataid locationhierarchy) tables in citys db
	    genIter = genSet.iterator();          
	    while(genIter.hasNext()){
	        Map.Entry<String, Object> entry=(Map.Entry<String, Object>)genIter.next();    
	        if(this.commonEntry.contains(entry.getKey().split("\\.")[1])){
	        		Map dimMap =  ((Map)(((Map)entry.getValue()).get("dimension")));
	        		Map tagMap =  ((Map)(((Map)entry.getValue()).get("tag")));         
	        		Map tagNullMap =  ((Map)(((Map)entry.getValue()).get("tag:null")));         
	                Set dimSet =  ((Map)(((Map)entry.getValue()).get("dimension"))).entrySet();         
	                Iterator dimIter = dimSet.iterator();          
	                String entryName = entry.getKey();
	                while(dimIter.hasNext()){
	                        Map.Entry<String, Object> dimEntry=(Map.Entry<String, Object>)dimIter.next(); 
	                        this.initDimension(dimEntry,tagMap,tagNullMap,"skip");
	                }
	        }
	    }
	    //gen event hourly
	    initEventHourlyTables();
	    initParkingTotalTable();
        initDateidTable();
        initParkingSpotUpdateTable();
        initMobilityInner();
        initPersonalDeviceTables();
        initParkingSessionTable();
	    
        initConnectionTable("cim.SpatialMobilityStats.connection");
        initAqiTables();
	    //generate factor and dimension tables
	    genIter = genSet.iterator();          
	    while(genIter.hasNext()){
	        Map.Entry<String, Object> entry=(Map.Entry<String, Object>)genIter.next();  
	        Map hierMap=  ((Map)(((Map)entry.getValue()).get("hierarchy")));
	        if(this.commonEntry.contains(entry.getKey().split("\\.")[1])){
	        	if(hierMap != null){
		        	initHierarchyTable(hierMap,entry.getKey());
		        }
	        	continue;
	        }

	        Map dimMap =  ((Map)(((Map)entry.getValue()).get("dimension")));
	        Map tagMap =  ((Map)(((Map)entry.getValue()).get("tag")));         
	        Map facMap =  ((Map)(((Map)entry.getValue()).get("factor")));
	        Map conMap=  ((Map)(((Map)entry.getValue()).get("connection")));
	        
	        if(conMap!=null){
	        	initConnectionTable((String)conMap.keySet().toArray()[0]);
	        }
	        
	        if(hierMap != null){
	        	initHierarchyTable(hierMap,entry.getKey());
	        }
	        
	        Map objMap = (Map)entry.getValue();
	        Set objSet = objMap.entrySet();         
	        Iterator objIter = objSet.iterator();          
	    
	        HashMap<String, String> tagList = new HashMap<String, String>();
	        HashMap<String, String> tagNullList = new HashMap<String, String>();
	        Map tagNullMap =  ((Map)(((Map)entry.getValue()).get("tag:null")));
	        Map.Entry<String, Object> tagEntry = null;
	        Map.Entry<String, Object> tagNullEntry = null;
	        int tagCount = 0;
	        
	        //handle tag list (tag,tag:<object> and tag:null)
	        while(objIter.hasNext()){
	            Map.Entry<String, Object> objEntry=(Map.Entry<String, Object>)objIter.next();    
	            String key = objEntry.getKey();
	            if ( (key.split(":")).length > 1 && (key.split(":"))[0].equals("tag") && !(key.split(":"))[1].equals("null") ){
	                tagCount += 1;
	                tagEntry = (Map.Entry<String, Object>)((Map)objEntry.getValue()).entrySet().toArray()[0];
	                tagList.put( objEntry.getKey().split(":")[1] + ":" +tagEntry.getKey() , this.familyName + ":" + (String) ((Map)tagEntry.getValue()).get("type"));
	                
	            }
	            if(key.equals("tag:null")){
	                Set tagNullSet = tagNullMap.entrySet();
	                Iterator tagNullIter = tagNullSet.iterator();          
	                while(tagNullIter.hasNext()){
	                    Map.Entry<String, Object> tmpEntry=(Map.Entry<String, Object>)tagNullIter.next();   
	                    tagNullList.put( tmpEntry.getKey() , this.familyName + ":" + (String) ((Map)tmpEntry.getValue()).get("type"));
	                }
	            }
	        }
	        
	        if(dimMap != null){
                Set dimSet =  ((Map)(((Map)entry.getValue()).get("dimension"))).entrySet();         
                Iterator dimIter = dimSet.iterator();          
    
                String entryName = entry.getKey();
                //traverse dimension entry, two situation(not main dimension or main dimension with tagcount zero) init dimension table
                while(dimIter.hasNext()){
                        Map.Entry<String, Object> dimEntry=(Map.Entry<String, Object>)dimIter.next();
                        if(!dimEntry.getKey().equals(entryName)){
                                this.initDimension(dimEntry,tagMap,tagNullMap,"full");
                        }
                        else if(tagCount == 0){
                                this.initDimension(dimEntry,tagMap,tagNullMap,"full");
                        }
                } 
	        }
	        
	        if(facMap.isEmpty() && tagCount == 0 ){
	        	continue;
	        }
	        
	        if(tagNullList.isEmpty()){
	        	tagNullList=null;
	        }  
	        
	        //generate special state table(such as roadsegment.state.vehicleclassvalue) 
	        if(facMap != null){
	        	Iterator<Map.Entry<String, Object>> facEntry = facMap.entrySet().iterator(); 
	        	while (facEntry.hasNext()) { 
	        		Map.Entry<String, Object> myEntry = facEntry.next();
	        		if(myEntry.getKey().startsWith(entry.getKey() + ".state." )){
	        			this.initInnerTable(entry,tagMap,tagList,tagNullList,myEntry.getKey().substring(entry.getKey().length()+1),true);
	        		}
	        		if(myEntry.getKey().equals(entry.getKey() + ".state" ))
	        			this.initInnerTable(entry,tagMap,tagList,tagNullList,"state",true);
	        		if(myEntry.getKey().equals(entry.getKey() + ".statistic" )){
	        			this.initStatisticTable(myEntry,tagMap,"");
	        		}else if(myEntry.getKey().equals(entry.getKey() + ".statistic.hourly" )){
	        			this.initStatisticTable(myEntry,tagMap,"hourly");
	        		}else if(myEntry.getKey().equals(entry.getKey() + ".statistic.daily" )){
	        			this.initStatisticTable(myEntry,tagMap,"daily");
	        		}
	        	}    	
	        }
	         
	        	
	        if(dimMap != null){
	            this.initInnerTable(entry,tagMap,tagList,null,"event",true);
	        	this.initInnerTable(entry,tagMap,tagList,null,"update",true);
	        }
	        


	        if(tagCount > 0){
	                if(dimMap != null)
	                {
	                        this.initOuterTable(entry,tagList,"dim");
	                }else{	    
	                	Map eventMap = (Map) facMap.get(entry.getKey() + ".event");
	                	if(eventMap != null){
	                        this.initOuterTable(entry,tagList,"event");
	                    }else{
	                    	this.initOuterTable(entry,tagList,"update");
	                    }
	                }
	        }else{
	            this.initInnerTable(entry,tagMap,tagList,null,"event",false);
	        }

	        
	    }   
	    impalaCmd("invalidate metadata");
	}



	private void initAqiTables() throws SQLException {
		String cmd = "";
		cmd="create table if not exists cimdata.AQIEntryData_state(sampleTimestamp bigint,code string,sources string,timezone string,"
			+"parentEntityType string,receiveTimestamp bigint,sid string,lastUpdated bigint,sampleTimestampID int,locationId string,timezoneoffset int,"
			+"attributeName string,value int,day int,sensitiveGroups string,announcement string,period string,advisory string,entityType string,"
			+"entityId string,pollutant string,effects string,tenantId string,category string,startTimestamp bigint) "
			+"partitioned by (year int,month int,city string) stored as parquet";
		impalaCmd(cmd);			
		cmd="create table if not exists cimdata.AQIEntry_statisticdata_hourly(code string,week int,sources string,timeid int,weekday int,parentEntityType string,"
			+"sid string,hour int,timezoneoffset int,attributeName string,value double,day int,sensitiveGroups string,announcement string,period string,monthweek int,advisory string,value_min double,"
			+"value_avg Double,entityType string,entityId string,pollutant string,effects string,locationid string,value_max double,tenantId string,category string,startTimestamp bigint,year int,month int,city string) stored as parquet ";
		impalaCmd(cmd);
		cmd ="create table if not exists cimdata.AQIEntry_statistic_hourly(geocoordinates_longitude double,code string,week int,sources string,timeid int,weekday int,parentEntityType string,"
			+"sid string,hour int,geocoordinates_latitude double,timezoneoffset int,attributeName string,value double,day int,sensitiveGroups string,announcement string,period string,monthweek int,advisory string,"
			+"value_min double,value_avg Double,entityType string,geocoordinates_altitude double,entityId string,pollutant string,effects string,locationid string,value_max double,tenantId string,category string,startTimestamp bigint,year int,month int,city string) stored as parquet ";
		impalaCmd(cmd);
	}

	private void initParkingSpotUpdateTable() throws ClassNotFoundException, SQLException {
		HashMap<String, String> parkingspotUpdateValues = new HashMap<String, String>(){{
				put("sid","string");
				put("parkingspaceid","string");
				put("tenantid","string");
				put("city","string");
				put("locationid","string");
				put("sampletimestamp_delta","bigint");
				put("sampletimestamp","bigint");
				put("timezoneoffset","bigint");
				put("occupied","int");
				put("sampletimestampid","int");
				put("year","int");
				put("month","int");
				put("expectedrevenue","double");		
	    }};	
	    this.createImpalaTable(this.dataDB, "parkingspot_update" , parkingspotUpdateValues,"");	
	}

	private void initParkingSessionTable() throws ClassNotFoundException, SQLException {
		HashMap<String, String> hourlyValues = new HashMap<String, String>(){{
			put("sid","string");
			put("tenantid","string");
			put("entityid","string");
			put("entitytype","string");
			put("parkingspaceid","string");
			put("turnover","boolean");
			
			put("sessiontime_sum","bigint");
			put("sessiontime_max","bigint");
			put("sessiontime_min","bigint");
			put("revenue_sum","double");
			put("revenue_max","double");
			put("revenue_min","double");
			put("count","int");			
			
        	put("year","smallint");
        	put("sampletimestamp","bigint");
        	put("month","smallint");
        	put("week","smallint");
        	put("weekday","smallint");
        	put("monthweek","smallint");
        	put("day","smallint");
        	put("hour","smallint");	
        	put("timeid","int");
        	put("locationid","string");
        	put("city","string");
    	}};
        this.createImpalaTable(this.dataDB, "parking_session_hourly" , hourlyValues,"");
	}


	private void initPersonalDeviceTables() throws ClassNotFoundException, SQLException {
		HashMap<String, String> dimValues = new HashMap<String, String>(){{			
			put("sid","string");
			put("tenantid","string");
			put("macaddress","string");		
			put("sampletimestamp","bigint");	
    	}};
		
		HashMap<String, String> updateValues = new HashMap<String, String>(){{			
			put("sid","string");
			put("tenantid","string");
			//put("roiid","string");
			put("macaddress","string");
			put("roiid_before","string");
			put("sampletimestamp_before","bigint");
			put("sampletimestamp","bigint");
			put("sampletimestampid","bigint");
			put("sampletimestamp_delta","bigint");
			put("receivetimestamp","bigint");
    	}};
    	
    	//create table if not exists cim_San_Diego.personaldevice_update_nonredundant(roiid string,macaddress string,
    	// sampletimestamp bigint)
    	HashMap<String, String> nonRedundantValues = new HashMap<String, String>(){{			
			//put("roiid","string");
			put("macaddress","string");
			put("sampletimestamp","bigint");
    	}};
    	
    	HashMap<String, String> periodValues = new HashMap<String, String>(){{			
			put("roiid","string");
			put("tenantid","string");
			put("macaddress","string");
			put("sampletimestamp","bigint");
			put("before_sampletimestamp","bigint");
			put("sampletimestampid","bigint");
    	}};
    	
    	HashMap<String, String> periodBounceValues = new HashMap<String, String>(){{	
    		put("sid","string");
    		put("tenantid","string");
			put("roiid","string");
			put("roiid_before","string");
			put("macaddress","string");
			put("sampletimestamp","bigint");
			put("before_sampletimestamp","bigint");
			put("sampletimestampid","bigint");
			put("sampletimestamp_delta","bigint");
    	}};
		
		HashMap<String, String> hourlyValues = new HashMap<String, String>(){{
			put("roiid","string");
			put("tenantid","string");
			put("first_count","bigint");
			put("repeat_count","bigint");
			put("sessiontime_count_0_2","bigint");
			put("sessiontime_count_2_10","bigint");
			put("sessiontime_count_10_20","bigint");
			put("sessiontime_count_20_30","bigint");
			put("count","bigint");
			
        	put("year","smallint");
        	put("sampletimestamp","bigint");
        	put("month","smallint");
        	put("week","smallint");
        	put("weekday","smallint");
        	put("monthweek","smallint");
        	put("day","smallint");
        	put("hour","smallint");	
        	put("timeid","int");
        	put("locationid","string");
        	put("city","string");
    	}};
        //this.createImpalaTable(this.dataDB, "personaldevice" , dimValues,"");
        this.createImpalaTable(this.dataDB, "personaldevice_update" , updateValues,"roiid");
        this.createImpalaTable(this.dataDB, "personaldevice_update_period" , periodValues,"");
        this.createImpalaTable(this.dataDB, "personaldevice_bounce_period" , periodBounceValues,"");
        this.createImpalaTable(this.dataDB, "personaldevice_update_nonredundant" , nonRedundantValues,"roiid");
        this.createImpalaTable(this.dataDB, "personaldevice_update_hourly" , hourlyValues,"");
        this.createImpalaTable(this.dataDB, "personaldevice_update_bounce_hourly" , hourlyValues,"");
		
		
	}


	private void initMobilityInner() throws SQLException {
		String[] tableList = "temporal_inner:spatial_inner".split(":");
		String cmd="";
        String cityName = this.dataDB;
        for(String s:tableList){
            	cmd = "create table if not exists "+ cityName + "." + s 
            			+"(sid string,tenantid string,entityid string,entitytype string,mobilitystatsname string,city string,"
            			+ s.split("_")[0] + "mobilitystatsname string)";
            	impalaCmd(cmd);
        }   
       		
	}

	private void initParkingTotalTable() throws SQLException, ClassNotFoundException {	
  		String cmd = "create table if not exists "+ this.dataDB + ".parkingspace_total(sid string,total int,tenantid string)";
           	impalaCmd(cmd);
	}

	private void syncParkingTotalTable() throws SQLException, ClassNotFoundException {
		String cmd = "insert overwrite table cimdata.parkingspace_total(sid,total,tenantid) select distinct sid,total,tenantid from cimdata.parkingspace_state";
	      	impalaCmd(cmd);
	}

	private void initEventHourlyTables() throws SQLException, ClassNotFoundException {
    	String cmd="";
        String cityName = this.dataDB;
        for(int j=0; j<aggEventList.size();j++){
            String tableName = aggEventList.get(j);
            cmd = "create table if not exists "+ cityName + "." + tableName + "(";
            for(int k=0; k<aggEventValues.size();k++){
            	cmd += aggEventValues.get(k).split(":")[0] + " " + aggEventValues.get(k).split(":")[1] + ",";
            }
            String eventTables =  aggEventMap.get(tableName+"_tables");
            for(int k=0; k< eventTables.split(",").length;k++){
            	cmd += eventTables.split(",")[k] + "_count int,";
            }
            String eventIds =  aggEventMap.get(tableName+"_ids");
            for(int k=0; k< eventIds.split(",").length;k++){
            	cmd += eventIds.split(",")[k] + " string,";
            }
            cmd = cmd.substring(0,cmd.length()-1) + ")";
            impalaCmd(cmd);
        }	
		HashMap<String, String> incidentHourlyValues = new HashMap<String, String>(){{
			put("incidenttype","string");
			put("thirdpartyid","string");
			put("severity","string");
			put("total_count","int");
			put("create_count","int");
			put("update_count","int");
			put("close_count","int");
			put("destroy_count","int");
			put("locationid","string");
			put("city","string");
			put("code","string");
			put("tenantid","string");
			
			put("year","smallint");
			put("month","smallint");
        	put("week","smallint");
        	put("weekday","smallint");
        	put("monthweek","smallint");
        	put("day","smallint");
        	put("hour","smallint");	
        	put("timeid","int");    	
    	}};
    	HashMap<String, String> trafficHourlyValues = new HashMap<String, String>();
    	HashMap<String, String> parkingHourlyValues = new HashMap<String, String>();
    	HashMap<String, String> wasteHourlyValues = new HashMap<String, String>();
    	HashMap<String, String> environmentHourlyValues = new HashMap<String, String>();
    	trafficHourlyValues.putAll(incidentHourlyValues);
    	parkingHourlyValues.putAll(incidentHourlyValues);
    	wasteHourlyValues.putAll(incidentHourlyValues);
    	environmentHourlyValues.putAll(incidentHourlyValues);
    	trafficHourlyValues.put("roadsegmentid", "string");
    	trafficHourlyValues.put("roadsegmentlaneid", "string");
    	parkingHourlyValues.put("parkingspotid", "string");
    	parkingHourlyValues.put("parkingspaceid", "string");
    	parkingHourlyValues.put("parkingareaid", "string");
    	
		HashMap<String, String> eventHourlyValues = new HashMap<String, String>(){{
			put("eventtype","string");
			put("source","string");
			put("thirdpartyid","string");
			put("severity","string");
			put("total_count","int");
			put("create_count","int");
			put("update_count","int");
			put("close_count","int");
			put("destroy_count","int");
		
			put("locationid","string");
			put("city","string");
			put("tenantid","string");
			
			put("year","smallint");
			put("month","smallint");
        	put("week","smallint");
        	put("weekday","smallint");
        	put("monthweek","smallint");
        	put("day","smallint");
        	put("hour","smallint");	
        	put("timeid","int");    	
    	}};
    	HashMap<String, String> wasteTripHourlyValues = new HashMap<String, String>(){{
			
			put("sid","string");
			put("tenantid","string");
			put("wastecollectiontripid","string");
			
			put("distance_max","double");
			put("distance_min","double");
			put("distance_sum","double");
			put("distance_avg","double");
			put("totalwastecollected_max","double");
			put("totalwastecollected_min","double");
			put("totalwastecollected_sum","double");
			put("totalwastecollected_avg","double");
			put("binsvisited_max","int");
			put("binsvisited_min","int");
			put("binsvisited_sum","bigint");
			put("binsvisited_avg","double");
			
		
			put("driverid","string");
			put("vehicleid","string");
			put("agencyid","string");
			
			put("locationid","string");
			put("city","string");
			put("count_sum","bigint");
			
			put("year","smallint");
			put("month","smallint");
        	put("week","smallint");
        	put("weekday","smallint");
        	put("monthweek","smallint");
        	put("day","smallint");
        	put("hour","smallint");
        	put("timeid","int");    	
    	}};
    	HashMap<String, String> wastebinStateHourlyValues = new HashMap<String, String>(){{
			
			put("sid","string");
			put("wastespaceid","string");
			put("tenantid","string");
			
			put("count","int");
			put("filllevel_max","double");
			put("filllevel_min","double");
			put("filllevel_avg","double");
			put("tilt_max","double");
			put("tilt_min","double");
			put("tilt_avg","double");
			put("weight_max","double");
			put("weight_min","double");
			put("weight_avg","double");	
			put("temperature_max","double");
			put("temperature_min","double");
			put("temperature_avg","double");	

			
			put("locationid","string");
			put("city","string");
			
			put("year","smallint");
			put("month","smallint");
        	put("week","smallint");
        	put("weekday","smallint");
        	put("monthweek","smallint");
        	put("day","smallint");
        	put("hour","smallint");
        	put("timeid","int");    	
    	}};
    	this.createImpalaTable(this.dataDB, "incident_event_hourly" , incidentHourlyValues,"");
    	this.createImpalaTable(this.dataDB, "parkingincident_event_hourly" , parkingHourlyValues,"");
    	this.createImpalaTable(this.dataDB, "trafficincident_event_hourly" , trafficHourlyValues,"");
    	this.createImpalaTable(this.dataDB, "mobilityincident_event_hourly" , trafficHourlyValues,"");
    	this.createImpalaTable(this.dataDB, "wasteincident_event_hourly" , wasteHourlyValues,"");
    	this.createImpalaTable(this.dataDB, "environmentincident_event_hourly" , environmentHourlyValues,"");
    	this.createImpalaTable(this.dataDB, "genericevent_event_hourly" , eventHourlyValues,"");	
    	this.createImpalaTable(this.dataDB, "wastecollectionridership_state_hourly" , wasteTripHourlyValues,"");
    	this.createImpalaTable(this.dataDB, "wastebin_state_hourly" , wastebinStateHourlyValues,"");
	}


	/**
	 * 
	 * @param   schema
	 * @param	starttime	start time of receivcetimestamp for no hourly tables
	 * @param	stoptime	stop time of receivetimestamp for no hourly tables
	 * @param	aggstarttime	start time of sampletimestamp for hourly tables
	 * @param	aggstoptime		stop time of sampletimestamp for hourly tables
	 * @return  none
	 * construct sqls of insert,join action and run them
	 * 
	 */ 
	public void ETLloop(Map<String, Map<String, Object>> schema,String url,String starttime,String stoptime,String aggstarttime,String aggstoptime,String firsttime ) throws Exception{
		if(url != ""){
			this.initIncidentType(url);
		}
		aggStateList.add("cim.WasteBin.state");
		aggStateMap.put("cim.WasteBin.state", "double:tilt,double:temperature,double:filllevel,double:weight,string:wastespaceid");
		this.getCities();
	    this.setTimezoneMap();
	    Set entitySet = schema.entrySet();
	    Iterator entityIter = entitySet.iterator(); 
	    this.startTime = starttime;
	    this.stopTime = stoptime;
	    this.firsttime = firsttime;
	    if(aggstarttime.equals("-1")){
	    	this.aggFlag = false;
	    }else{
	    	this.aggFlag = true;
	    	this.aggStartTime = aggstarttime;
	    	this.aggStopTime = aggstoptime;
	    }
	    //sync location and dateid table
	    syncLocation(schema.get(this.defaultDB+".Locations"));
	   
	    syncParkingTotalTable(); 

	    //add location id,block,street,timezone for dimension table
	    entityIter = entitySet.iterator();          
	    while(entityIter.hasNext()){
	        Map.Entry<String, Object> entry=(Map.Entry<String, Object>)entityIter.next();
	        
	        String entryName = entry.getKey();
	        if(this.commonEntry.contains(entryName.substring(this.defaultDB.length()+1)) || skipEntry.contains(entryName.substring(this.defaultDB.length()+1))){
	        	continue;
	        }
	        Map tagMap =  ((Map)(((Map)entry.getValue()).get("tag")));         
	        Map tagNullMap =  ((Map)(((Map)entry.getValue()).get("tag:null")));         
	        Map dimMap = (Map) ((Map) ((Map)entry.getValue()).get("dimension"));
	        Map<String, Object> multiTags = new HashMap<String, Object>(); 
	        Iterator objIter = ((Map)entry.getValue()).entrySet().iterator();          
	        int tagCount = 0;
	        while(objIter.hasNext()){
	            Map.Entry<String, Object> objEntry=(Map.Entry<String, Object>)objIter.next();    
	            String key = objEntry.getKey();
	            if ( (key.split(":")).length > 1 && (key.split(":"))[0].equals("tag") && !(key.split(":"))[1].equals("null") ){
	                tagCount += 1;
	                multiTags.put(key.split(":")[1], objEntry.getValue());
	            }
	        }
	        
	        if (dimMap != null){
	            Iterator dimIter = dimMap.entrySet().iterator();          
	            while(dimIter.hasNext()){
	                Map.Entry<String, Object> dimEntry=(Map.Entry<String, Object>)dimIter.next();
	                if(dimEntry.getKey().equals(entryName)){
	                	joinLocationTable(tagMap,tagNullMap,multiTags,dimEntry,"main");
	                }else{
	                	joinLocationTable(tagMap,null,multiTags,dimEntry,"other");
	                }
	            }    
	        }
	    }
	    
	   //join and sync for dimension tables        
	    entityIter = entitySet.iterator();          
	    while(entityIter.hasNext()){
	        Map.Entry<String, Object> entry=(Map.Entry<String, Object>)entityIter.next(); 
	        if(this.commonEntry.contains(entry.getKey().split("\\.")[1]) || skipEntry.contains(entry.getKey().split("\\.")[1])){
	            continue;
	        }
	        String entryName = entry.getKey();
	        String entityName =  (String) ((Map)schema.get(entryName)).get("entity");         
	        Map tagMap =  (Map) ((Map)schema.get(entryName)).get("tag");         
	        Map tagNullMap =  (Map) ((Map)schema.get(entryName)).get("tag:null");         
	        Map dimMap =  (Map) ((Map)schema.get(entryName)).get("dimension");  
	        Map mainDim = null;
	        if  (dimMap == null){
	        	continue;
	        }
	        mainDim = (Map) dimMap.get(entryName);
	        //Map<String, Object> multiTags = new HashMap<String, Object>(); 
	        Iterator objIter = ((Map)schema.get(entryName)).entrySet().iterator();          
	        int tagCount = 0;
	        List<String> multiTag = new ArrayList<String>();
	        while(objIter.hasNext()){
	            Map.Entry<String, Object> objEntry=(Map.Entry<String, Object>)objIter.next();    
	            String key = objEntry.getKey();
	            if ( (key.split(":")).length > 1 && (key.split(":"))[0].equals("tag") && !(key.split(":"))[1].equals("null") ){
	                tagCount += 1;
	                Map<String, Object> tmpTag = new HashMap<String, Object>();
	                tmpTag= (Map<String, Object>) objEntry.getValue();
	                
	                //multiTags.put(objEntry.getValue(), objEntry.getValue());
	                multiTag.add((String) tmpTag.keySet().toArray()[0]);
	                
	            }
	        }   

	            Iterator dimIter = dimMap.entrySet().iterator();  
	            while(dimIter.hasNext()){
	            	Map.Entry<String, Object> dimEntry=(Map.Entry<String, Object>)dimIter.next();   
	            	syncDimensionTable(tagMap,tagNullMap,multiTag,dimEntry,entryName);
	            }     
	    }
	    

	    updateDateidTable();
	    syncHierarchyTable();
	    syncAreaTable();
	    updateInnerTable();    
	    updatePlusTable();
	    //updatePersonalDeviceTable();
	    
	    Calendar calStart;
	    Calendar calEnd;
	    Calendar calUTC;
	  
	    //join and sync for factor tables
	    entityIter = entitySet.iterator();          
	    while(entityIter.hasNext()){
	        Map.Entry<String, Object> entry=(Map.Entry<String, Object>)entityIter.next(); 
	        if(this.commonEntry.contains(entry.getKey().split("\\.")[1]) || skipEntry.contains(entry.getKey().split("\\.")[1])){
	            continue;
	        }
	        String entryName = entry.getKey();
	        Map tagMap =  ((Map)(((Map)entry.getValue()).get("tag")));         
	        Map tagNullMap =  ((Map)(((Map)entry.getValue()).get("tag:null")));         
	        Map dimMap = (Map) ((Map) ((Map)entry.getValue()).get("dimension"));
	        Map mainDim = null;
	        if  (dimMap != null){
	            mainDim = (Map) dimMap.get(entryName);
	        }
	            
	        Map facMap = (Map) ((Map) ((Map)entry.getValue()).get("factor"));
	        Map stateMap = (Map) facMap.get(entryName + ".state");
	        Map updateMap = (Map) facMap.get(entryName + ".update");
	        Map eventMap = (Map) facMap.get(entryName + ".event");
	        Map statisticMap = (Map) facMap.get(entryName + ".statistic");
	        String entityName =  ((String)(((Map)entry.getValue()).get("entity")));         
	            
	        Map<String, Object> multiTags = new HashMap<String, Object>(); 
	        Iterator objIter = ((Map)entry.getValue()).entrySet().iterator();          
	        int tagCount = 0;
	        while(objIter.hasNext()){
	            Map.Entry<String, Object> objEntry=(Map.Entry<String, Object>)objIter.next();    
	            String key = objEntry.getKey();
	            if ( (key.split(":")).length > 1 && (key.split(":"))[0].equals("tag") && !(key.split(":"))[1].equals("null")){
	                tagCount += 1;
	                multiTags.put(key.split(":")[1], objEntry.getValue());
	            }
	        }
	        
	        ArrayList<String> valList = new ArrayList<String>();
	        if(tagMap !=null)
	        	valList.addAll(tagMap.keySet());
	        if(stateMap != null){     
	        		if(entryName.endsWith("Sensor") && !entryName.equals("cim.EnvironmentSensor")){
	        			this.getPeriodFactorData(entryName+"_state", valList, stateMap,"state_envData");
	        			this.getPeriodFactorData(entryName+"_state", valList, stateMap,"state_envSensor");
	        		}else{
	        			this.getPeriodFactorData(entryName+"_state", valList, stateMap,"state");
	        		}
	        }
	        if(updateMap != null){
	        		this.getPeriodFactorData(entryName+"_update", valList, updateMap,"update");
	        }
	        if(statisticMap != null){
	        		this.getPeriodFactorData(entryName+"_statistic", valList, statisticMap,"statistic");
	        }
	        if(eventMap !=null && tagCount == 0){
	        		this.getPeriodFactorData(entryName+"_event", valList, eventMap,"event");
	        }
	        if(dimMap!=null){
	        	Iterator dimIter = dimMap.entrySet().iterator();          
	        	while(dimIter.hasNext()){
	        		Map.Entry<String, Object> dimEntry=(Map.Entry<String, Object>)dimIter.next();   
	        		if(!dimEntry.getKey().equals(entryName)){

	        		}
	        	}
	        }
	        if(tagCount > 0){      
	        	if(mainDim !=null){
	        		if(eventMap != null){
	        			this.getPeriodFactorData(entryName+"_event", valList, eventMap,"event");
	        		}
	        		Collection<Object> tmp = multiTags.values();
	        		for(int i = 0; i < tmp.size();i++){
	        			valList.addAll(((Map)tmp.toArray()[i]).keySet());
	        		}
	        	}else if(eventMap !=null){
	        		Collection<Object> tmp = multiTags.values();
	        		for(int i = 0; i < tmp.size();i++){
	        			valList.addAll(((Map)tmp.toArray()[i]).keySet());
	        		}
	        		this.getPeriodFactorData(entryName+"_event", valList, eventMap,"event");
	        	}
	        }else{
	        	if(mainDim !=null)
	        	if(eventMap !=null){
	        		this.getPeriodFactorData(entryName+"_event", valList, eventMap,"event");
	        	}
	        }
	        
	        //insert data to specific city
	        String cityName = this.dataDB;
	        if(statisticMap != null){
	         	syncStatisticTable(statisticMap,entryName,cityName);
	        }
	        if(tagCount > 0){
	            if(mainDim != null){
	            		;
	            }else{
	              	if(eventMap != null){
	              		joinTables(tagMap,null,multiTags,entryName,eventMap,cityName,entityName, "event_out");
	              	}else{
	              		joinTables(tagMap,null,multiTags,entryName,updateMap,cityName,entityName, "update_out");
	              	}
	            }
			} else {
				if (mainDim == null) {
					if (updateMap != null) {
						this.syncFactorTable(tagMap, eventMap, entryName, cityName, "update");
					}
					if (eventMap != null) {
						//this.syncFactorTable(tagMap, eventMap, entryName, cityName, "event", true);
						this.syncFactorTable(tagMap, eventMap, entryName, cityName, "event");
					}
				}
			}
			if (stateMap != null) {
				if (mainDim != null && !entryName.equals("cim.ParkingSpot")) {
					joinTables(tagMap, tagNullMap, multiTags, entryName, stateMap, cityName, entityName, "state_in");
				}
			}
			if (facMap != null) {
				Iterator<Map.Entry<String, Object>> facEntry = facMap.entrySet().iterator();
				while (facEntry.hasNext()) {
					Map.Entry<String, Object> myEntry = facEntry.next();
					if (myEntry.getKey().startsWith(entry.getKey() + ".state.")) {
						joinTables(tagMap, tagNullMap, multiTags, entryName, (Map) (facMap.get(myEntry.getKey())),
								cityName, entityName,
								myEntry.getKey().substring(entry.getKey().length() + 1).replace("_", ".") + "_in");
					} else if (myEntry.getKey().equals(entry.getKey() + ".statistic.hourly")) {
						syncStatistcHourlyTable((Map) myEntry.getValue(), tagMap, myEntry.getKey(), cityName, "hourly","EnvironmentData");
						syncStatistcHourlyTable((Map) myEntry.getValue(), tagMap, myEntry.getKey(), cityName, "hourly","EnvironmentSensor");
					} else if (myEntry.getKey().equals(entry.getKey() + ".statistic.daily")) {
						syncStatistcHourlyTable((Map) myEntry.getValue(), tagMap, myEntry.getKey(), cityName, "daily","EnvironmentData");
						syncStatistcHourlyTable((Map) myEntry.getValue(), tagMap, myEntry.getKey(), cityName, "daily","EnvironmentSensor");
					}

				}
			}
			if (updateMap != null) {
				if (mainDim != null)
					joinTables(tagMap, tagNullMap, multiTags, entryName, updateMap, cityName, entityName, "update_in");
			}

			if (eventMap != null && mainDim != null) {
				joinTables(tagMap, tagNullMap, multiTags, entryName, eventMap, cityName, entityName, "event_in");
			}  
	        
	    }   
    	for(String stateEntry : aggStateList){
			if (stateEntry.endsWith("Sensor.state")) {
				syncUpdateToState(stateEntry.substring(0, stateEntry.length() - 6), this.aggStateMap.get(stateEntry),"sensor");
				syncUpdateToState(stateEntry.substring(0, stateEntry.length() - 6).replace("Sensor", "Data"), this.aggStateMap.get(stateEntry),"data");
			} else if (stateEntry.equals("cim.ParkingSpot.state")) {
				syncUpdateToState(stateEntry.substring(0, stateEntry.length() - 6), this.aggStateMap.get(stateEntry),"");
    		}
    	}
    	convertParkingSpotUpdate();
    	insertToAqiState();

    	int minuteOffsetStart = 0;
    	int minuteOffsetEnd = 0;
    	int minuteOffsetStartHalf = 0;
    	int minuteOffsetEndHalf = 0;
	    calUTC= Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	    calUTC.setTime(new Date( Long.parseLong(this.startTime)  - GAPTIME*60*1000 + 1));
	    for(String s:cityList){
	    	this.TIMEZONE = this.timezoneMap.get(s);
	    	calStart = Calendar.getInstance(TimeZone.getTimeZone(this.TIMEZONE));
	    	calEnd = Calendar.getInstance(TimeZone.getTimeZone(this.TIMEZONE));
	    	
	    	calStart.setTime(new Date( Long.parseLong(this.startTime)  - GAPTIME*60*1000 + 1));
	    	
	    	calEnd.setTime(new Date(Long.parseLong(this.stopTime) - GAPTIME*60*1000 + 1));
	    	if(calStart.get(Calendar.HOUR_OF_DAY)!=calEnd.get(Calendar.HOUR_OF_DAY) ||
	    			calStart.get(Calendar.DAY_OF_YEAR) != calEnd.get(Calendar.DAY_OF_YEAR)){
	    		if(calStart.get(Calendar.MINUTE) - calUTC.get(Calendar.MINUTE)== 0){
	    			this.cityListForHourly.add(s);
	    			minuteOffsetStart = calStart.get(Calendar.MINUTE);
	    			minuteOffsetEnd = calEnd.get(Calendar.MINUTE) ;
	    		}else{
	    			this.cityListForHalfHourly.add(s);
	    			minuteOffsetStartHalf = calStart.get(Calendar.MINUTE);
	    			minuteOffsetEndHalf = calEnd.get(Calendar.MINUTE) ;
	    		}
	    	}
	    }

	    String timeFilterCmd = "";
	    Calendar calUTCNextDay= Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	    Calendar calUTCPreviousDay = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
	    calUTCPreviousDay.setTime(new Date( Long.parseLong(this.startTime)  - GAPTIME*60*1000 - 24*3600*1000));
	    calUTCNextDay.setTime(new Date( Long.parseLong(this.stopTime)  - GAPTIME*60*1000 + 24*3600*1000));
	    if(calUTCPreviousDay.get(Calendar.YEAR) != calUTCNextDay.get(Calendar.YEAR)){
	    	timeFilterCmd += " ((year = " + calUTCPreviousDay.get(Calendar.YEAR) + " and month = 12) or "
	    			 + " (year = " + calUTCNextDay.get(Calendar.YEAR) + " and month = 1)) ";
	    }else{
	    	if(calUTCPreviousDay.get(Calendar.MONTH) != calUTCNextDay.get(Calendar.MONTH)){
	    		timeFilterCmd += " ( year = " +  calUTCPreviousDay.get(Calendar.YEAR) 
	    					+ " and (month = " + (calUTCPreviousDay.get(Calendar.MONTH) + 1)
	    					+ " or month = " + (calUTCNextDay.get(Calendar.MONTH) + 1) + ")) ";
	    	}else{
	    		timeFilterCmd += " ( year = " +  calUTCPreviousDay.get(Calendar.YEAR) 
							+ " and month = " + (calUTCPreviousDay.get(Calendar.MONTH) + 1) + ") ";
	    	}
	    }
	    
	    boolean hasExecuted = false;
	    if(!this.cityListForHourly.isEmpty()){
	    	Long start = Long.parseLong(this.startTime) - GAPTIME*60*1000 - minuteOffsetStart*60*1000;
	    	Long end =  Long.parseLong(this.stopTime) - GAPTIME*60*1000 - minuteOffsetEnd*60*100;
	    	if(!hasExecuted){
    			updatePersonalDeviceHbase(start-1800000,end+1800000);
    			hasExecuted = true;
    		}
    		updateParkingSessionHourly(this.cityListForHourly,start,end,timeFilterCmd);	
    		updateAQIHourly(this.cityListForHourly,start,end,timeFilterCmd);	
    		updatePersonalDeviceHourly(this.cityListForHourly,start,end);
    		entityIter = entitySet.iterator();          
    	    while(entityIter.hasNext()){
    	        Map.Entry<String, Object> entry=(Map.Entry<String, Object>)entityIter.next(); 
    	        String entryName = entry.getKey();
    	        Map facMap = (Map) ((Map) ((Map)entry.getValue()).get("factor"));
    	        Map statisticMap = (Map) facMap.get(entryName + ".statistic");
    	        if(statisticMap != null)
    	        	genAggStatisticTable(entryName,this.cityListForHourly,statisticMap,start,end,timeFilterCmd);
	        }
    		for(String stateEntry : aggStateList){
    			if(stateEntry.endsWith("Sensor.state")){
    				genUpdateToStateHourly(stateEntry.substring(0,stateEntry.length()-6),this.cityListForHourly,this.aggStateMap.get(stateEntry),start,end,timeFilterCmd,"Sensor");
    				genUpdateToStateHourly(stateEntry.substring(0,stateEntry.length()-6),this.cityListForHourly,this.aggStateMap.get(stateEntry),start,end,timeFilterCmd,"Data");
    			}else if(stateEntry.equals("cim.ParkingSpot.state") || stateEntry.equals("cim.WasteBin.state")){
    				genUpdateToStateHourly(stateEntry.substring(0,stateEntry.length()-6),this.cityListForHourly,this.aggStateMap.get(stateEntry),start,end,timeFilterCmd,"");
    			}else{
    				genAggStateTable(stateEntry,this.cityListForHourly,this.aggStateMap.get(stateEntry),start,end,timeFilterCmd);
    			}
    		}
    		for(String stateEntry : aggUpdateList){
            	genAggUpdateTable(stateEntry.substring(0,stateEntry.length()-7),this.cityListForHourly,this.aggUpdateMap.get(stateEntry),start,end,timeFilterCmd);
            }
    		joinEventHourly(this.cityListForHourly,start,end,timeFilterCmd);
    		joinStateHourly(this.cityListForHourly,start,end,timeFilterCmd);
    		
	    }
	    if(!this.cityListForHalfHourly.isEmpty()){
	    	Long start = Long.parseLong(this.startTime) - GAPTIME*60*1000 - minuteOffsetStartHalf*60*1000;
	    	Long end =  Long.parseLong(this.stopTime) - GAPTIME*60*1000 - minuteOffsetEndHalf*60*1000;
	    	if(!hasExecuted){
    			updatePersonalDeviceHbase(start-1800000,end+1800000);
    			hasExecuted = true;
    		}
			updateParkingSessionHourly(this.cityListForHalfHourly, start, end,timeFilterCmd);
			updateAQIHourly(this.cityListForHalfHourly,start,end,timeFilterCmd);	
			updatePersonalDeviceHourly(this.cityListForHalfHourly, start, end);
			entityIter = entitySet.iterator();
			while (entityIter.hasNext()) {
				Map.Entry<String, Object> entry = (Map.Entry<String, Object>) entityIter.next();
				String entryName = entry.getKey();
				Map facMap = (Map) ((Map) ((Map) entry.getValue()).get("factor"));
				Map statisticMap = (Map) facMap.get(entryName + ".statistic");
				if (statisticMap != null)
					genAggStatisticTable(entryName, this.cityListForHalfHourly, statisticMap, start, end,timeFilterCmd);
			}
			for (String stateEntry : aggStateList) {
				if(stateEntry.endsWith("Sensor.state")){
    					genUpdateToStateHourly(stateEntry.substring(0,stateEntry.length()-6),this.cityListForHalfHourly,this.aggStateMap.get(stateEntry),start,end,timeFilterCmd,"Sensor");
    					genUpdateToStateHourly(stateEntry.substring(0,stateEntry.length()-6),this.cityListForHalfHourly,this.aggStateMap.get(stateEntry),start,end,timeFilterCmd,"Data");
    				}else if (stateEntry.equals("cim.ParkingSpot.state") || stateEntry.equals("cim.WasteBin.state") ) {
					genUpdateToStateHourly(stateEntry.substring(0, stateEntry.length() - 6), this.cityListForHalfHourly,
							this.aggStateMap.get(stateEntry), start, end,timeFilterCmd,"");
				} else {
					genAggStateTable(stateEntry, this.cityListForHalfHourly, this.aggStateMap.get(stateEntry), start,
							end,timeFilterCmd);
				}
			}
			for (String stateEntry : aggUpdateList) {
				genAggUpdateTable(stateEntry.substring(0, stateEntry.length() - 7), this.cityListForHalfHourly,
						this.aggUpdateMap.get(stateEntry), start, end,timeFilterCmd);
			}	
			joinEventHourly(this.cityListForHalfHourly,start,end,timeFilterCmd);
			joinStateHourly(this.cityListForHourly,start,end,timeFilterCmd);
	    }
	}

	private void updateAQIHourly(List<String> cityList, Long start, Long end, String timeFilterCmd) throws SQLException {
		String cmd = "";
		cmd = "insert into cimdata.AQIEntry_statistic_hourly(code,week,sources,timeid,weekday,parentEntityType,sid,hour,"
			+ "attributeName,day,sensitiveGroups,announcement,monthweek,advisory,value_min,value_avg,entityType,"
			+ "entityId,pollutant,effects,locationid,value_max,tenantId,category,year,month,city)"
			+ " select code,week,sources,timeid,weekday,parentEntityType,A.sid,hour,attributeName,cimdata.datelist_half.day,"
			+ "sensitiveGroups,announcement,monthweek,advisory,min(value) as value_min,avg(value) as value_avg,entityType,entityId,"
			+ "pollutant,effects,A.locationid,max(value) as value_max,A.tenantId,category,cimdata.datelist_half.year,"
			+ "cimdata.datelist_half.month,A.city from (select * from cimdata.AQIEntry_state "
			+ " where " + timeFilterCmd + " and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd += "sampletimestamp >=" + start +" and sampletimestamp <" + end; 		
		cmd	+= ") as A"
			+ " inner join cimdata.environmentsensor on A.sid = cimdata.environmentsensor.sid and A.tenantid = cimdata.environmentsensor.tenantid "
			+ " left join cimdata.datelist_half on A.sampletimestampid = cimdata.datelist_half.halfutcid  and A.city = cimdata.datelist_half.city "
			+ " group by year,month,A.city,A.tenantId,category,A.locationid,entityType,entityId,pollutant,effects,code,week,sources,timeid,"
			+ "weekday,parentEntityType,A.sid,hour,attributeName,day,sensitiveGroups,announcement,monthweek,advisory";
		impalaCmd(cmd);
		
		cmd = "insert into cimdata.AQIEntry_statisticdata_hourly("
			+ "code,week,sources,timeid,weekday,parentEntityType,sid,hour,"
			+ "attributeName,day,sensitiveGroups,announcement,monthweek,advisory,value_min,value_avg,entityType,"
			+ "entityId,pollutant,effects,locationid,value_max,tenantId,category,year,month,city)"
			+ " select code,week,sources,timeid,weekday,parentEntityType,A.sid,hour,attributeName,cimdata.datelist_half.day,"
			+ "sensitiveGroups,announcement,monthweek,advisory,min(value) as value_min,avg(value) as value_avg,entityType,entityId,"
			+ "pollutant,effects,A.locationid,max(value) as value_max,A.tenantId,category,cimdata.datelist_half.year,"
			+ "cimdata.datelist_half.month,A.city from (select * from cimdata.AQIEntrydata_state "
			+ " where " + timeFilterCmd + " and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd += "sampletimestamp >=" + start +" and sampletimestamp <" + end; 		
		cmd	+= ") as A"
			+ " inner join cimdata.environmentdata on A.sid = cimdata.environmentdata.sid and A.tenantid = cimdata.environmentdata.tenantid "
			+ " left join cimdata.datelist_half on A.sampletimestampid = cimdata.datelist_half.halfutcid  and A.city = cimdata.datelist_half.city "
			+ " group by year,month,A.city,A.tenantId,category,A.locationid,entityType,entityId,pollutant,effects,code,week,sources,timeid,"
			+ "weekday,parentEntityType,A.sid,hour,attributeName,day,sensitiveGroups,announcement,monthweek,advisory";
		impalaCmd(cmd);
	}

	private void insertToAqiState() throws SQLException {
		String cmd ="insert into cimdata.AQIEntry_state(sampleTimestamp,code,sources,parentEntityType,receiveTimestamp,sid,"
				+ " lastUpdated,sampleTimestampID,locationId,timezoneoffset,attributeName,value,day,"
				+ " sensitiveGroups,announcement,period,advisory,entityType,entityId,pollutant,effects,"
				+ " tenantId,category,startTimestamp) partition(year,month,city) select "
				+ " cim.AQIEntry_state_period.sampleTimestamp,code,sources,parentEntityType,receiveTimestamp,sid,lastUpdated,"
				+ " sampleTimestampID,A.locationId,timezoneoffset,attributeName,value,day,sensitiveGroups,announcement,period,advisory,"
				+ " entityType,entityId,pollutant,effects,A.tenantId,category,startTimestamp,year,month,A.city from cim.AQIEntry_state_period" 
				+ " left join (select distinct sid as asid,city,locationid,tenantid from cimdata.environmentsensor) as A "
				+ "on A.asid = cim.AQIEntry_state_period.sid and A.tenantid = cim.AQIEntry_state_period.tenantid"
				+ " left join cimdata.datelist_half on cimdata.datelist_half.city = A.city and cimdata.datelist_half.halfutcid "
				+ "=cim.AQIEntry_state_period.sampletimestampid where parentEntityType ='EnvironmentSensor'";
		impalaCmd(cmd);
		cmd ="insert into cimdata.AQIEntrydata_state(sampleTimestamp,code,sources,parentEntityType,receiveTimestamp,sid,"
				+ " lastUpdated,sampleTimestampID,locationId,timezoneoffset,attributeName,value,day,"
				+ " sensitiveGroups,announcement,period,advisory,entityType,entityId,pollutant,effects,"
				+ " tenantId,category,startTimestamp) partition(year,month,city) select "
				+ " cim.AQIEntry_state_period.sampleTimestamp,code,sources,parentEntityType,receiveTimestamp,sid,lastUpdated,"
				+ " sampleTimestampID,A.locationId,timezoneoffset,attributeName,value,day,sensitiveGroups,announcement,period,advisory,"
				+ " entityType,entityId,pollutant,effects,A.tenantId,category,startTimestamp,year,month,A.city from cim.AQIEntry_state_period" 
				+ " left join (select distinct sid as asid,city,locationid,tenantid from cimdata.environmentdata) as A "
				+ "on A.asid = cim.AQIEntry_state_period.sid and A.tenantid = cim.AQIEntry_state_period.tenantid"
				+ " left join cimdata.datelist_half on cimdata.datelist_half.city = A.city and cimdata.datelist_half.halfutcid "
				+ "=cim.AQIEntry_state_period.sampletimestampid where parentEntityType ='EnvironmentData'";
		impalaCmd(cmd);
		
	}

	private void initIncidentType(String url) throws Exception {
		HDFSAccessor accessor = new HDFSAccessor();
        Properties configProperties = accessor.readProperties(url+"/incident.properties");
		
		String typeList = "";
		String incidentTypeList = "wasteManagementType,snsType,parkingType,mobilityType,trafficType,environmentType";
		typeList = configProperties.getProperty("wasteManagementType");
		wasteIncidentType.clear();
		for (String s : typeList.split(",")) {
			wasteIncidentType.add(s);
		}
		typeList = configProperties.getProperty("snsType");
		snsIncidentType.clear();
		for (String s : typeList.split(",")) {
			snsIncidentType.add(s);
		}
		typeList = configProperties.getProperty("parkingType");
		parkingIncidentType.clear();
		for (String s : typeList.split(",")) {
			parkingIncidentType.add(s);
		}
		typeList = configProperties.getProperty("mobilityType");
		mobilityIncidentType.clear();
		for (String s : typeList.split(",")) {
			mobilityIncidentType.add(s);
		}
		typeList = configProperties.getProperty("trafficType");
		trafficIncidentType.clear();
		for (String s : typeList.split(",")) {
			trafficIncidentType.add(s);
		}
		typeList = configProperties.getProperty("trafficType");
		environmentIncidentType.clear();
		for (String s : typeList.split(",")) {
			environmentIncidentType.add(s);
		}
		
	}

	private void convertParkingSpotUpdate() throws SQLException {
		String targetTable = this.dataDB + ".parkingspot_update";
		String dateTable = this.dataDB + ".datelist_half";
		String oriTable = this.defaultDB + ".genericevent_event_period"; 
		String cmd= "insert into "+targetTable+"(sid,parkingspaceid,tenantid,expectedrevenue,occupied,sampletimestamp_delta,"
				+ "sampletimestamp,sampletimestampid,locationid,city,year,month,timezoneoffset) "
				+ "select ori.parkingspotid as sid,ori.parkingspaceid,ori.tenantid,parkedvehicledetails_expectedrevenue,"
				+ "case when ori.closuretime is null then 1 else 0 end,"
				+ "ori.sampletimestamp-ori.createtimestamp,ori.sampletimestamp,ori.sampletimestampid,ori.locationid,cim.locations.city,"
				+ dateTable + ".year,"+dateTable+".month,"+dateTable+".timezoneoffset "
				+ "from (select * from "+oriTable+" where eventtype= 'ParkedVehicle' "
				+ " and receivetimestamp >= "+this.startTime+" and receivetimestamp<"+this.stopTime+") as ori "
				+ " left join cim.locations on ori.locationid=cim.locations.sid and ori.tenantid = cim.locations.tenantid"
				+ " left join "+dateTable+" on ori.sampleTimestampid = "+dateTable+".halfutcid "
				+ " and cim.locations.city = "+dateTable+".city";
		impalaCmd(cmd);	
	}

	private void updatePersonalDeviceHbase(Long start, Long end) {
		/*
		 try {
	        	String htableName = "cim.PersonalDevice.update";
	        	String targetTableName = "cim.PersonalDevice.update.period";	  	
	        	
	        	Configuration hconf = new Configuration();
	        	hconf = HBaseConfiguration.create();
	        	hconf.set(HConstants.ZOOKEEPER_CLIENT_PORT,"2181");
	        	hconf.set(HConstants.ZOOKEEPER_QUORUM, "name-01.novalocal,cm-hue-01.novalocal,data-01.novalocal,data-02.novalocal"); 
	        	Connection connection = (Connection) ConnectionFactory.createConnection(hconf);
	        	
	        	Table table = connection.getTable(TableName.valueOf(htableName));
	        	Table targetTable = connection.getTable(TableName.valueOf(targetTableName));
	        	Long starttime = System.currentTimeMillis();
	            Scan s = new Scan();
	            Long startSuffix = 2000000000000L -end;
	            Long endSuffix = 2000000000000L -start;
		    System.out.println("hbase start:" + startSuffix + "  end " + endSuffix);
		    int count = 0;
	            for(int i =0 ; i <4 ; i++){
		            s.setStartRow(Bytes.toBytes(i+ "|" +startSuffix));
		            s.setStopRow(Bytes.toBytes(i+ "|" +endSuffix));
		            ResultScanner rs = table.getScanner(s);
		            List<Put> puts = new ArrayList<Put>();
		            for (Result r : rs) {	
		                Put put= new Put(r.getRow()); 
		                for (KeyValue kv : r.raw()) {
		                	put.add(kv);
		                }
				count++;
		                puts.add(put);          
		            }
		            targetTable.put(puts);
		            rs.close();
	            }
				targetTable.close();
				table.close();
				
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        */
	}



	private void syncUpdateToState(String entryName, String values,String mode) throws SQLException {
		String cityName = this.dataDB;
		entryName = entryName.substring(this.defaultDB.length()+1);
		String dimTable = cityName + "." + entryName;
		String dateTable = cityName + "." + this.DATEIDHALF;
		String dimStateTable = cityName + ".environmentsensor_state"; 
		if(mode.equals("sensor")){
			dimTable = cityName + ".environmentsensor";
		}else if(mode.equals("data")){
			dimTable = cityName + ".environmentdata";
		}else{
			values = "occupied";
		}
		String stateTable = cityName + "." + entryName + "_state";
		String periodTable = this.defaultDB + "." + entryName + "_state_period";
		String hourlyTable = cityName + "." + entryName + "_state_hourly";
		//sync to city
		String[] stateValues = "receivetimestamp,sampletimestamp,sampletimestampid,sid,tenantid,starttimestamp".split(",");	
		String[] objectValues = values.split(",");
		
		
		String cmd;
		cmd = "insert into table " + stateTable + "(";
		for(String s:stateValues){
			cmd += s + ",";
		}
		
		for(String s:objectValues){
			if(s.contains(":")){
				if(s.split(":")[0].equals("double")){
					cmd += s.split(":")[1] + ",";
					if(!s.split(":")[1].startsWith("laeqt")){
						cmd += s.split(":")[1] + "_accuracy,";
						cmd += s.split(":")[1] + "_expiresat,";
						cmd += s.split(":")[1] + "_reliability,";
					}
				}else{
					cmd += s.split(":")[1] + ",";
				}
			}else{
				cmd += s + ",";
			}
		}
		cmd += "locationid,timezoneoffset,";
		if(mode.equals("sensor")){
			cmd += "geocoordinates_longitude,geocoordinates_latitude,geocoordinates_altitude,";
		}
		cmd = cmd.substring(0,cmd.length()-1);
		cmd += ") partition (year,month,city) select ";
		for(String s:stateValues){
			cmd += periodTable + "." +  s + ",";
		}
		for(String s:objectValues){
			if(s.contains(":")){
				if(s.split(":")[0].equals("double")){
					cmd += periodTable + "." + s.split(":")[1] + ",";
					if(!s.split(":")[1].startsWith("laeqt")){
						cmd += periodTable + "." + s.split(":")[1] + "_accuracy,";
						cmd += periodTable + "." + s.split(":")[1] + "_expiresat,";
						cmd += periodTable + "." + s.split(":")[1] + "_reliability,";
					}
				}else{
					cmd += s.split(":")[1] + ",";
				}
			}else{
				cmd += periodTable + "." + s + ",";
			}
		}
		cmd += "A.locationid," + dateTable + ".timezoneoffset,";
		if(mode.equals("sensor")){
			cmd += "DS.geocoordinates_longitude,DS.geocoordinates_latitude,DS.geocoordinates_altitude," ;
		}
		cmd += dateTable+".year," +  dateTable + ".month,A.city from " + periodTable;
		if(mode.equals("sensor")){
			cmd += " left join (select sid as dsid,sampletimestamp,"
			    + "geocoordinates_longitude,geocoordinates_latitude,geocoordinates_altitude from "
				+ dimStateTable +" ) as DS on DS.dsid = " + periodTable + ".sid "
			    + " and DS.sampletimestamp = " + periodTable +".sampletimestamp" ;	    
		}			
		cmd	+= " left join (select distinct sid as asid,city,locationid,tenantid from "+dimTable+" ) as A"
				+" on A.asid = "+periodTable+".sid and A.tenantid = "
				+ periodTable+".tenantid left join " + dateTable 
				+" on "+dateTable +".city = A.city and "
				+ dateTable+".halfutcid ="+periodTable +".sampletimestampid";	
		impalaCmd(cmd);
		
	}


	private void genUpdateToStateHourly(String entryName, List<String> cityList, String values,
			Long starttime, Long endtime,String timeFilterCmd,String envType) throws SQLException {	
		entryName = entryName.substring(this.defaultDB.length()+1);
		if(envType.equals("Data")){
			entryName = entryName.replace("Sensor", "Data");
		}
		String dimTable = this.dataDB + "." + entryName;
		if(entryName.startsWith("BAQI")){
			return;
		}else if(entryName.endsWith("Data")){
			dimTable = this.dataDB + ".environmentdata";
		}else if(entryName.endsWith("Sensor")){
			dimTable = this.dataDB + ".environmentsensor";
		}else if(entryName.equals("WasteBin")){
			dimTable = this.dataDB + ".wastebin";
		}else{
			return;
			//values = "occupied";
		}
		String stateTable = this.dataDB + "." + entryName + "_state";
		String periodTable = this.defaultDB + "." + entryName + "_state_period";
		String hourlyTable = this.dataDB + "." + entryName + "_state_hourly";
		String dateTable = this.dataDB + ".datelist_half";
		String[] stateValues = "receivetimestamp,sampletimestamp,sampletimestampid,sid,starttimestamp".split(",");	
		String[] objectValues = values.split(",");
		
		String cmd;
		cmd = "insert into " + hourlyTable+ "(sid,tenantid,count,locationid,city,";
		for(String s : timeList){
			cmd += s + ",";
		}
		if(entryName.endsWith("Sensor") || entryName.endsWith("Data") || entryName.equals("WasteBin")){
			for(String s: objectValues){
				if(!s.split(":")[0].equals("string")){
					cmd += s.split(":")[1] + "_avg," + s.split(":")[1] + "_max," + s.split(":")[1] + "_min,";
				}else{
					cmd += s.split(":")[1] + ",";
				}
			}
		}else{
			cmd += "occupied_avg,";
		}
		cmd += "timeid) select sid,tenantid,cast(count(*) as int),locationid,city,";
		for(String s : timeList){
			cmd += s + ",";
		}
		if(entryName.endsWith("Sensor") || entryName.endsWith("Data")|| entryName.equals("WasteBin")){
			for(String s: objectValues){
				if(!s.split(":")[0].equals("string")){
					s= s.split(":")[1];
					cmd += "sum(" + s +"*(endtimestamp-starttimestamp))/(sum(endtimestamp-starttimestamp)+0.001) as "+ s+ "_avg," 
				        + "max(" + s + ") as " + s + "_max,min(" + s + ") as " + s + "_min," ;
				}else{
					cmd += s.split(":")[1] + ",";
				}
			}
		}else{
			cmd += "sum(case occupied when 1 then endtimestamp-starttimestamp else 0 end)/(sum(endtimestamp-starttimestamp)+0.001),";
		}
		cmd += "timeid from (select A.sampletimestamp as endtimestamp, A.starttimestamp as starttimestamp, "
				+ "A.sid as sid, A.tenantid as tenantid,A.city,";
		if(entryName.endsWith("Sensor") || entryName.endsWith("Data")|| entryName.equals("WasteBin")){
			for(String s: objectValues){
				if(!s.split(":")[0].equals("string")){
					s= s.split(":")[1];
					cmd += "A." + s + " as " + s + ",";
				}else{
					cmd += "A." + s.split(":")[1] + " as " + s.split(":")[1] + ",";
				}
			}
		}else{
			cmd += "occupied,";
		}
		for(String s : timeList){
			cmd += dateTable + "." + s + " as " + s + ",";
		}
		cmd += dateTable + ".timeid as timeid," + dimTable + ".locationid as locationid"
			+ " from ( select * from " + stateTable
			+ " where sampletimestamp > " + starttime + " and sampletimestamp <= " + endtime
			+ " ) as A left join " + dateTable + " on A.sampletimestampid = " + dateTable + ".halfutcid "
			+ " and A.city = " +dateTable+ ".city inner join "
			+ dimTable + " on A.sid = " + dimTable + ".sid and A.tenantid = " + dimTable + ".tenantid" 
			+ " group by A.sampletimestamp, A.starttimestamp, A.sid,A.tenantid,A.city, A.sampletimestamp," +  dateTable + ".timeid,";
		if(entryName.endsWith("Sensor") || entryName.endsWith("Data")|| entryName.equals("WasteBin")){
			for(String s: objectValues){
				if(!s.split(":")[0].equals("string")){
					s= s.split(":")[1];
					cmd += "A." + s + ",";
				}else{
					cmd += "A." + s.split(":")[1] + ",";
				}
			}
		}else{
			cmd += "A.occupied,";
		}
		for(String s : timeList){
			cmd += dateTable + "." + s + ",";
		}
		cmd += dimTable + ".locationid) as T group by T.sid,T.tenantid,";
		for(String s : timeList){
			cmd +=  "T." + s + ",";
		}
		if(entryName.endsWith("Sensor") || entryName.endsWith("Data")|| entryName.equals("WasteBin")){
			for(String s: objectValues){
				if(s.split(":")[0].equals("string")){
					cmd += "T." + s.split(":")[1] + ",";
				}
			}
		}
		cmd += "T.locationid,T.timeid,T.city";
		impalaCmd(cmd);
	}


	private void syncAreaTable() throws SQLException {
		String cmd;
		String targetTable = "";
		String oriTable = this.defaultDB + ".parkingarea_hbase";
		String dimTable = "";
        targetTable = this.dataDB + ".parkingarea";
        dimTable = this.dataDB + ".parkingspace";
        cmd = "insert into " + targetTable + "(area_geopoint,parkingspaceid,receivetimestamp,sampletimestamp,sid,locationid,city)"
            	+ " select  area_geopoint,parkingspaceid,receivetimestamp,sampletimestamp,sid,A.locationid,A.city from " + oriTable
            	+ " inner join (select distinct sid as psid,locationid,city,tenantid from " +  dimTable + ") as A"
            	+ " on A.psid = " + oriTable  + ".parkingspaceid and A.tenantid = " +oriTable+ ".tenantid"  ;
        impalaCmd(cmd);
	}


	private void updateParkingSessionHourly(List<String> cityList, Long start, Long end,String timeFilterCmd) throws SQLException {
		String cmd;	
		String hourlyTable = this.dataDB + ".parking_session_hourly";
		String dimTable = this.dataDB + ".parkingspace";
		
		String dateTable = this.dataDB + ".datelist_half";
		String areaTable = this.dataDB + ".parkingarea";
		String oriTable = "";
		//floating parking session
		oriTable = this.dataDB + ".floatingparkingsession_event";
		cmd = "insert into " + hourlyTable + "(entityid,locationid,tenantid,entitytype,parkingspaceid,turnover"
			+ ",sessiontime_sum,sessiontime_max,sessiontime_min,revenue_sum,revenue_max,revenue_min,count,timeid";
		for(String s:timeList){
			cmd += "," + s;
		}	
		cmd += ",city) select parkingspaceid,A.locationid,tenantid,'FloatingParkingSession',parkingspaceid,false,"
			+ "sum(sessiontime),max(sessiontime),min(sessiontime),sum(farepaid),max(farepaid),min(farepaid),"
			+ "cast(count(*) as int)," + dateTable + ".timeid"; 
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		cmd += ",A.city from (select (destroytimestamp-createtimestamp) as sessiontime,farepaid,parkingspaceid,locationid,tenantid,sampletimestampid,city from "
			 + oriTable +  " where " + timeFilterCmd + " and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd += "sampletimestamp >=" + start +" and sampletimestamp <" + end 
			 + " and destroytimestamp is not null "
			 + ") as A left join "
			 + dateTable + " on A.sampletimestampid = " + dateTable + ".halfutcid and "
			 + dateTable + ".city = A.city "
			 + "group by A.city,parkingspaceid,A.locationid,tenantid," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		impalaCmd(cmd);
		
		oriTable = this.dataDB + ".floatingparkingsession_event";
		cmd = "insert into " + hourlyTable + "(entityid,locationid,tenantid,entitytype,parkingspaceid,turnover"
			+ ",sessiontime_sum,sessiontime_max,sessiontime_min,revenue_sum,revenue_max,revenue_min,count,timeid";
		for(String s:timeList){
			cmd += "," + s;
		}	
		cmd += ",city) select parkingspaceid,A.locationid,tenantid,'FloatingParkingSession',parkingspaceid,false,"
			+ "0,0,0,sum(farepaid),max(farepaid),min(farepaid),"
			+ "cast(count(*) as int)," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		cmd += ",A.city from (select 0,farepaid,parkingspaceid,locationid,tenantid,sampletimestampid,city,createtimestamp,createtimestampid from "
			 + oriTable +  " where " + timeFilterCmd + " and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd += "createtimestamp >=" + start +" and createtimestamp <" + end 
			 + ") as A left join "
			 + dateTable + " on A.createtimestampid = " + dateTable + ".halfutcid and "
			 + dateTable + ".city = A.city "
			 + "group by A.city,parkingspaceid,A.locationid,tenantid," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		impalaCmd(cmd);
		
		//parking area
		oriTable = this.dataDB + ".occupiedparkingarea_event";
		cmd = "insert into " + hourlyTable + "(entityid,locationid,tenantid,entitytype,parkingspaceid,turnover"
				+ ",sessiontime_sum,sessiontime_max,sessiontime_min,revenue_sum,revenue_max,revenue_min,count,timeid";
		for(String s:timeList){
				cmd += "," + s;
		}	
		cmd += ",city) select parkingareaid,A.locationid,A.tenantid,'ParkingArea',parkingspaceid,false,"
			+ "sum(sessiontime),max(sessiontime),min(sessiontime),sum(expectedrevenue),max(expectedrevenue),min(expectedrevenue),"
			+ "cast(count(*) as int)," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		cmd += ",A.city from (select sid,(destroytimestamp-createtimestamp) as sessiontime,expectedrevenue,parkingareaid,locationid,tenantid,sampletimestampid,city from "
			+ oriTable +  " where "+ timeFilterCmd + " and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd	+= "sampletimestamp >=" + start +" and sampletimestamp <" + end
			+ " and destroytimestamp is not null "
			+ ") as A left join (select distinct sid,tenantid,parkingspaceid,city from "
			+ areaTable + ") as B on A.parkingareaid = B.sid "
			+ " and A.tenantid = B.tenantid left join " + dateTable
			+ " on A.sampletimestampid = " + dateTable + ".halfutcid and "
			+ dateTable + ".city = A.city " 
			+ "group by A.city,parkingareaid,A.locationid,A.tenantid,parkingspaceid," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		impalaCmd(cmd);
		
		oriTable = this.dataDB + ".occupiedparkingarea_event";
		cmd = "insert into " + hourlyTable + "(entityid,locationid,tenantid,entitytype,parkingspaceid,turnover"
				+ ",sessiontime_sum,sessiontime_max,sessiontime_min,revenue_sum,revenue_max,revenue_min,count,timeid";
		for(String s:timeList){
				cmd += "," + s;
		}	
		cmd += ",city) select parkingareaid,A.locationid,A.tenantid,'ParkingArea',parkingspaceid,true,"
			+ "0,0,0,sum(expectedrevenue),max(expectedrevenue),min(expectedrevenue),"
			+ "cast(count(*) as int)," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		cmd += ",A.city from (select sid,0,expectedrevenue,parkingareaid,locationid,tenantid,sampletimestampid,city,createtimestamp,createtimestampid from "
			+ oriTable +  " where "+ timeFilterCmd + " and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd	+= "createtimestamp >=" + start +" and createtimestamp <" + end
			+ ") as A left join (select distinct sid,tenantid,parkingspaceid,city from "
			+ areaTable + ") as B on A.parkingareaid = B.sid"
			+ " and A.tenantid = B.tenantid left join " + dateTable
			+ " on A.createtimestampid = " + dateTable + ".halfutcid and "
			+ dateTable + ".city = A.city " 
			+ "group by A.city,parkingareaid,A.locationid,A.tenantid,parkingspaceid," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		impalaCmd(cmd);
		
		//parkingspot 	
		oriTable = this.dataDB + ".parkingspot_update";
		String spotTable = this.dataDB + ".parkingspot";
		cmd = "insert into " + hourlyTable + "(entityid,locationid,tenantid,entitytype,parkingspaceid,turnover"
				+ ",sessiontime_sum,sessiontime_max,sessiontime_min,revenue_sum,revenue_max,revenue_min,count,timeid";
		for(String s:timeList){
				cmd += "," + s;
		}	
		cmd += ",city) select parkingspotid,A.locationid,A.tenantid,'ParkingSpot',parkingspaceid,false,"
			+ "sum(sessiontime),max(sessiontime),min(sessiontime),"
			+ "sum(expectedrevenue),max(expectedrevenue),min(expectedrevenue),"
			+ "cast(count(*) as int)," + dateTable + ".timeid"; 
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		cmd += ",A.city from (select sid as parkingspotid,locationid,tenantid,sampletimestamp_delta as sessiontime,"
			+ "sampletimestampid,expectedrevenue,city from "+ oriTable +  " where "+ timeFilterCmd + " and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd += "sampletimestamp >=" 
			+ start +" and sampletimestamp <" + end + " and occupied = 0"
			+ ") as A left join (select distinct sid,tenantid,parkingspaceid from "
			+ spotTable + ") as B on A.parkingspotid = B.sid"
			+ " and A.tenantid = b.tenantid left join " + dateTable
			+ " on A.sampletimestampid = " + dateTable + ".halfutcid and "
			 + dateTable + ".city = A.city " 
			+ "group by A.city,parkingspotid,A.locationid,A.tenantid,parkingspaceid," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		impalaCmd(cmd);
		cmd = "insert into " + hourlyTable + "(entityid,locationid,tenantid,entitytype,parkingspaceid,turnover"
				+ ",sessiontime_sum,sessiontime_max,sessiontime_min,revenue_sum,revenue_max,revenue_min,count,timeid";
		for(String s:timeList){
				cmd += "," + s;
		}	
		cmd += ",city) select parkingspotid,A.locationid,A.tenantid,'ParkingSpot',parkingspaceid,true,"
			+ "sum(sessiontime),max(sessiontime),min(sessiontime),"
			+ "sum(expectedrevenue),max(expectedrevenue),min(expectedrevenue),"
			+ "cast(count(*) as int)," + dateTable + ".timeid"; 
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		cmd += ",A.city from (select sid as parkingspotid,locationid,tenantid,sampletimestamp_delta as sessiontime,"
			+ "sampletimestampid,expectedrevenue,city from "+ oriTable +  " where "+ timeFilterCmd + " and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd += "sampletimestamp >=" 
			+ start +" and sampletimestamp <" + end + " and occupied = 1"
			+ ") as A left join (select distinct sid,tenantid,parkingspaceid from "
			+ spotTable + ") as B on A.parkingspotid = B.sid "
			+ " and A.tenantid = B.tenantid left join " + dateTable
			+ " on A.sampletimestampid = " + dateTable + ".halfutcid and "
			+ dateTable + ".city = A.city "  
			+ "group by A.city,parkingspotid,A.locationid,A.tenantid,parkingspaceid," + dateTable + ".timeid";
		for(String s:timeList){
			cmd += "," +dateTable + "." + s;
		}
		impalaCmd(cmd);
	}

	private void updatePersonalDeviceHourly(List<String> cityList,Long start, Long end) throws SQLException {
		String cmd;
		String hourlyTable = this.dataDB + ".personaldevice_update_hourly";
		String bounceHourlyTable = this.dataDB + ".personaldevice_update_bounce_hourly";
		String periodTable = this.dataDB + ".personaldevice_update_period";
		String bounceTable = this.dataDB + ".personaldevice_bounce_period";
		String updateTable = this.dataDB + ".personaldevice_update";
		String nonredTable = this.dataDB + ".personaldevice_update_nonredundant"; 
		String dateTable = this.dataDB + ".datelist_half";
		String datelistTable = this.dataDB + ".datelist";
		
		Long start_before_year = start - 365*24*3600*1000;
		Long start_before_day = start - 24*3600*1000;
	
		String dimHbase = "cim.personaldevice_hbase";
		String updateHbase = "cim.personaldevice_update_period_hbase";
		String dimTable = this.dataDB + ".personaldevice";
		String roiTable = this.dataDB + ".mobilityroi";
		
		//sync update 		
		cmd = "insert into " +updateTable+"(sid,roiid_before,macaddress,sampletimestamp_delta,"
			+ " sampletimestamp,receivetimestamp,sampletimestampid) partition(roiid) "
			+ " select distinct sid,roiid_before,macaddress,sampletimestamp_delta,sampletimestamp,receivetimestamp,sampletimestampid,roiid from ("  
			+ " select sid,roiid_before,macaddress,sampletimestamp_delta,sampletimestamp,receivetimestamp,sampletimestampid,roiid "
			+ " from  " + updateHbase
			+ " where roiid in (select sid from "+roiTable+") and sampletimestamp >= "+start+" and sampletimestamp < " + end 
			+ " union"
			+ " select sid,roiid_before,macaddress,sampletimestamp_delta,sampletimestamp,receivetimestamp,sampletimestampid,roiid"
			+ " from  cim.personaldevice_update_period_hbase "
			+ " where roiid_before in (select sid from "+roiTable+")"
			+ " and sampletimestamp >= "+start+" and sampletimestamp < "+end+") as A";
		impalaCmd(cmd);
		
		//get period data all for bounce 
		cmd = "insert overwrite " + bounceTable + "(sid,roiid,macaddress,sampletimestamp,sampletimestampid,roiid_before,sampletimestamp_delta)"
				+ " select sid,roiid,macaddress,sampletimestamp,sampletimestampid,roiid_before,sampletimestamp_delta from " + updateTable
				+ " where sampletimestamp >= " + start_before_day +" and sampletimestamp < " + end;
		impalaCmd(cmd);
		
		//get period data
		cmd = "insert overwrite " + periodTable + "(roiid,macaddress,sampletimestamp,sampletimestampid)"
			+ " select roiid,macaddress,sampletimestamp,sampletimestampid from " + bounceTable
			+ " where sampletimestamp >= " + start +" and sampletimestamp < " + end
			+ " and (roiid != roiid_before or sampletimestamp_delta = 0)";
		impalaCmd(cmd);
		
		//sync period to nonredundant
		cmd =  "insert into " + nonredTable + "(macaddress,sampletimestamp) partition (roiid)"
			    + " select A.macaddress,A.sampletimestamp,A.roiid from (select min(sampletimestamp) as sampletimestamp"
			    + ",macaddress,roiid from "+periodTable+" group by macaddress,roiid) as A left join "
			    + nonredTable + " on A.macaddress= "+nonredTable+".macaddress"
			    + " and A.roiid= "+nonredTable +".roiid where " + nonredTable +".sampletimestamp is null";
		impalaCmd(cmd);	
		
		
		//get first/repeat property
		cmd = "insert overwrite "+ periodTable+"(before_sampletimestamp,roiid,macaddress,sampletimestamp,sampletimestampid)"
			+ " select max(B.sampletimestamp),A.roiid,A.macaddress,A.sampletimestamp,A.sampletimestampid from "
			+ "	(select * from "+ periodTable +") as A left join  (select * from "+ nonredTable +" where "
			+ " sampletimestamp >="+start_before_year +" and sampletimestamp < " + end +") as B"
			+ " on A.macaddress = B.macaddress and A.roiid = B.roiid and A.sampletimestamp > B.sampletimestamp"
			+ " group by A.roiid,A.macaddress,A.sampletimestamp,A.sampletimestampid";
		impalaCmd(cmd);
		
		//handle bounce to bounce time
		cmd = "insert overwrite " + bounceTable + "(sid,sampletimestampid,sampletimestamp,roiid,sampletimestamp_delta)"
			+ " select A.sid,A.sampletimestampid,A.sampletimestamp,A.roiid_before,B.sumvalue from "
			+ " (select * from "+ bounceTable +" where sampletimestamp >=" + start +" and (roiid != roiid_before or roiid is null)) as A"
			+ " inner join "
			+ "(select max(sampletimestamp) as maxtime,sum(sampletimestamp_delta) as sumvalue,roiid_before,sid from "+bounceTable
			+" where roiid != roiid_before or roiid is null group by roiid_before,sid) as B"
			+ " on A.sampletimestamp = B.maxtime and A.roiid_before = B.roiid_before and A.sid = B.sid";
		impalaCmd(cmd);
		
		
		//sync bounce hourly
		cmd = "insert overwrite "+bounceHourlyTable +"(roiid,sessiontime_count_0_2,"
			 + "sessiontime_count_2_10,sessiontime_count_10_20,sessiontime_count_20_30,count,timeid)"
			 + " select roiid,sum(case when sampletimestamp_delta>0 and sampletimestamp_delta<120000 then 1 else 0 end)"
			 + ",sum(case when sampletimestamp_delta>=120000 and sampletimestamp_delta<600000 then 1 else 0 end)"
			 + ",sum(case when sampletimestamp_delta>=600000 and sampletimestamp_delta<1200000 then 1 else 0 end)"
			 + ",sum(case when sampletimestamp_delta>=1200000 and sampletimestamp_delta<1800000 then 1 else 0 end)"
			 + ",sum(case when sampletimestamp_delta>0  then 1 else 0 end) as count,timeid from "
			 + "(select * from " + bounceTable + ") as A left join "+ dateTable
			 + " on A.sampletimestampid = "+dateTable+".halfutcid group by roiid,timeid";
		impalaCmd(cmd);
		// insert into final hourly table
		cmd = "insert into "+ hourlyTable+"(roiid,first_count,repeat_count,timeid,hour,day,weekday,week,monthweek,month,year,"
		    + "sessiontime_count_0_2,sessiontime_count_2_10,sessiontime_count_10_20,sessiontime_count_20_30,count)"
		    + " select A.roiid,A.first_count,A.repeat_count,A.timeid,A.hour,A.day,A.weekday,A.week,A.monthweek,A.month,A.year,"
		    + bounceHourlyTable+".sessiontime_count_0_2,"
		    + bounceHourlyTable+".sessiontime_count_2_10,"
		    + bounceHourlyTable+".sessiontime_count_10_20,"
		    + bounceHourlyTable+".sessiontime_count_20_30,"
		    + bounceHourlyTable+".count from "
		    + "(select roiid,sum(case when before_sampletimestamp is null then 1 else 0 end) as first_count"
		    + ",sum(case when before_sampletimestamp is not null then 1 else 0 end) as repeat_count,timeid,"
		    + "hour,day,weekday,week,monthweek,month,year from " + periodTable 
		    + " left join " + dateTable + " on "+periodTable +".sampletimestampid = " +dateTable 
		    + ".halfutcid where roiid is not null "
			+ "group by roiid,timeid,hour,day,weekday,week,monthweek,month,year) as A "
			+ " left join " + bounceHourlyTable + " on A.roiid = " + bounceHourlyTable+ ".roiid"
			+ " and A.timeid = " + bounceHourlyTable +".timeid";
		impalaCmd(cmd);
		
		//add no data hour record
		/*
		cmd = "insert into table " + hourlyTable + "(roiid,timeid,";
		for(String s: timeList){
			cmd += s + ",";
		}
		cmd += "count,first_count,repeat_count,sessiontime_count_0_2,sessiontime_count_2_10,sessiontime_count_10_20)"
			+ " select roiid,B.timeid,";
		for(String s: timeList){
			cmd += "B." + s + ",";
		}
		cmd += "0,0,0,0,0,0 from (select distinct roiid,year from " + hourlyTable 
			+ ") as A cross join (select * from " + datelistTable + " where timeid >= " + starttimeid 
			+ " and timeid < " + endtimeid + ") as B "
			+ "where  concat(A.roiid,cast(timeid as string))"
			+ " not in (select concat(roiid,cast(timeid as string)) from " + hourlyTable + ")";
		impalaCmd(cmd);	
		*/
	}


	private void updatePersonalDeviceTable() throws SQLException {
		/*
		String cityName = "";
		String dimHbase = "cim.personaldevice_hbase";
		String dimTable = "";
		String updateHbase = "cim.personaldevice_update_hbase";
		String updateTable = "";
		String periodTable = "";
		String dateTable = "";
		String cmd = "";
		String roiTable = "";
		for(int i=0; i< cityList.size() ; i++){
			cityName = cityList.get(i);
			dimTable = cityName + ".personaldevice";
			updateTable = cityName + ".personaldevice_update";
			periodTable = cityName + ".personaldevice_update_period";
			dateTable = cityName + ".datelist_half";
			roiTable = cityName + ".mobilityroi";
			
			cmd = "insert overwrite " + dimTable + "(sid,macaddress,sampletimestamp) select sid,macaddress,sampletimestamp from " + dimHbase + " where ";
			if(this.firsttime.equals("true")){
				cmd += "receivetimestamp >= 0 and receivetimestamp < " + this.stopTime;
			}else{
				cmd += "receivetimestamp >=" +this.startTime + " and receivetimestamp < " + this.stopTime;
			}
			impalaCmd(cmd);

			cmd = "insert into " + updateTable + "(sid,roiid_before,macaddress,sampletimestamp_delta,sampletimestamp,"
					+ "receivetimestamp,sampletimestampid) partition(roiid)"
					+ " select A.sid,A.roiid_before,"
					+ "A.macaddress,A.sampletimestamp_delta,A.sampletimestamp,A.receivetimestamp," 
					+ "A.sampletimestampid,A.roiid from ( select * from " + updateHbase
					+ " where receivetimestamp >= "+ startTime + " and receivetimestamp < "+ stopTime +") as A "
					+ " inner join " + roiTable + " on A.roiid = " + roiTable + ".sid or A.roiid_before = " + roiTable + ".sid";
			impalaCmd(cmd);		
		}
		*/
	}


	private void syncStatistcHourlyTable(Map statisticMap, Map tagMap,String entryName ,String city,String mode,String type) throws SQLException {
		String tmpStr = statisticMap.keySet().toString();
		String[] valueList = tmpStr.substring(1,tmpStr.length()-1).split(",");
		String tagStr = tagMap.keySet().toString();
		String[] tagList = tagStr.substring(1,tagStr.length()-1).split(",");
		
		entryName = entryName.substring(this.defaultDB.length()+1);
		String oriTable = this.defaultDB + "." + entryName.replace(".", "_") + "_hbase";
		String dimensionTable = city + ".environmentsensor"; 
		String dateTable = city + "." + this.DATEIDHALF; 
		String targetTable = city + "." + entryName.replace(".", "_");
		
		
		if(type.equals("EnvironmentData")){
			dimensionTable = city + ".environmentdata";
			targetTable = targetTable.replace("_statistic", "_statisticdata");
		}
		
		String cmd = "insert into "+ targetTable + "(";
		for(String s:valueList){
			cmd +=  s + ",";
		}
		for(String s:tagList){
			cmd +=  s + ",";
		}

		if(mode.equals("hourly") && type.equals("EnvironmentSensor")){
			
			cmd += "geocoordinates_latitude,geocoordinates_longitude,geocoordinates_altitude,";
		}
		cmd += "timeid,timezoneoffset,locationid,city,month,year,monthweek,week,weekday,day";
		if(mode.equals("hourly")){
			cmd += ",hour";
		}
		cmd +=") select ";
		for(String s:valueList){
			cmd += oriTable + "." + s + ",";
		}
		for(String s:tagList){
			cmd += oriTable + "." + s + ",";
		}
		if(mode.equals("hourly") && type.equals("EnvironmentSensor")){
			cmd += "geocoordinates_latitude,geocoordinates_longitude,geocoordinates_altitude,";
		}
		cmd += dateTable + ".timeid,"+dateTable+".timezoneoffset,M.locationid,M.city,month,year,monthweek,week,weekday,day";
		if(mode.equals("hourly")){
			cmd += "," + dateTable + ".hour";
		}
		cmd +=" from " + oriTable + " inner join (select distinct sid as msid,locationid,city from "
			+ dimensionTable + ") as M on " + oriTable + ".entityid=M.msid ";
		if(mode.equals("hourly") && type.equals("EnvironmentSensor")){
			cmd +="left join (select sid as psid,geocoordinates_latitude,geocoordinates_longitude,geocoordinates_altitude,sampletimestamp,receivetimestamp "
				+ " from cim.environmentsensor_state_hbase) as P on P.psid= "+oriTable+".entityid "
				+ " and P.receivetimestamp = " + oriTable + ".receivetimestamp";
		}	
		cmd	+= " left join " + dateTable + " on " + oriTable + ".sampletimestampid = " + dateTable + ".halfutcid"
			+ " and " + dateTable + ".city = M.city "
			+ " where " + oriTable + ".receivetimestamp >=" + startTime 
			+ " and " + oriTable + ".receivetimestamp <" + stopTime
			+ " and parententitytype = '" + type + "'"; 
		
		impalaCmd(cmd);
	}


	private void genAggStatisticTable(String entryName, List<String> cityList, Map statisticMap,Long start,Long end,String timeFilterCmd) throws SQLException {	
		String tmpStr = entryName.substring(this.defaultDB.length()+1);
		String dimTable;
		if(tmpStr.equals("PersonalDeviceMobilityStats")){
			dimTable = this.dataDB + ".mobilityroi";
		}else{
			dimTable = this.dataDB + "." + tmpStr + "_plus";
		}
		String oriTable = this.dataDB + "." + tmpStr + "_statistic";
		String dateTable = this.dataDB + "." + this.DATEIDHALF;
		String hourlyTable = this.dataDB + "." + tmpStr + "_statistic_hourly";
		String cmd = "insert into " + hourlyTable + "(sid,tenantid,";
		cmd += "entityid,entitytype,mobilitystatsname,city,";
		if(entryName.equals("cim.DirTemporalMobilityStats")){
			cmd += "temporalmobilitystatsname,bearing,directionLabel,";
		}else{
			cmd += "spatialmobilitystatsname,";
		}
		for(String s:timeList){
			cmd += s + ",";
		}
		cmd += "timeid,locationid,";
		String convertCmd = "";
	    Set tmpSet = statisticMap.entrySet();
	    Iterator tmpIter = tmpSet.iterator();  
	    while(tmpIter.hasNext()){
	    	Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tmpIter.next();
	    	String key = tmpEntry.getKey();
	        if(((Map)tmpEntry.getValue()).keySet().contains("statistic")){
	        	if(((String) ((Map)tmpEntry.getValue()).get("statistic")).equals("sum")){
	        		cmd +=  key + "_sum," + key + "_avg,";
				if(key.equals("count") && entryName.equals("cim.SpatialMobilityStats")){
					convertCmd += "cast(sum(A.count*deltatimestamp)/(sum(case when count>0 then deltatimestamp else 0 end)+0.001) as int),"
								+ "sum(A.count*deltatimestamp)/(sum(case when count>0 then deltatimestamp else 0 end)+0.001),";
	        		}else{
	        			convertCmd += "cast(sum(A." + key + ") as int),sum(A." + key + ")/count(*),";
	        		}
	        	}else if(((String) ((Map)tmpEntry.getValue()).get("statistic")).equals("avg")){
	        		cmd +=  key + "_avg," + key + "_max," + key + "_min,";
	        		if(key.equals("density")){
	        			convertCmd += "sum(A.density*deltatimestamp)/(sum(case when density>0 then deltatimestamp else 0 end) + 0.001)"
	        					+ ",max(A." + key 
		        				+ "),min(A." + key + "),";		
	        		}else{ 			
	        			convertCmd += "sum(A." + key + ")/count(*),max(A." + key 
	        				+ "),min(A." + key + "),";
	        		}
	        	}else if(((String) ((Map)tmpEntry.getValue()).get("statistic")).equals("max") || 
	        			((String) ((Map)tmpEntry.getValue()).get("statistic")).equals("min")){
	        		cmd += key + "_max," + key + "_min,";
	        		convertCmd += "max(A." + key + "),min(A." + key + "),";
	        	}
	        }
	    }
	    
	    cmd = cmd.substring(0,cmd.length()-1) + ") select sid,A.tenantid,";
	    if(tmpStr.equals("PersonalDeviceMobilityStats")){
	    	cmd += "'','','','',A.city,";
	    }else{
	    	cmd	+= "M.entityid,M.entitytype,M.mobilitystatsname,A.city,";
	    	if(entryName.equals("cim.DirTemporalMobilityStats")){
				cmd += "M.temporalmobilitystatsname,M.bearing,M.directionLabel,";
			}else{
				cmd += "M.spatialmobilitystatsname,";
			}
	    }
	    
	    for(String s:timeList){
			cmd +=  dateTable +"." + s + ",";
		}
	    cmd += dateTable + ".timeid,M.locationid,";
	    cmd += convertCmd;
	    cmd = cmd.substring(0,cmd.length()-1) + " from (select * from " + oriTable 
	    	+ " where " + timeFilterCmd + " and (";
	    for(String s:cityList){
	    	cmd += " city = '" + s + "' or";
	    }
	    cmd = cmd.substring(0, cmd.length()-2) + ") and "
	    	+ " sampletimestamp >= " + start + " and " 
	    	+ " sampletimestamp <" + end + ") as A "
	    	+ " inner join (select distinct sid as msid,tenantid as mtenantid,locationid";
	    if(!tmpStr.equals("PersonalDeviceMobilityStats")){
	    	cmd += ",entityid,entitytype,mobilitystatsname,";
			if(entryName.equals("cim.DirTemporalMobilityStats")){
				cmd += "temporalmobilitystatsname,bearing,directionLabel";
			}else{
				cmd += "spatialmobilitystatsname";
			}
		}
	    cmd += " from " + dimTable + ") as M on A.sid = M.msid "
	    	+ " and A.tenantid = M.mtenantid left join " + dateTable
	    	+ " on A.sampletimestampid = " + dateTable + ".halfutcid and " 
	    	+ dateTable + ".city = A.city "
	    	+ " group by sid,A.tenantid," + dateTable + ".timeid,M.locationid,A.city,";
	    if(!tmpStr.equals("PersonalDeviceMobilityStats")){
	    	cmd += "M.entityid,M.entitytype,M.mobilitystatsname,";
	    	if(entryName.equals("cim.DirTemporalMobilityStats")){
				cmd += "M.temporalmobilitystatsname,M.bearing,M.directionLabel,";
			}else{
				cmd += "M.spatialmobilitystatsname,";
			}
	    } 
	    
	    for(String s:timeList){
	    	cmd += dateTable +"." + s + ",";
	    }
	    cmd = cmd.substring(0,cmd.length()-1);
	    impalaCmd(cmd);
	}


	private void syncStatisticTable(Map statisticMap, String entryName ,String city) throws SQLException {
		String tmpStr = statisticMap.keySet().toString();
		String[] valueList = tmpStr.substring(1,tmpStr.length()-1).split(",");
		
		entryName = entryName.substring(this.defaultDB.length()+1);
		String oriTable = this.defaultDB + "." + entryName + "_statistic_period";
		//String oriTable = city + "." + entryName + "_statistic";
		String dimensionTable;
		
		if(entryName.equals("PersonalDeviceMobilityStats")){
			dimensionTable = city + ".spatialmobilitystats_plus" ;
		}else{
			dimensionTable = city + "." + entryName + "_plus";
		}
		String dateTable = city + "." + this.DATEIDHALF; 
		String targetTable = city + "." + entryName + "_statistic";
		String cmd = "insert into "+ targetTable + "(sid,tenantid,timezoneoffset,";
		for(String s:valueList){
			cmd +=  s + ",";
		}
		cmd += "locationid) partition(month,year,city) select sid,tenantid,"+dateTable + ".timezoneoffset,";
		for(String s:valueList){
			cmd += oriTable + "." + s + ",";
		}
		cmd += "M.locationid,"+dateTable+".month,"+dateTable+".year,M.city from " + oriTable 
			+ " inner join (select distinct  sid as msid,tenantid as mtenantid,locationid,city from "
			+ dimensionTable +") as M on " + oriTable + ".sid=M.msid and " +oriTable+ ".tenantid = M.mtenantid"
			+ " left join " + dateTable + " on " + oriTable + ".sampletimestampid = " + dateTable + ".halfutcid"
			+ " and M.city = " + dateTable + ".city"
			+ " where " + oriTable + ".receivetimestamp >=" + startTime 
			+ " and " + oriTable + ".receivetimestamp <" + stopTime;
		impalaCmd(cmd);	
		
	}


	private void updatePlusTable() throws SQLException {
		String cmd = "";
		String[] temporalValues = "bearing,directionLabel".split(",");
		String[] spatialValues = "".split(",");
		String innerTable = "";
		String dimTable = "";
            innerTable = this.dataDB + ".temporal_inner";
            dimTable = this.dataDB +".dirtemporalmobilitystats";
            cmd = "insert overwrite table " + this.dataDB + ".dirtemporalmobilitystats_plus(sid,tenantid,city,temporalstatsid,entityid,entitytype,mobilitystatsname,"
            	+ "temporalmobilitystatsname,";
            for(String s:temporalValues){
            			cmd += s + ",";
            }
            cmd += "sampletimestamp,sampletimestampid,receivetimestamp,locationid,timezoneoffset)"
            	+ " select " + dimTable + ".sid,"+dimTable+".tenantid,M.city," + dimTable + ".temporalstatsid,"
            	+ innerTable + ".entityid," + innerTable + ".entitytype,"+innerTable + ".mobilitystatsname,"
            	+ innerTable + ".temporalmobilitystatsname,";
            for(String s:temporalValues){
    			cmd += dimTable + "." +  s + ",";
            }
            cmd +=dimTable +  ".sampletimestamp," + dimTable +  ".sampletimestampid,"+ dimTable +".receivetimestamp,M.locationid,"
            	+ dimTable + ".timezoneoffset from " 
            	+ dimTable + " left join "+innerTable + " on "+ dimTable + ".sid = "+innerTable + ".sid and "
            	+ dimTable + ".tenantid = "+innerTable + ".tenantid "
            	+ " left join (select distinct sid,tenantid,city,locationid from "+this.dataDB+".mobilitypom"
            	+ " union select distinct sid,tenantid,city,locationid from "+this.dataDB+".mobilityroi"
            	+ " union select distinct sid,tenantid,city,locationid from "+this.dataDB+".roadsegment) as M "
            	+ "on "+innerTable+".entityid = M.sid and "+dimTable+".tenantid = M.tenantid"
            	+ " left join cim.locations on " +dimTable + ".locationid=cim.locations.sid and "
            	+ dimTable + ".tenantid = cim.locations.tenantid";
            impalaCmd(cmd);
            innerTable = this.dataDB + ".spatial_inner";
            dimTable = this.dataDB +".spatialmobilitystats";
            cmd = "insert overwrite table " + this.dataDB + ".spatialmobilitystats_plus(sid,tenantid,city,entityid,entitytype,mobilitystatsname,"
                	+ "spatialmobilitystatsname,sampletimestamp,sampletimestampid,receivetimestamp,locationid,timezoneoffset)"
                	+ " select " + dimTable + ".sid,"+ dimTable +".tenantid,M.city," + innerTable + ".entityid," +innerTable + ".entitytype,"
                	+ innerTable + ".mobilitystatsname,"+ innerTable + ".spatialmobilitystatsname,"
                	+ dimTable + ".sampletimestamp," + dimTable + ".sampletimestampid," + dimTable+".receivetimestamp,M.locationid,"
                	+ dimTable + ".timezoneoffset from " 
                	+ this.dataDB + ".spatialmobilitystats left join "+ innerTable + " on "+ dimTable +".sid = " + innerTable +".sid and "
                	+ dimTable + ".tenantid = "+innerTable + ".tenantid "
                	+ " left join (select distinct sid,tenantid,city,locationid from "+this.dataDB+".mobilitypom"
                	+ " union select distinct sid,tenantid,city,locationid from "+this.dataDB+".mobilityroi"
                	+ " union select distinct sid,tenantid,city,locationid from "+this.dataDB+".roadsegment) as M "
                	+ "on "+innerTable+".entityid = M.sid and "+dimTable+".tenantid = M.tenantid"
            	    + " left join cim.locations on "+dimTable+".locationid=cim.locations.sid and "
                	+ dimTable + ".tenantid = cim.locations.tenantid";
            impalaCmd(cmd);
		
	}


	private void updateInnerTable() throws SQLException {	
		String cmd = "";
        cmd = updateInnerCmd("dirtemporalmobilitystats","temporal");
        impalaCmd(cmd);
        cmd = updateInnerCmd("spatialmobilitystats","spatial");
        impalaCmd(cmd);
	}
	
	private String updateInnerCmd(String oriTable,String mode){
		String cmd = "insert overwrite table ";
		
		if(mode.equals("temporal")){
			cmd += this.dataDB + ".temporal_inner(sid,tenantid,temporalmobilitystatsname,mobilitystatsname,entityid,entitytype)"
				+ " select T.sid,T.tenantid,cim.temporalmobilitystats_connection.name as temporalmobilitystatsname,"
				+ "cim.MobilityStats_connection.name as mobilitystatsname,cim.MobilityStats_connection.entityid as entityid, "
				+ "cim.MobilityStats_connection.entitytype as entitytype from (select distinct sid,tenantid,temporalstatsid from " + this.dataDB
				+ ".dirtemporalmobilitystats) as T left join cim.temporalmobilitystats_connection on "
				+ "T.temporalStatsid=cim.temporalmobilitystats_connection.sid "
				+ " and T.tenantid=cim.temporalmobilitystats_connection.tenantid "
				+ " and cim.temporalmobilitystats_connection.entitytype="
				+"'MobilityStats' inner join cim.mobilitystats_connection on "
				+ "cim.temporalmobilitystats_connection.entityid = cim.MobilityStats_connection.sid "
				+ " and cim.temporalmobilitystats_connection.tenantid = cim.MobilityStats_connection.tenantid ";			
		}else{
			cmd += this.dataDB + ".spatial_inner(sid,tenantid,spatialmobilitystatsname,mobilitystatsname,entityid,entitytype)" 
					+ "select T.sid,T.tenantid,cim.spatialmobilitystats_connection.name as spatialmobilitystatsname,"
					+ "cim.MobilityStats_connection.name as mobilitystatsname,cim.MobilityStats_connection.entityid as entityid, " 
					+ "cim.MobilityStats_connection.entitytype as entitytype from (select distinct sid,tenantid from "+ this.dataDB
					+".spatialmobilitystats) as T left join cim.spatialmobilitystats_connection on "
					+ "T.sid=cim.spatialmobilitystats_connection.sid and "
					+ "T.tenantid=cim.spatialmobilitystats_connection.tenantid and "
					+ "cim.spatialmobilitystats_connection.entitytype='MobilityStats' "
					+ "inner join cim.mobilitystats_connection on cim.spatialmobilitystats_connection.entityid = cim.MobilityStats_connection.sid "
					+ " and cim.spatialmobilitystats_connection.tenantid = cim.MobilityStats_connection.tenantid ";			
		}
		return cmd;
	}


	/*
	private void syncDimLocationInCity(String cityName, String entryName) throws SQLException {
		String[] locValues = {"locationid","city"};
		String[] dateValues = {"timeid","year","month","week","weekday","monthweek","day","hour"};
		
		String objName = entryName.substring(this.defaultDB.length()+1);
		String dimTable = cityName + "." + objName;
		
		String locationTable = cityName + "."+ objName +"_location";
		String cmd = "insert overwrite " + locationTable +"(sid,sampletimestamp,";
		for(String s:locValues){
			cmd += s + ",";
		}
		cmd = cmd.substring(0,cmd.length()-1) + ") select "+
				dimTable +".sid,"+ dimTable +".sampletimestamp,";
		for(String s:locValues){
			cmd += dimTable +"." + s + ",";
		}
		cmd = cmd.substring(0,cmd.length()-1) + " from "+ dimTable 
				+ " inner join (select sid,max(sampletimestamp) as maxtime from "
				+ dimTable + " where sampletimestamp <=" + this.stopTime
				+ " group by sid) as D1 on "+dimTable + ".sid=D1.sid and "
				+ dimTable +".sampletimestamp=D1.maxtime";
		impalaCmd(cmd);  		
		
	}
	*/


	private void syncHierarchyTable() throws Exception {
		Set hieSet = hierarchyMap.entrySet();         
        Iterator hieIter = hieSet.iterator();
        String cmd = "";
        String objName = "";
        String values = "";
        String[] valueList = null;
        String targetTable = "";
        String oriTable = "";
        while(hieIter.hasNext()){
            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)hieIter.next();
            objName = tmpEntry.getKey().substring(this.defaultDB.length()+1);
            if(objName.equals("Locations")){
            	if(isLocationHierarchySyncd()){
            		continue;
            	}
            }
            oriTable = this.defaultDB + "."+ objName + "_hierarchy";
            values = (String) tmpEntry.getValue();
            targetTable = this.dataDB + "." + objName + "_hierarchy";        	
            cmd = "insert overwrite table " + targetTable + "(" + values 
            		+ ") select " + values + " from "+ oriTable;
            impalaCmd(cmd);
        }
	}
	
	private String getMaxSampletimeOfDateid() throws SQLException {
        String querySQL = "select max(sampletimestamp) from " + this.dataDB + "." + this.DATEIDHALF ;
        ResultSet rs = dbm.executeImpalaCmdQuery(querySQL);
        rs.next();
        String result =  rs.getString(1);
        if(result == null)
            return "empty";
        return result;
	}
	
	private void updateDateidTable() throws Exception {	
		String refreshCmd = "insert overwrite cimdata.datelist_half(timeid,halfutcid,weekday,monthweek,month,sampletimestamp,timezoneoffset,year,hour,day,city,week)"
				+ " select timeid,halfutcid,weekday,monthweek,month,sampletimestamp,timezoneoffset,year,hour,day,city,week"
				+ " from cimdata.datelist_half";
		impalaCmd(refreshCmd);
		String result = this.getMaxSampletimeOfDateid();
		Long start = (long) (Math.floor(Long.parseLong(startTime) / (3600*1000)) *3600*1000);
		Long end = (long) ((Math.floor(Long.parseLong(stopTime) / (3600*1000)) + 1)*3600*1000);
		
		if(result.equals("empty")){
				start =  Long.parseLong(this.startTime);
		}else{
			Long maxSampletime = Long.parseLong(result);
			if(maxSampletime + 3600*1000 >= end){
				return;
			}else{
				start = maxSampletime + 3600*1000;
			}
		}   	
		String timeZone = "";
		for(int i =0 ;i <cityList.size() ; i++){
			String cityName = cityList.get(i);
			timeZone= this.timezoneMap.get(cityName);
			Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timeZone)); //to be done,change timezone of the city
			Calendar calutc = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			String tmpcmd = "";  
			String tmpcmd_half="";
			Long hourTime;
			hourTime = start;
			
	    	while(hourTime < end){
	    		String offset = getOffset(hourTime,timeZone);
	    		Date tmpdate = new Date(hourTime);
	    		cal.setTime(tmpdate);
	    		calutc.setTime(tmpdate);
	    		String timeid = "" + cal.get(Calendar.YEAR) + String.format("%02d", cal.get(Calendar.MONTH)+1)
	    					+String.format("%02d", cal.get(Calendar.DAY_OF_MONTH))
	    					+String.format("%02d", cal.get(Calendar.HOUR_OF_DAY));
	    		String utcid =  "" + calutc.get(Calendar.YEAR) + String.format("%02d", calutc.get(Calendar.MONTH) + 1)
							+String.format("%02d", calutc.get(Calendar.DAY_OF_MONTH))
							+String.format("%02d", calutc.get(Calendar.HOUR_OF_DAY));
	    		String halfutcid =  "" + calutc.get(Calendar.YEAR) + String.format("%02d", calutc.get(Calendar.MONTH) + 1)
							+String.format("%02d", calutc.get(Calendar.DAY_OF_MONTH))
							+String.format("%02d", calutc.get(Calendar.HOUR_OF_DAY)*2);
	    		int week_of_year = 0;
	    		
	    		if(cal.get(Calendar.WEEK_OF_YEAR) == 1 && cal.get(Calendar.MONTH) > 10){
	    			week_of_year = 53;
	    		}else{
	    			week_of_year = cal.get(Calendar.WEEK_OF_YEAR);
	    		}
	    		tmpcmd += "(" +  hourTime + "," + cal.get(Calendar.HOUR_OF_DAY) + ","
	    				+ cal.get(Calendar.DAY_OF_MONTH)  + "," + cal.get(Calendar.DAY_OF_WEEK) + ","
	    				+ week_of_year + ","
	    				+ cal.get(Calendar.WEEK_OF_MONTH) + "," + (cal.get(Calendar.MONTH) +1 ) + ","
	    				+ cal.get(Calendar.YEAR) + "," + timeid + "," +  utcid + ",0),";
	    		tmpcmd_half += "(" +  hourTime + "," + cal.get(Calendar.HOUR_OF_DAY) + ","
	    				+ cal.get(Calendar.DAY_OF_MONTH)  + "," + cal.get(Calendar.DAY_OF_WEEK) + ","
	    				+ week_of_year + ","
	    				+ cal.get(Calendar.WEEK_OF_MONTH) + "," + (cal.get(Calendar.MONTH) +1 ) + ","
	    				+ cal.get(Calendar.YEAR) +"," + timeid + ","+ halfutcid + ","+offset+",'"+cityName+"'),";
	    		
	    		if(calutc.get(Calendar.MINUTE) == cal.get(Calendar.MINUTE)){
	    			halfutcid =  "" + calutc.get(Calendar.YEAR) + String.format("%02d", calutc.get(Calendar.MONTH) + 1)
							+String.format("%02d", calutc.get(Calendar.DAY_OF_MONTH)) + String.format("%02d", calutc.get(Calendar.HOUR_OF_DAY)*2+1);
				}else{
					tmpdate = new Date(hourTime-3600*1000);
					calutc.setTime(tmpdate);
					halfutcid =  "" + calutc.get(Calendar.YEAR) + String.format("%02d", calutc.get(Calendar.MONTH) + 1)
						+String.format("%02d", calutc.get(Calendar.DAY_OF_MONTH)) + String.format("%02d", calutc.get(Calendar.HOUR_OF_DAY)*2+1);
				}
	    		tmpcmd_half += "(" +  hourTime + "," + cal.get(Calendar.HOUR_OF_DAY) + ","
	    				+ cal.get(Calendar.DAY_OF_MONTH)  + "," + cal.get(Calendar.DAY_OF_WEEK) + ","
	    				+ week_of_year + ","
	    				+ cal.get(Calendar.WEEK_OF_MONTH) + "," + (cal.get(Calendar.MONTH) +1 ) + ","
	    				+ cal.get(Calendar.YEAR) +"," + timeid + ","+ halfutcid + ","+offset+",'"+cityName+"'),";  		
	    		hourTime += 3600*1000;
	    	}		
	    	if(tmpcmd_half.length() > 0){
	    		tmpcmd = tmpcmd.substring(0,tmpcmd.length()-1);
	    		tmpcmd_half = tmpcmd_half.substring(0,tmpcmd_half.length()-1);        
	    		String cmd = "";    
	    		String halfhourTable = this.dataDB + "." + this.DATEIDHALF;
	    		cmd = "insert into table " + halfhourTable + "(sampletimestamp,hour,day,weekday,week,monthweek,month,year,timeid,halfutcid,timezoneoffset,city) VALUES" + tmpcmd_half;
	    		impalaCmd(cmd);
			}
		}
	}

	private void joinStateHourly(List<String> cityListForHourly, Long start, Long end, String timeFilterCmd) throws SQLException {
    	String[] locValues = {"locationid","city"};
		String[] dateValues = {"timeid","year","month","week","weekday","monthweek","day","hour"};
		String[] wasteCollectionValues = {"agencyid","driverid","wastecollectiontripid","vehicleid"};
		String dimensionTable = this.dataDB + ".wastecollectionridership";
		String dateTable = this.dataDB + "." + this.DATEIDHALF;
		for(int j=0; j<stateHourlyList.size();j++){
			String oriTable = this.dataDB + "." + stateHourlyList.get(j);
			String hourlyTable = oriTable + "_hourly";
			String properties = stateHourlyMap.get(stateHourlyList.get(j));
			String cmd= "insert into table "+ hourlyTable+"(sid,tenantid,";
			for(String s:properties.split(",")){
			    cmd +=  s + "_max,";
			    cmd +=  s + "_min,";
			    cmd +=  s + "_avg,";
			    cmd +=  s + "_sum,";
			}	
			for(String s:dateValues){
	            cmd += s + ",";
	        }
			for(String s:locValues){
	            cmd += s + ",";
	        }
			for(String s:wasteCollectionValues){
				cmd += s + ",";
			}
			cmd += "count_sum) select  sid,tenantid,";
			for(String s:properties.split(",")){
				cmd += "max(" + s + "),"
					+ "min(" + s + "),"
					+ "avg(" + s + "),"
					+ "sum(" + s + "),";
			}
			for(String s:dateValues){
	            cmd += dateTable + "." + s + ",";
	        }
			for(String s:locValues){
	            cmd += "A." + s + ",";
	        }
			for(String s:wasteCollectionValues){
				cmd += "M." + s + ",";
			}
			cmd += "count(*) from  (select * from " + oriTable + " where status = 2 and "
					+ timeFilterCmd + ") as A left join (select distinct sid as msid," ;
			for(String s:wasteCollectionValues){
				cmd += s + ",";
			}
			cmd = cmd.substring(0,cmd.length()-1);
			cmd += " from " + dimensionTable + ") as M on A.sid = M.msid "
				+ " left join " + dateTable + " on A.sampletimestampid ="
				+ dateTable + ".halfutcid and A.city = " + dateTable + ".city"
				+ " group by A.sid,A.tenantid,"; 
			for(String s:dateValues){
	            cmd += dateTable +"." + s + ",";
	        }
			for(String s:locValues){
	            cmd += "A." + s + ",";
	        }
			for(String s:wasteCollectionValues){
				cmd +=  "M." + s + ",";
			}
			cmd = cmd.substring(0,cmd.length()-1);
			impalaCmd(cmd);
			
		}	
	}
	
    private void joinEventHourly(List<String> cityList,Long start,Long end,String timeFilterCmd) throws SQLException {
    	String[] locValues = {"locationid","city"};
		String[] dateValues = {"timeid","year","month","week","weekday","monthweek","day","hour"};
		String dateTable = this.dataDB + "." + this.DATEIDHALF;
		for(int j=0; j<eventHourlyList.size();j++){	
			String oriTable = this.dataDB + "." + eventHourlyList.get(j);
			String hourlyTable = oriTable + "_hourly";
			String properties = eventHourlyMap.get(eventHourlyList.get(j));
			String cmd= "insert into table "+ hourlyTable+"(";
			for(String s:properties.split(",")){
			    cmd += s + ",";
			}	
			cmd += "locationid,total_count,create_count,update_count,close_count,destroy_count,city,";
			for(String s:dateValues){
	            cmd += s + ",";
	        }
			cmd = cmd.substring(0,cmd.length()-1);
			cmd += ") select ";
			for(String s:properties.split(",")){
		    	cmd += s + ",";
			}	
			cmd+="locationid,cast(count(*) as int) as total_count,"
				+" cast(sum(case when Ori.sampletimestamp = Ori.createtimestamp then 1 else 0 end) as int) as create_count,"
				+" cast(sum(case when Ori.destroytimestamp is null and Ori.closuretime is null and Ori.sampletimestamp != Ori.createtimestamp then 1 else 0 end) as int) as update_count,"
				+" cast(sum(case when Ori.destroytimestamp is null and Ori.closuretime is not null then 1 else 0 end) as int) as close_count,"
				+" cast(sum(case when Ori.destroytimestamp is not null then 1 else 0 end) as int) as destroy_count,Ori.city,";
			for(String s:dateValues){
	            cmd += dateTable + "." + s + ",";
	        }		
			cmd = cmd.substring(0,cmd.length()-1);
			cmd +=" from (select sid,";
			for(String s:properties.split(",")){
		    	cmd += s + ",";
			}	
			cmd	+= "locationid,sampletimestamp,createtimestamp,sampletimestampid,destroytimestamp,closuretime,city"
					+" from " + oriTable +" where " + timeFilterCmd +" and (";
			for(String s:cityList){
				cmd += " city = '" + s + "' or";
			}
			cmd = cmd.substring(0, cmd.length()-2) + ") and ";
			cmd += "sampleTimestamp > " + start + " and sampleTimestamp <" +  end;
					

			cmd += ") as Ori"
				+ " inner join " + dateTable 
				+ " on " + dateTable +".city = Ori.city and "+dateTable +".halfutcid = Ori.sampletimestampid"
				+" group by ";
			for(String s:properties.split(",")){
		    	cmd += s + ",";
			}	
			cmd +="locationid,tenantid,";
			for(String s:dateValues){
	            cmd += dateTable + "." + s + ",";
	        }	
			cmd += "Ori.city";
			impalaCmd(cmd);
		}	
    }

	
    private void initDateidTable() throws ClassNotFoundException, SQLException{
      	HashMap<String, String> dateValues = new HashMap<String, String>(){{
        	put("year","smallint");
        	put("sampletimestamp","bigint");
        	put("month","smallint");
        	put("week","smallint");
        	put("weekday","smallint");
        	put("monthweek","smallint");
        	put("day","smallint");
        	put("hour","smallint");	
        	put("timeid","int");
        	put("utcid","int");
        	put("timezoneoffset","int");
        	put("city","string");
    	}};
   
    	HashMap<String, String> dateValuesHalf = new HashMap<String, String>(){{
        	put("year","smallint");
        	put("sampletimestamp","bigint");
        	put("month","smallint");
        	put("week","smallint");
        	put("weekday","smallint");
        	put("monthweek","smallint");
        	put("day","smallint");
        	put("hour","smallint");	
        	put("timeid","int");
        	put("halfutcid","int");
        	put("timezoneoffset","int");
        	put("city","string");
    	}};
    	
        this.createImpalaTable(this.dataDB, this.DATEID , dateValues,"");
        this.createImpalaTable(this.dataDB, this.DATEIDHALF , dateValuesHalf,"");
    }
    
        
	/**
	 * 
	 * @param   entry		the specified object of schema
	 * @param	tagMap		sid
	 * @param	tagList		target object name and joint property
	 * @param	tagNullList	special added value (lightzoneid)
	 * @param	mode		state/update/event
	 * @param	dim			whether need to join dimension table
	 * @return  none
	 * generate factor table join dimension of self object and their hourly table if needed
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * 
	 */ 
	private void initInnerTable(Map.Entry<String, Object> entry,Map tagMap,Map<String, String> tagList,Map<String, String> tagNullList,String mode,boolean dim) throws ClassNotFoundException, SQLException{	

		String tableName = entry.getKey().substring(this.defaultDB.length()+1) + "." + mode;
        String objectName = entry.getKey().substring(this.defaultDB.length()+1);
        String fullName = entry.getKey() + "." + mode;
        String aggValues="";
        Map factorMap =   ((Map)((Map)((Map)entry.getValue()).get("factor")).get(this.defaultDB + "." + tableName));
        if(factorMap == null){
            return;
        }
        
        HashMap<String, String> syncValues = new HashMap<String, String>();

        HashMap<String, String> syncHiveValues = new HashMap<String, String>();        
        List<String> rowkey = new ArrayList<String>();
        rowkey.add("rowkey");
        rowkey.add("string");
        
       
        HashMap<String, String> syncAggValues = new HashMap<String, String>();
        if(this.aggStateList.contains(fullName) || this.aggUpdateList.contains(fullName)){
        	for(String s: this.aggCommonValues){
        		syncAggValues.put(s.split(":")[0], s.split(":")[1]);
        	}
        	if(fullName.equals("cim.Light.state") || fullName.equals("cim.LightGrpCtrl.state")){
        		syncAggValues.put("minutepoweron_sum","int");
        	}
        }
        if(tagMap != null){
        	Set tagSet = tagMap.entrySet();   
        	Iterator tagIter = tagSet.iterator();     
	    	while(tagIter.hasNext()){
	        	Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tagIter.next();
	        	syncValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
	        	syncHiveValues.put(this.familyName+":"+tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
	    	}
        }
        Set outSet = tagList.entrySet();         
        Iterator outIter = outSet.iterator();          
        
        while(outIter.hasNext()){   	
            Map.Entry<String, Object> outEntry = (Map.Entry<String, Object>)outIter.next();
            syncValues.put(outEntry.getKey().split(":")[1], ((String)outEntry.getValue()).split(":")[1]);
            if(outEntry.getKey().split(":")[1].equals("sid")){
            	syncHiveValues.put(this.familyName + ":" + outEntry.getKey().split(":")[1],((String)outEntry.getValue()).split(":")[1]);	
            }
        }

        if(tagNullList != null){
        	outSet = tagNullList.entrySet();         
        	outIter = outSet.iterator();          
    
        	while(outIter.hasNext()){
            	Map.Entry<String, Object> outEntry = (Map.Entry<String, Object>)outIter.next();
            	syncValues.put(outEntry.getKey(), ((String)outEntry.getValue()).split(":")[1]);
        	}
        }
        Set factorSet =   factorMap.entrySet() ;
        Iterator factorIter = factorSet.iterator();  
        while(factorIter.hasNext()){
                Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)factorIter.next();
                Map valueMap = (Map)tmpEntry.getValue(); 
                if(mode.equals("update")){
                	if(!valueMap.containsKey("update") && !valueMap.get("family").equals("timestamp")){
                		syncValues.put(tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
                    	syncHiveValues.put(this.familyName + ":" + tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
                    	syncValues.put(tmpEntry.getKey() + "_before",(String) ((Map)tmpEntry.getValue()).get("type"));
                    	syncHiveValues.put(this.familyName + ":" + tmpEntry.getKey() + "_before",(String) ((Map)tmpEntry.getValue()).get("type"));
                    	syncValues.put(tmpEntry.getKey() + "_delta",(String) ((Map)tmpEntry.getValue()).get("type"));
                    	syncHiveValues.put(this.familyName + ":" + tmpEntry.getKey() + "_delta",(String) ((Map)tmpEntry.getValue()).get("type"));
                	}else{
                		syncValues.put(tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
                    	syncHiveValues.put(this.familyName + ":" + tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
                	}
                }else{
                	
                	syncValues.put(tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
                	syncHiveValues.put(this.familyName + ":" + tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
                }
        }
        
        if(this.aggStateList.contains(fullName)){
                factorIter = factorSet.iterator();         
                while(factorIter.hasNext()){
                        Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)factorIter.next();
                        if(((Map)tmpEntry.getValue()).keySet().contains("aggregate")){
                        	if(((Map)tmpEntry.getValue()).get("type").equals("boolean")){
                        		syncAggValues.put(tmpEntry.getKey() + "_avg","double");
                        	}else{
	                        	syncAggValues.put(tmpEntry.getKey() + "_max",(String) ((Map)tmpEntry.getValue()).get("type"));
	                        	syncAggValues.put(tmpEntry.getKey() + "_min",(String) ((Map)tmpEntry.getValue()).get("type"));
	                        	if(entry.getKey().endsWith("Sensor")){
	                        		syncAggValues.put(tmpEntry.getKey() + "_avg",(String) ((Map)tmpEntry.getValue()).get("type"));
	                        	}else{
	                        		syncAggValues.put(tmpEntry.getKey() + "_sum",(String) ((Map)tmpEntry.getValue()).get("type"));
	                        	}
	                        	if( ((String) ((Map)tmpEntry.getValue()).get("type")).equals("double")){
	                        		aggValues += "double:" + tmpEntry.getKey() + ",";
	                        	}else{
	                        		aggValues += tmpEntry.getKey() + ",";
	                        	}
                        	}
                        }
                        if(((Map)tmpEntry.getValue()).get("type").equals("string")){
                        	syncAggValues.put(tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
                        	aggValues += "string:" + tmpEntry.getKey() + ",";
                        }
                }
                outSet = tagList.entrySet();         
                outIter = outSet.iterator();          

                while(outIter.hasNext()){
                        Map.Entry<String, Object> outEntry = (Map.Entry<String, Object>)outIter.next();
                        syncAggValues.put(outEntry.getKey().split(":")[1], ((String)outEntry.getValue()).split(":")[1]);
                        if(!outEntry.getKey().split(":")[1].equals("sid"))
                        	aggValues += "dim:" + outEntry.getKey().split(":")[1] + ",";
                }
                if(tagNullList != null){
                        outSet = tagNullList.entrySet();         
                        outIter = outSet.iterator();          
            
                        while(outIter.hasNext()){
                            Map.Entry<String, Object> outEntry = (Map.Entry<String, Object>)outIter.next();
                            syncAggValues.put(outEntry.getKey(), ((String)outEntry.getValue()).split(":")[1]);
                            aggValues += "null:" + outEntry.getKey() + ",";
                        }
                }
                this.aggStateMap.put(fullName, aggValues.substring(0,aggValues.length()-1));
        }
        if(this.aggUpdateList.contains(fullName)){
                factorIter = factorSet.iterator();         
                while(factorIter.hasNext()){
                        Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)factorIter.next();
                        if(((Map)tmpEntry.getValue()).keySet().contains("aggregate")){
                        	Map valueMap = (Map)tmpEntry.getValue();
                        	if(((String)valueMap.get("type")).toLowerCase().equals("double")){
                        		syncAggValues.put(tmpEntry.getKey() + "_max", "double");
                        		syncAggValues.put(tmpEntry.getKey() + "_min", "double");
                        		syncAggValues.put(tmpEntry.getKey() + "_sum", "double");
                        		aggValues += "double:" + tmpEntry.getKey() + ",";
                        	}else{
                        		for(String s: this.aggUpdateValues){
                        			syncAggValues.put(tmpEntry.getKey() + s.split(":")[0], s.split(":")[1]);
                        		}
                        		aggValues += tmpEntry.getKey() + ",";
                        	}
                        }
                }
                outSet = tagList.entrySet();         
                outIter = outSet.iterator();          
                while(outIter.hasNext()){
                        Map.Entry<String, Object> outEntry = (Map.Entry<String, Object>)outIter.next();
                        syncAggValues.put(outEntry.getKey().split(":")[1], ((String)outEntry.getValue()).split(":")[1]);
                        aggValues += "dim:" + outEntry.getKey().split(":")[1] + ",";
                }
                if(tagNullList != null){
                        outSet = tagNullList.entrySet();         
                        outIter = outSet.iterator();          
            
                        while(outIter.hasNext()){
                            Map.Entry<String, Object> outEntry = (Map.Entry<String, Object>)outIter.next();
                            syncAggValues.put(outEntry.getKey(), ((String)outEntry.getValue()).split(":")[1]);
                            aggValues += "null:" + outEntry.getKey() + ",";
                        }
                }
                this.aggUpdateMap.put(fullName, aggValues.substring(0,aggValues.length()-1));
        }
        if(!syncValues.containsKey("locationId")){
        	syncValues.put(this.locationID, "string");
        }
        for(String s: this.locList){
        	syncValues.put(s, "string");
        }
        //to check
        syncValues.put("timezone", "string");
        syncValues.put("day", "int");
        Map dimMap =   (Map)((Map)((Map)entry.getValue()).get("dimension"));
        
        
        String beforeTableName = this.BEFOREPREFIX + tableName; 
        String destTableName = this.DESTPREFIX + tableName;
        if(dimMap != null){ 
                if(mode.equals("update")){
                        syncValues.put(this.beforeDimensionID, "bigint");
                        syncValues.put("receiveTimestamp","bigint");
                        this.createImpalaTable(this.dataDB,beforeTableName, syncValues,"parquet");
                        syncValues.put(this.afterDimensionID, "bigint");
                }else{
                	if(dim)
                        syncValues.put(this.dimensionID, "bigint");
                }
                
                if(mode.equals("event")){
                		syncValues.put(this.offset, "int");
                        this.createImpalaTable(this.dataDB,destTableName, syncValues,"parquet");
                }
        }else{
                if(mode.equals("event") || mode.equals("update")){
                		syncValues.put(this.offset, "int");
                        this.createImpalaTable(this.dataDB,destTableName, syncValues,"parquet");
                }
        }
        
        syncValues.put(this.offset, "int");
        
        Boolean envFlag = false;
        if(fullName.endsWith("Sensor.state") && !fullName.endsWith("EnvironmentSensor.state")){
        	envFlag = true;
        }
        if(this.aggStateList.contains(fullName) || this.aggUpdateList.contains(fullName)){
    		if(envFlag){
    			this.createImpalaTable(this.dataDB,tableName + "_hourly", syncAggValues,"");
    			this.createImpalaTable(this.dataDB,tableName.replace("Sensor", "Data") + "_hourly", syncAggValues,"");
    		}else{
    			this.createImpalaTable(this.dataDB,tableName + "_hourly", syncAggValues,"");
    		}
	    }
	    if(!tableName.equals("PersonalDevice.update")){
	    		if(envFlag){
	    			this.createImpalaTable(this.dataDB,tableName.replace("Sensor", "Data"), syncValues,"parquet");
	    			syncValues.put("geocoordinates_altitude", "double");
	    			syncValues.put("geocoordinates_longitude", "double");
	    			syncValues.put("geocoordinates_latitude", "double");
	    			this.createImpalaTable(this.dataDB,tableName, syncValues,"parquet");
	    		}else{
	    			this.createImpalaTable(this.dataDB,tableName, syncValues,"parquet");
	    		}
	    }
    
        this.createHiveTable(tableName, rowkey, syncHiveValues,"");
        
        /*
        if(tableName.equals("PersonalDevice.update")){      	
        	try {
	        	String targetTableName = "cim.PersonalDevice.update.period";        	
	        	Configuration hconf = new Configuration();
	        	hconf = HBaseConfiguration.create();
	        	hconf.set(HConstants.ZOOKEEPER_CLIENT_PORT,"2181");
	        	hconf.set(HConstants.ZOOKEEPER_QUORUM, "name-01.novalocal,cm-hue-01.novalocal,data-01.novalocal,data-02.novalocal"); 
	        	HBaseAdmin hba = new HBaseAdmin(hconf);   	
	        	if(hba.tableExists(targetTableName)  ){  
	        		if(hba.isTableEnabled(targetTableName)){
		        		hba.disableTable(targetTableName);
		        	}
	                hba.deleteTable(targetTableName);
	            }
	        	HTableDescriptor tableDescriptor = new HTableDescriptor(targetTableName);
	        	tableDescriptor.addFamily(new HColumnDescriptor("d")); 
	        	hba.createTable(tableDescriptor);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
        	this.createHiveTable(tableName+".period", rowkey, syncHiveValues,"");
        }
        */
        
        HashMap<String, String> stateValues = new HashMap<String, String>();
        for (Entry<String, String> locEntry : syncHiveValues.entrySet()) {
                stateValues.put(locEntry.getKey().split(":")[1], locEntry.getValue());
        }
        if(envFlag){
        	this.createImpalaTable(this.defaultDB,tableName.replace("Sensor", "Data")+this.PERIODSUFFIX, stateValues,"");
        	stateValues.put("geocoordinates_altitude", "double");
        	stateValues.put("geocoordinates_longitude", "double");
        	stateValues.put("geocoordinates_latitude", "double");
        	this.createImpalaTable(this.defaultDB,tableName+this.PERIODSUFFIX, stateValues,"");
        }else if(tableName.equals("Incident.event")){
        	this.createImpalaTable(this.defaultDB,tableName+this.PERIODSUFFIX, stateValues,"incident");
        }else{
        	this.createImpalaTable(this.defaultDB,tableName+this.PERIODSUFFIX, stateValues,"");
        }
    	
    }
        
        
    /**
	 * 
	 * @param   entry		the specified object of schema
	 * @param 	tagList		target object name and joint property
	 * @param	mode		dim/event
	 * @return  none
	 * generate factor and dimension table join dimension with other entity and their hourly table if needed
	 * 
	 */ 
    private void initOuterTable(Map.Entry<String, Object> entry,Map<String, String> tagList,String mode) throws ClassNotFoundException, SQLException{
    	
    	HashMap<String, String> syncValues = new HashMap<String, String>();
        HashMap<String, String> syncValuesBefore = new HashMap<String, String>();
        HashMap<String, String> syncHiveValues = new HashMap<String, String>();
        String projectName = entry.getKey().split("\\.")[0];
        String tableName = "";
        String beforeTableName = "";
        String aggValues = "";
        if(mode.equals("dim")){
            tableName = entry.getKey().substring(projectName.length()+1);
        }
        else if(mode.equals("event")){
            tableName = entry.getKey().substring(projectName.length()+1) + ".event";
        }
        else if(mode.equals("update")){       	
        	tableName = entry.getKey().substring(projectName.length()+1) + ".update";
        }
        
        else
            return;
            
        String entryName = entry.getKey();

        Map tagMap = (Map)((Map)entry.getValue()).get("tag");
        if(tagMap != null){
	        Set tagSet = tagMap.entrySet();         
	        Iterator tagIter = tagSet.iterator();          
	    
	        while(tagIter.hasNext()){
	            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tagIter.next();
	            syncValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
	            syncHiveValues.put(this.familyName+":"+tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
	        }
        }
        syncValues.put(this.locationID, "string");
        for(String s: this.locList){
        	syncValues.put(s, "string");
        }
        syncValues.put(this.offset, "int");
        syncValues.put("timezone", "string");
      
      
        HashMap<String, String> syncAggValues = new HashMap<String, String>();        
        
        Map outMap = null;
        Map syncMap = null;
        if (mode.equals("dim")){
            outMap = (Map) ((Map) ((Map)entry.getValue()).get("dimension"));
            syncMap = (Map) outMap.get(entryName);
        }else if(mode.equals("event")){
            outMap = (Map) ((Map) ((Map)entry.getValue()).get("factor"));
            syncMap = (Map) outMap.get(entryName+".event");
        }else{
            outMap = (Map) ((Map) ((Map)entry.getValue()).get("factor"));
            syncMap = (Map) outMap.get(entryName+".update");
        }
        Set syncSet = syncMap.entrySet();         
        Iterator syncIter = syncSet.iterator();          
        while(syncIter.hasNext()){      
             Map.Entry<String, Object> syncEntry=(Map.Entry<String, Object>)syncIter.next();
             Map valueMap = (Map)syncEntry.getValue();
             if(mode.equals("update") && !valueMap.containsKey("update") && !valueMap.get("family").equals("timestamp")){
            	 syncValues.put(syncEntry.getKey(), (String) ((Map)syncEntry.getValue()).get("type"));
                 syncHiveValues.put( this.familyName +":"+syncEntry.getKey(),(String) ((Map)syncEntry.getValue()).get("type"));
                 syncValues.put(syncEntry.getKey() + "_before", (String) ((Map)syncEntry.getValue()).get("type"));
                 syncHiveValues.put( this.familyName +":"+syncEntry.getKey() + "_before",(String) ((Map)syncEntry.getValue()).get("type"));
             }
             syncValues.put(syncEntry.getKey(), (String) ((Map)syncEntry.getValue()).get("type"));
             syncHiveValues.put( this.familyName +":"+syncEntry.getKey(),(String) ((Map)syncEntry.getValue()).get("type"));
        } 
        if(mode.equals("event") || mode.equals("update")){
        	syncValues.put("day", "int");
        }
        List<String> rowkey = new ArrayList<String>();
        rowkey.add("rowkey");
        rowkey.add("string");
        Set outSet = tagList.entrySet();         
        Iterator outIter = outSet.iterator();          
   
        while(outIter.hasNext()){
            Map.Entry<String, Object> outEntry = (Map.Entry<String, Object>)outIter.next();
            syncValues.put(outEntry.getKey().split(":")[1], ((String)outEntry.getValue()).split(":")[1]);
            if(mode.equals("update")){     	
            	syncValues.put(outEntry.getKey().split(":")[0].substring(projectName.length()+1) + this.beforeDimensionID, "bigint");
            	syncValuesBefore.putAll(syncValues);
            }
            syncValues.put(outEntry.getKey().split(":")[0].substring(projectName.length()+1) + this.dimensionID, "bigint");     
            syncHiveValues.put( ((String)outEntry.getValue()).split(":")[0] + ":" + outEntry.getKey().split(":")[1] , ((String)outEntry.getValue()).split(":")[1]);
        }

        this.createHiveTable(tableName, rowkey, syncHiveValues,"");
        if(mode.equals("dim")){
            HashMap<String, String> locationValues = new HashMap<String, String>();
            for (Entry<String, String> locEntry : syncHiveValues.entrySet()) {
                locationValues.put(locEntry.getKey().split(":")[1], locEntry.getValue());
            }
            locationValues.put(this.locationID, "String");
            
        	for(String s: this.locList){
        		locationValues.put(s, "String");
        	}
            
            locationValues.put("timezone", "String");
            this.createImpalaTable(this.defaultDB,tableName+this.LOCATIONSUFFIX, locationValues,"");
        }
        else{
            HashMap<String, String> stateValues = new HashMap<String, String>();
            for (Entry<String, String> locEntry : syncHiveValues.entrySet()) {
                stateValues.put(locEntry.getKey().split(":")[1], locEntry.getValue());
            }
            this.createImpalaTable(this.defaultDB,tableName+this.PERIODSUFFIX, stateValues,"");
        }
        if(mode.equals("update")){
         	this.createImpalaTable(this.dataDB,tableName, syncValues,"parquet");
        }
        if(mode.equals("event")){
        	this.createImpalaTable(this.dataDB,tableName, syncValues,"parquet");
            this.createImpalaTable(this.dataDB,this.DESTPREFIX + tableName, syncValues,"parquet");
        }
        if(mode.equals("dim")){
        	this.createImpalaTable(this.dataDB,tableName, syncValues,"");
        }
        
    }
    
    private void initStatisticTable(Entry<String, Object> entry, Map tagMap,String mode) throws ClassNotFoundException, SQLException {
		String hbaseTableName = entry.getKey().substring(this.defaultDB.length()+1);
		String tableName = hbaseTableName.replace(".", "_");
		String hourlyTableName = "";
		if(mode.equals("")){
			hourlyTableName = tableName + "_hourly";
		}else{
			hourlyTableName = tableName;
		}
		
		
		HashMap<String, String> syncValues = new HashMap<String, String>();
		HashMap<String, String> syncHourlyValues = new HashMap<String, String>();
        HashMap<String, String> syncHiveValues = new HashMap<String, String>();
        if(tagMap != null){
	        Set tagSet = tagMap.entrySet();         
	        Iterator tagIter = tagSet.iterator();            
	        while(tagIter.hasNext()){
	            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tagIter.next();
	            syncValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
	            syncHourlyValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
	            syncHiveValues.put(this.familyName+":"+tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
	        }
        }
        Set tmpSet = ((Map)entry.getValue()).entrySet();
        Iterator tmpIter = tmpSet.iterator();  
        while(tmpIter.hasNext()){
            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tmpIter.next();
            syncValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
            syncHiveValues.put(this.familyName+":"+tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
            if(((Map)tmpEntry.getValue()).keySet().contains("statistic")){
            	if(((String) ((Map)tmpEntry.getValue()).get("statistic")).equals("sum")){
            		syncHourlyValues.put(tmpEntry.getKey() + "_sum",(String) ((Map)tmpEntry.getValue()).get("type") );
            		syncHourlyValues.put(tmpEntry.getKey() + "_avg","Double" );
            	}else if(((String) ((Map)tmpEntry.getValue()).get("statistic")).equals("avg")){
            		syncHourlyValues.put(tmpEntry.getKey() + "_min",(String) ((Map)tmpEntry.getValue()).get("type") );
            		syncHourlyValues.put(tmpEntry.getKey() + "_max",(String) ((Map)tmpEntry.getValue()).get("type") );
            		syncHourlyValues.put(tmpEntry.getKey() + "_avg","Double" );
            	}else if(((String) ((Map)tmpEntry.getValue()).get("statistic")).equals("max") || ((String) ((Map)tmpEntry.getValue()).get("statistic")).equals("min")){
            		syncHourlyValues.put(tmpEntry.getKey() + "_min",(String) ((Map)tmpEntry.getValue()).get("type") );
            		syncHourlyValues.put(tmpEntry.getKey() + "_max",(String) ((Map)tmpEntry.getValue()).get("type") );
            	}
            }
            if(!mode.equals("")){
            	syncHourlyValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
            }
        }
        syncValues.put("locationid","String");
        syncValues.put("timezoneoffset","int");
        syncValues.put("year","int");
        syncValues.put("month","int");
        if(mode.equals("")){
        	syncHourlyValues.put("entityid","String" );
        	syncHourlyValues.put("entitytype","String" );
        	syncHourlyValues.put("mobilitystatsname","String" );
		}
		if(tableName.equals("DirTemporalMobilityStats_statistic")){
			syncHourlyValues.put("temporalmobilitystatsname","String" );
			syncHourlyValues.put("bearing","int" );
			syncHourlyValues.put("directionLabel", "string");
		}else if(tableName.equals("SpatialMobilityStats_statistic") 
				|| tableName.equals("PersonalDeviceMobilityStats_statistic")){
			syncHourlyValues.put("spatialmobilitystatsname","String" );
		}
		syncHourlyValues.put("timeid","int" );
		syncHourlyValues.put("city","string" );
		syncHourlyValues.put("locationid","string" );
		syncHourlyValues.put("timezoneoffset","int");
		syncValues.put("city", "string");
		for(String s :timeList){
			syncHourlyValues.put(s,"int" );
		}
		List<String> rowkey = new ArrayList<String>();
        rowkey.add("rowkey");
        rowkey.add("string");
		
        this.createHiveTable(hbaseTableName, rowkey, syncHiveValues,"");
        
        syncValues.put("city", "string");
        this.createImpalaTable(this.defaultDB,tableName + this.PERIODSUFFIX, syncValues,"");
        if(mode.equals(""))
        	this.createImpalaTable(this.dataDB,tableName, syncValues,"parquet");
        if(!mode.equals("")){
        	this.createImpalaTable(this.dataDB,hourlyTableName.replace("_statistic", "_statisticdata"), syncHourlyValues,"");
        	syncHourlyValues.put("geocoordinates_latitude", "double");
        	syncHourlyValues.put("geocoordinates_longitude", "double");
        	syncHourlyValues.put("geocoordinates_altitude", "double");   	
        }
        this.createImpalaTable(this.dataDB,hourlyTableName, syncHourlyValues,"");
        
    }

    private void initConnectionTable(String tableName) throws ClassNotFoundException, SQLException{
    	HashMap<String, String> syncHiveValues = new HashMap<String, String>();
    	for(String s:this.connectValues){
    		syncHiveValues.put(this.familyName + ":" + s.split(":")[0],s.split(":")[1]);
    	}
    	syncHiveValues.put(this.familyName + ":tenantId","String");
    	List<String> rowkey = new ArrayList<String>();
        rowkey.add("rowkey");
        rowkey.add("string");
        tableName = tableName.substring(this.defaultDB.length()+1);
        this.createHiveTable(tableName, rowkey, syncHiveValues,"single");
    }
    
    /**
	 * 
	 * @param   entry		the specified object of schema
	 * @param	tagMap		target object name and joint property
	 * @param	tagNullMap		target object name and joint property
	 * @param	mode		dim/event
	 * @return  none
	 * generate dimension table not need to join other dimension table
	 * 
	 */ 
    private void initDimension(Map.Entry<String, Object> dimEntry,Map tagMap,Map tagNullMap ,String mode) throws ClassNotFoundException, SQLException{
    	HashMap<String, String> syncValues = new HashMap<String, String>();
        HashMap<String, String> syncHiveValues = new HashMap<String, String>();
        String projectName = dimEntry.getKey().split("\\.")[0];
        String tableName = dimEntry.getKey().substring(projectName.length()+1); 
        Set tagSet = tagMap.entrySet();         
        Iterator tagIter = tagSet.iterator();          
        
        while(tagIter.hasNext()){
            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tagIter.next();
            syncValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
            syncHiveValues.put(this.familyName +":"+tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
        }
        
        if(tagNullMap != null){
        	tagSet = tagNullMap.entrySet();         
            tagIter = tagSet.iterator();          
                
            while(tagIter.hasNext()){
                Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)tagIter.next();
                syncValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
                syncHiveValues.put(this.familyName +":"+tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
            }
        }
        Set syncSet = ((Map) dimEntry.getValue()).entrySet();         
        Iterator syncIter = syncSet.iterator();          
        while(syncIter.hasNext()){      
             Map.Entry<String, Object> syncEntry=(Map.Entry<String, Object>)syncIter.next();
             syncValues.put(syncEntry.getKey(), (String) ((Map)syncEntry.getValue()).get("type"));
             syncHiveValues.put( this.familyName+":"+syncEntry.getKey(),(String) ((Map)syncEntry.getValue()).get("type"));
        } 
        List<String> rowkey = new ArrayList<String>();
        rowkey.add("rowkey");
        rowkey.add("string");

        this.createHiveTable(tableName, rowkey, syncHiveValues,mode);
        if(mode.equals("skip")){
        	this.createImpalaTable(this.dataDB,tableName, syncValues,"");
        	return;
        }
        if(mode.equals("single")){
       		return;
        }
        syncValues.put(this.locationID, "string");
        for(String s: this.locList){
        	syncValues.put(s, "string");
        }
        syncValues.put("timezone", "String");
        syncValues.put(this.offset, "int");
        HashMap<String, String> locationValues = new HashMap<String, String>();
        for (Entry<String, String> locEntry : syncHiveValues.entrySet()) {
            locationValues.put(locEntry.getKey().split(":")[1], locEntry.getValue());
        }
        locationValues.put(this.locationID, "String");
        for(String s: this.locList){
        	locationValues.put(s, "String");
        }
        locationValues.put("timezone", "String");  
        if(!this.skipEntry.contains(tableName)){
        		this.createImpalaTable(this.defaultDB,tableName+this.LOCATIONSUFFIX, locationValues,"");
        		this.createImpalaTable(this.dataDB,tableName, syncValues,"");
        }
        if(tableName.equals("DirTemporalMobilityStats") || tableName.equals("SpatialMobilityStats") ){
        	syncValues.put("entitytype", "string");
        	syncValues.put("entityid", "string");
        	syncValues.put("mobilitystatsname","string");
        	if(tableName.equals("DirTemporalMobilityStats")){
        		syncValues.put("temporalmobilitystatsname","string");
        	}else{
        		syncValues.put("spatialmobilitystatsname","string");
        	}
            this.createImpalaTable(this.dataDB,tableName + "_plus", syncValues,"");
        }    
    }
    
    
    private void initHierarchyTable(Map<String, Object> hierarchyTable,String objName) throws Exception{
    	String tableName = objName.substring(this.defaultDB.length()+1) + "_hierarchy";
    	HashMap<String, Object> propertyMap = (HashMap<String, Object>) hierarchyTable.get(objName+".hierarchy");
    	HashMap<String, String> syncValues = new HashMap<String, String>();
        HashMap<String, String> syncHiveValues = new HashMap<String, String>();
        List<String> rowkey = new ArrayList<String>();
        rowkey.add("rowkey");
        rowkey.add("string");
        Set propertySet = propertyMap.entrySet();         
        Iterator propertyIter = propertySet.iterator();          
        
        while(propertyIter.hasNext()){
            Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)propertyIter.next();
            syncValues.put(tmpEntry.getKey(), (String) ((Map)tmpEntry.getValue()).get("type"));
            syncHiveValues.put(this.familyName +":"+tmpEntry.getKey(),(String) ((Map)tmpEntry.getValue()).get("type"));
        }
        syncValues.put("tenantid","string");
        syncHiveValues.put(this.familyName +":tenantId","String");
        this.hierarchyMap.put(objName,StringUtils.join(syncValues.keySet(),","));
        this.createHiveTable(tableName.replace("_", "."), rowkey, syncHiveValues,"single");
        this.createImpalaTable(this.dataDB,tableName, syncValues,"");   	
    }

    /**
	 * 
	 * @param   entryName	
	 * @param	cityName
	 * @param	values		values to handle 
	 * @return  none
	 * the general function to generate update hourly table sql and run it
	 * 
	 */ 
    private void genAggUpdateTable(String entryName, List<String> cityList, String values,
    		Long start,Long end,String timeFilterCmd) throws SQLException {
    	String updateTable = this.dataDB + "." + entryName.substring(this.defaultDB.length() + 1).replace(".", "_") + "_update";	
		String dimensionTable = this.dataDB + "." + entryName.substring(this.defaultDB.length()+1);
		String dateTable = this.dataDB + "." + this.DATEIDHALF;
		String[] tmpValues = values.split(",");
		String tmpLocString = "";		
		for(int i=0; i<tmpValues.length ; i++){
			if(tmpValues[i].startsWith("dim:") || tmpValues[i].startsWith("double:")){
				tmpLocString += tmpValues[i] + ",";
			}else{
				tmpLocString += tmpValues[i] + ",";
				tmpLocString += tmpValues[i] + "_delta,";
			}	
		}
		tmpLocString += "sampletimestamp_delta";
		
		String[] updateValues = tmpLocString.split(",");
		String cmdtmp = genLocationTimeCmd(updateTable,dimensionTable,dateTable,updateValues,start,end,cityList,timeFilterCmd);
		String[] commonValues = {"sid","tenantid","locationid","city",
				"timeid","year","month","week","weekday","monthweek","day","hour"};
		String cmd = "insert into "+ this.dataDB + "." + entryName.substring(this.defaultDB.length()+1) + "_update_hourly( ";
		for(int i=0; i<commonValues.length ; i++){
			cmd += commonValues[i] +",";
		}
		for(int i=0; i<tmpValues.length ; i++){
			if(tmpValues[i].startsWith("dim:")){
				cmd += tmpValues[i].split(":")[1] + ",";
			}else{
				if(tmpValues[i].startsWith("double:")){
					cmd += tmpValues[i].split(":")[1] + "_max,";
					cmd += tmpValues[i].split(":")[1] + "_min,";
					cmd += tmpValues[i].split(":")[1] + "_sum,";
				}else{
					for(String s: this.aggUpdateValues){
						cmd += tmpValues[i] +  s.split(":")[0] + ",";
					}
				}
			}
		}
		cmd +=  "count) select ";
		for(int i=0; i<commonValues.length ; i++){
			cmd +=  "T." + commonValues[i] +",";
		}
		
		for(int i=0; i<tmpValues.length ; i++){
			if(tmpValues[i].startsWith("dim:")){
				cmd += "T." + tmpValues[i].split(":")[1] + ",";
			}else if(tmpValues[i].startsWith("double:")){
				cmd += "max(T."+ tmpValues[i].split(":")[1] + "),";
				cmd += "min(T."+ tmpValues[i].split(":")[1] + "),";
				cmd += "sum(T."+ tmpValues[i].split(":")[1] + "),";
			}else{
                String dValue = "T.sampletimestamp_delta";
                cmd += "sum(case when T." + tmpValues[i] + "_delta=false then "+ dValue + " else 0 end),"
                        + "max(case when T." + tmpValues[i] + "_delta=false then "+ dValue + " else 0 end),"
                        +"case when sum(case when T."+ tmpValues[i] + "_delta=false then 1 else 0 end)!=0 then min(case when T."
                        +tmpValues[i] + "_delta=false then "+dValue + " end) else 0 end,"
                        + "sum(case when T." + tmpValues[i] + "_delta=true then "+ dValue + " else 0 end),"
                        + "max(case when T." + tmpValues[i] + "_delta=true then "+ dValue + " else 0 end),"
                        +"case when sum(case when T."+ tmpValues[i] + "_delta=true then 1 else 0 end)!=0 then min(case when T."
                        +tmpValues[i] + "_delta=true then "+dValue + " end) else 0 end,"
                        + "CAST(sum(case when T." + tmpValues[i] + "_delta=true and T.sampletimestamp_delta>0 then 1 else 0 end) as int),"
                        + "CAST(sum(case when T." + tmpValues[i] + "_delta=false and T.sampletimestamp_delta>0 then 1 else 0 end) as int),";
                cmd = addTimeRangeValueCmd(cmd,tmpValues[i],dValue);
			}
		}
		cmd += "CAST(count(*) as int) ";
		cmd += " from (" + cmdtmp + ") as T group by ";
		for(int i=0; i<commonValues.length ; i++){
			cmd +=  "T." + commonValues[i] +",";
		}
		for(int i=0; i<tmpValues.length ; i++){
			if(tmpValues[i].startsWith("dim:")){
				cmd += "T." + tmpValues[i].split(":")[1] + ",";
			}
		}
		cmd = cmd.substring(0,cmd.length()-1);
		
		impalaCmd(cmd);
	}

    
    /**
	 * 
	 * @param   cmd			origin cmd
	 * @param 	tmpValue	value to add time range
	 * @param	dValue		"T.sampletimestamp_delta"	
	 * @return  cmd			cmd added time range value
	 * the general function to generate update hourly table sql and run it
	 * 
	 */ 
	private String addTimeRangeValueCmd(String cmd,String tmpValue,String dValue) {
		String tmpStart = "";
		String tmpEnd = "";
        for(String s: this.aggUpdateNumber){
        	tmpStart = Integer.parseInt(s.split(",")[0])*60*1000 + "";
        	if(s.split(",")[1].equals("-1")){
        		cmd += "CAST(sum(case when T." + tmpValue + "_delta=true and " + dValue +">="+ tmpStart+ " then 1 else 0 end) as int),";
        	}else{
        		tmpEnd = Integer.parseInt(s.split(",")[1])*60*1000 + "";
        		cmd += "CAST(sum(case when T." + tmpValue + "_delta=true and " + dValue +">="+ tmpStart+" and "+ dValue+ "<" + tmpEnd + " then 1 else 0 end) as int),";
        	}
		}
        for(String s: this.aggUpdateNumber){
        	tmpStart = Integer.parseInt(s.split(",")[0])*60*1000 + "";
        	if(s.split(",")[1].equals("-1")){
        		cmd += "CAST(sum(case when " + tmpValue + "_delta=false and " + dValue +">="+ tmpStart+ " then 1 else 0 end) as int),";
        	}else{
        		tmpEnd = Integer.parseInt(s.split(",")[1])*60*1000 + "";
        		cmd += "CAST(sum(case when " + tmpValue + "_delta=false and " + dValue +">="+ tmpStart+" and "+ dValue+ "<" + tmpEnd + " then 1 else 0 end) as int),";
        	}
		}
		return cmd;
	}

    /**
	 * 
	 * @param   entryName
	 * @param	cityName	
	 * @param	values		string:value/dim:value/null:value/value
	 * @param	stateName 	statename of hourly table(such as roadsegment_state_vehicleclassvalue)
	 * @return  none
	 * the general function to generate state hourly table sql and run it
	 * 
	 */ 
	private void genAggStateTable(String entryName, List<String> cityList, String values,
			Long start,Long end,String timeFilterCmd) throws SQLException {
		String stateName = entryName.substring(this.defaultDB.length() + 1).replace(".", "_");	
		String stateTable = this.dataDB + "." + stateName;
		String dimensionTable;
		dimensionTable = this.dataDB + "." + stateName.split("_")[0];
		String dateTable = this.dataDB + "." + this.DATEIDHALF;
		String[] stateValues = values.split(",");
		String cmdtmp = genLocationTimeCmd(stateTable,dimensionTable,dateTable,stateValues,start,end,cityList,timeFilterCmd);
		String[] commonValues = {"sid","tenantid","locationid","city",
				"timeid","year","month","week","weekday","monthweek","day","hour"};
		
		String cmd = "insert into "+ this.dataDB + "." + stateName + "_hourly( ";
		for(int i=0; i<commonValues.length ; i++){
			cmd += commonValues[i] +",";
		}
		for(int i=0; i<stateValues.length ; i++){
			if(stateValues[i].startsWith("string:") || stateValues[i].startsWith("dim:") || 
					stateValues[i].startsWith("null:")){
				cmd += stateValues[i].split(":")[1] + ",";
			}else{
				if(stateValues[i].startsWith("double:")){
					cmd += stateValues[i].split(":")[1] + "_max,";
					cmd += stateValues[i].split(":")[1] + "_min,";
					cmd += stateValues[i].split(":")[1] + "_sum,";	
				}else{
					cmd += stateValues[i] + "_max,";
					cmd += stateValues[i] + "_min,";
					cmd += stateValues[i] + "_sum,";	
				}
			}
		}
		if(entryName.equals("cim.Light.state") || entryName.equals("cim.LightGrpCtrl.state")){
			cmd += "minutepoweron_sum,";
		}
		cmd += "count )";
		cmd += "select ";
		for(int i=0; i<commonValues.length ; i++){
			cmd +=  "T." + commonValues[i] +",";
		}
		for(int i=0; i<stateValues.length ; i++){
			if(stateValues[i].startsWith("string:") || stateValues[i].startsWith("dim:") ||
					stateValues[i].startsWith("null:")){
				cmd += "T." + stateValues[i].split(":")[1] + ",";
			}else{
				if(stateValues[i].startsWith("double:")){			
					if(stateValues[i].split(":")[1].toLowerCase().equals(this.POWERCONSUMPTION)){
						cmd += "max(case when T." + stateValues[i].split(":")[1] + ">=0 then T."+stateValues[i].split(":")[1] 
								+" else 0 end),";
						cmd += "min(case when T." + stateValues[i].split(":")[1] + ">=0 then T."+stateValues[i].split(":")[1] 
								+" else 0 end),";
						cmd += "sum(T." + stateValues[i].split(":")[1] 
							+ ")+sum(case when T."+ stateValues[i].split(":")[1] +"<0 then 1 else 0 end),";
					}else{
						cmd += "max(T." + stateValues[i].split(":")[1] + "),";
						cmd += "min(T." + stateValues[i].split(":")[1] + "),";
						cmd += "sum(T." + stateValues[i].split(":")[1] + "),";						
					}
				}else{
					cmd += "max(T." + stateValues[i] + "),";
					cmd += "min(T." + stateValues[i] + "),";
					cmd += "CAST(sum(T." + stateValues[i] + ") as int),";
				}
				
			}
		}
		if(entryName.equals("cim.Light.state") || entryName.equals("cim.LightGrpCtrl.state")){
			cmd += "CAST(sum(case when T.intensityLevel > 0 then 1 else 0 end)/count(*)*60 as int),";
		}
		cmd += "CAST(count(*) as int) ";
		cmd += " from (" + cmdtmp + ") as T group by ";
		for(int i=0; i<commonValues.length ; i++){
			cmd +=  "T." + commonValues[i] +",";
		}
		for(int i=0; i<stateValues.length ; i++){
			if(stateValues[i].startsWith("string:") || stateValues[i].startsWith("dim:") ||
					stateValues[i].startsWith("null:")){
				cmd += "T." +  stateValues[i].split(":")[1] + ",";
			}
		}
		cmd = cmd.substring(0,cmd.length()-1);
		
		impalaCmd(cmd);
	}

	
	
    /**
	 * 
	 * @param 	oriTable
	 * @param 	dimensionTable
	 * @param	dateTable
	 * @param	values		values to join   
     * @param timeFilterCmd 
     * @param cityList 
	 * @return  cmd			internal cmd
	 * join dateid and dimension table to get time and location info
	 * 
	 */ 
	private String genLocationTimeCmd(String oriTable, String dimensionTable, String dateTable,String[] values,
			Long start,Long end, List<String> cityList, String timeFilterCmd) {
		String cmd = "select A.sid as sid,A.tenantid as tenantid,";
		String[] locValues = {"locationid","city"};
		String[] dateValues = {"timeid","year","month","week","weekday","monthweek","day","hour"};
		for(int i=0; i<values.length ; i++){
			if(values[i].startsWith("string:")){
				cmd +=  "A." + values[i].split(":")[1] + " as " + values[i].split(":")[1] +",";
			}else if(values[i].startsWith("dim:") || values[i].startsWith("null:")){
				cmd += dimensionTable + "." + values[i].split(":")[1] + " as " + values[i].split(":")[1] +",";
			}else{
				if(values[i].startsWith("double:")){
					cmd += "A." + values[i].split(":")[1] + " as " + values[i].split(":")[1] +",";
				}else{
					cmd +=  "A." + values[i] + " as " + values[i] +",";
				}
			}
		}
		for(int i=0; i<dateValues.length ; i++){
			cmd += dateTable + "." + dateValues[i] + " as " + dateValues[i] + ",";
		}
		for(int i=0; i<locValues.length ; i++){
			cmd += dimensionTable + "." + locValues[i] + " as " + locValues[i] + ",";
		}
		cmd = cmd.substring(0,cmd.length()-1);
		cmd += " from (select * from " + oriTable + " where " + timeFilterCmd +" and (";
		for(String s:cityList){
			cmd += " city = '" + s + "' or";
		}
		cmd = cmd.substring(0, cmd.length()-2) + ") and ";
		cmd += "sampleTimestamp > " + start + " and sampleTimestamp <" +  end
			+ ") as A left join " + dateTable + " on " 
		    + "A.sampleTimestampid=" + dateTable + ".halfutcid and A.city = " + dateTable + ".city ";
		cmd += " inner join " + dimensionTable +" on " + dimensionTable + ".sid = A.sid "
				+ " and " + dimensionTable + ".tenantid = A.tenantid ";		
		return cmd;
	}


   
    /**
	 * 
	 * @param 	map
	 * @return	none
	 * sync location table to city db
	 * 
	 */ 
	private void syncLocation(Map<String, Object> map) throws SQLException {
        ArrayList<String> valList = new ArrayList<String>();
        valList.addAll(((Map)map.get("tag")).keySet());
        valList.addAll(((Map)((Map)((Map)map.get("dimension"))).get(this.defaultDB+".Locations")).keySet()  );
		String cmd = "insert overwrite table ";
		String tmpCmd = cmd + this.dataDB + ".locations(";
		for(int j=0;j<valList.size();j++){
			tmpCmd += valList.get(j) + ",";
		}
		tmpCmd = tmpCmd.substring(0,tmpCmd.length()-1) + ") select ";
		for(int j=0;j<valList.size();j++){
			tmpCmd += this.defaultDB+".locations." + valList.get(j) + ",";
		}
		tmpCmd = tmpCmd.substring(0,tmpCmd.length()-1);
		tmpCmd += " from " + this.defaultDB + ".locations";
		impalaCmd(tmpCmd);
	}

	
    
    /**
    *  
    * @param tableName
    * @param vals
    * @param map
    * @throws SQLException
    * 
    * the factor data to be handle of the current period is extracted first to corresponding table in the default db 
    */
    private void getPeriodFactorData(String tableName, List<String> vals, Map<String,Object> eventMap,String mode) throws SQLException {
    	boolean isIncident = false;
    	Map<String,Object> map = new HashMap<String,Object>(eventMap);
    	if(tableName.equals("cim.Incident_event")){
    		isIncident = true;
    		map.remove("incidentType");		
    	}
    	List<String> valList = new ArrayList<String>(vals);
    	Set objSet = map.entrySet() ;
		Iterator objIter = objSet.iterator(); 
		String envMode = "";
		if(mode.endsWith("envData")){
			envMode = "data";
			//tableName.replace("Sensor", "Data");
		}else if(mode.endsWith("envSensor")){
			envMode = "sensor";
		}
	    while(objIter.hasNext()){
	                Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)objIter.next();
	                Map valueMap = (Map)tmpEntry.getValue();                 
	                if(valueMap.containsKey("aggregate") && !valueMap.containsKey("update") && mode.equals("update")){
	                		valList.add(tmpEntry.getKey());
	                		valList.add(tmpEntry.getKey() + "_before");
	                		if(valueMap.get("type").equals("boolean")){
	                			valList.add(tmpEntry.getKey() + "_delta");
	                		}
	                }else{
	                	valList.add(tmpEntry.getKey());
	                }            
	    }
	    if(!valList.contains("sid")){
	    	valList.add("sid");
	    }
    	tableName = tableName.substring(this.defaultDB.length()+1).replace(".", "_");
    	String insertCmd = "insert overwrite table " + this.defaultDB + "." + tableName + this.PERIODSUFFIX ;
    	if(mode.endsWith("envData")){
    		insertCmd = "insert overwrite table " + this.defaultDB + "." + tableName.replace("Sensor", "Data") + this.PERIODSUFFIX ;
    	}
    			
    	String selectCmd =  " (";
        for(int j=0;j<valList.size();j++){
                selectCmd += valList.get(j) + ",";
        }
        selectCmd = selectCmd.substring(0,selectCmd.length()-1) + ") ";
        if(isIncident){
        	selectCmd += " partition(incidentType) ";
        }
        selectCmd += "select ";
        for(int j=0;j<valList.size();j++){
                selectCmd += this.defaultDB+"."+tableName+this.HBASESUFFIX + "."+valList.get(j) + ",";
        }
        if(isIncident){
        	 selectCmd += "incidentType,"; 
        }
        selectCmd = selectCmd.substring(0,selectCmd.length()-1);
    	selectCmd += " from " + this.defaultDB + "." + tableName + this.HBASESUFFIX;
    	selectCmd += " where ";
    	if(this.firsttime.equals("true") && 
    			(tableName.equals("ParkingSpot_state") || tableName.endsWith("Sensor_state"))){
    		selectCmd += this.defaultDB+"."+tableName+this.HBASESUFFIX + ".receiveTimestamp >=0 and ";
    	}else{
    		selectCmd += this.defaultDB+"."+tableName+this.HBASESUFFIX + ".receiveTimestamp >=" + this.startTime + " and ";
    	}
    	selectCmd += this.defaultDB+"."+tableName+this.HBASESUFFIX + ".receiveTimestamp <" + this.stopTime ;
    	
    	if(envMode.equals("sensor")){
    		selectCmd += " and parentEntityType = 'EnvironmentSensor'";
    	}else if(envMode.equals("data")){
    		selectCmd += " and parentEntityType = 'EnvironmentData'";
    	}
    	if(tableName.equals("LightGrpCtrl_state") || tableName.equals("Light_state")){
        		selectCmd += " and reliability > 0 ";
    	}
    	String cmd = insertCmd + selectCmd;
		impalaCmd(cmd);
	}
    
    
	/**
	 * 
	 * @param tagMap
	 * @param tagNullMap
	 * @param multiTag 
	 * @param dimEntry
	 * @param entryName
	 * @param city
	 * @throws SQLException
	 * 
	 * sync dimension table data to city db  
	 */
	private void syncDimensionTable(Map tagMap,Map tagNullMap,List<String> multiTag, Entry<String, Object> dimEntry,
			String entryName) throws SQLException {
	
    	String cmd = "";
        HashMap<String, Object> joinObj = new HashMap<String, Object>();
        ArrayList<String> valList = new ArrayList<String>(((Map)dimEntry.getValue()).keySet());
        valList.addAll(tagMap.keySet());
        if(tagNullMap!=null)
        	valList.addAll(tagNullMap.keySet());

        for(String s:multiTag){
        	valList.add(s);
        }
    	String oriTable =this.defaultDB + "." + dimEntry.getKey().substring(this.defaultDB.length()+1).replace(".", "_") + this.LOCATIONSUFFIX;
    	String targetTable = this.dataDB + "." +dimEntry.getKey().substring(this.defaultDB.length()+1).replace(".", "_");
    	cmd += "insert overwrite table " + targetTable + "(";
    	for(int i=0; i<valList.size(); i++){
    		cmd += valList.get(i) + ",";
     	}
        cmd += this.offset + ",";
        if(!valList.contains(this.locationID)){
        	cmd += this.locationID + ",";
        }
        for(String s: this.locList){
        		cmd += s + ",";
        }
     	cmd += "timezone" + " )  select ";
    	for(int i=0; i<valList.size(); i++){
    		cmd +=  valList.get(i) + ",";
     	}
    	cmd += "null,"; 
    	if(!valList.contains(this.locationID)){
    		 cmd +=  this.locationID + ","; 
    	}
     	for(String s: this.locList){
     		cmd +=  s + ",";
     	}   	
    	cmd += "timezone from " + oriTable;

    	this.impalaCmd(cmd);
	}

	/**
	 * 
	 * @param tagMap
	 * @param eventMap
	 * @param entryName
	 * @param city
	 * @param mode
	 * @param dest
	 * @throws SQLException
	 * 
	 * sync factor table to data city db  
	 */
	private void syncFactorTable(Map tagMap, Map eventMap,
			String entryName, String city,String mode) throws SQLException {
    	String cmd = "";
    	String dateTable = city + "." + this.DATEIDHALF;
        HashMap<String, Object> joinObj = new HashMap<String, Object>();
        ArrayList<String> valList = new ArrayList<String>(eventMap.keySet());
        valList.addAll(tagMap.keySet());
        
    	String oriTable =this.defaultDB + "." + entryName.substring(this.defaultDB.length()+1).replace(".", "_") + "_" + mode; 	
    	String periodTable = oriTable + "_period";
    	String targetTable = city + "." +entryName.substring(this.defaultDB.length()+1).replace(".", "_") + "_" + mode;
    	
    	String cmdtmp = "(select ";
    	for(int i=0; i<valList.size(); i++){
    		cmdtmp += periodTable + "." + valList.get(i) + ",";
     	}
    			
    	cmdtmp = cmdtmp.substring(0,cmdtmp.length()-1);
    	for(String s: this.locList){
    		cmdtmp += ","+ this.defaultDB + ".locations." + s;
    	}
    	cmdtmp +=" from "+periodTable
    			+" left join "+this.defaultDB+".locations on  "+periodTable+".locationid="+this.defaultDB+".locations.sid) as T";
    	
    	
    	cmd += "insert into table " + targetTable + "(";
    	for(int i=0; i<valList.size(); i++){
    		cmd += valList.get(i) + ",";
     	}
    	//cmd = cmd.substring(0,cmd.length()-1);
        cmd +="timezoneoffset ) partition (year, month,city) select ";
    	for(int i=0; i<valList.size(); i++){
    		cmd += "T." + valList.get(i) + ",";
     	}

    	cmd += dateTable + ".timezoneoffset," + dateTable +".year," + dateTable +".month,T.city" 
    		+ " from "+ cmdtmp + " left join " + dateTable +" on "
    		+ " T.sampleTimestampid = " + dateTable +".halfutcid "
    		+ " and  T.city = " + dateTable + ".city";
    	String whereCondition = "";

    	if(oriTable.equals("cim.Incident_event")){ 	
    		whereCondition = " where incidenttype in (";
    	    for(String s : snsIncidentType){
    	    	whereCondition += "'"+s+"',";
    	    }
    	    whereCondition = whereCondition.substring(0,whereCondition.length()-1) + ")";
    		this.impalaCmd(cmd + whereCondition);
    		
    		whereCondition = " where incidenttype in (";
    	    for(String s : parkingIncidentType){
    	    	whereCondition += "'"+s+"',";
    	    }
    	    whereCondition = whereCondition.substring(0,whereCondition.length()-1) + ")";
    		this.impalaCmd(cmd.replace("Incident_event(", "parkingincident_event(")+whereCondition);
    		
    		whereCondition = " where incidenttype in (";
    	    for(String s : trafficIncidentType){
    	    	whereCondition += "'"+s+"',";
    	    }
    	    whereCondition = whereCondition.substring(0,whereCondition.length()-1) + ")";
    		this.impalaCmd(cmd.replace("Incident_event(", "trafficincident_event(")+whereCondition);
    		
    		whereCondition = " where incidenttype in (";
    	    for(String s : mobilityIncidentType){
    	    	whereCondition += "'"+s+"',";
    	    }
    	    whereCondition = whereCondition.substring(0,whereCondition.length()-1) + ")";
    		this.impalaCmd(cmd.replace("Incident_event(", "mobilityincident_event(")+whereCondition);
    		
    		whereCondition = " where incidenttype in (";
    	    for(String s : wasteIncidentType){
    	    	whereCondition += "'"+s+"',";
    	    }
    	    whereCondition = whereCondition.substring(0,whereCondition.length()-1) + ")";
    		this.impalaCmd(cmd.replace("Incident_event(", "wasteincident_event(")+whereCondition);
    		
    		whereCondition = " where incidenttype in (";
    	    for(String s : environmentIncidentType){
    	    	whereCondition += "'"+s+"',";
    	    }
    	    whereCondition = whereCondition.substring(0,whereCondition.length()-1) + ")";
    		this.impalaCmd(cmd.replace("Incident_event(", "environmentincident_event(")+whereCondition);
    	}else{
		impalaCmd(cmd);
	}
	}
	
	
	/**
	 * 
	 * @param tagMap
	 * @param tagNullMap
	 * @param multiTags
	 * @param entityName
	 * @param objMap
	 * @param city
	 * @param entity
	 * @param mode			join mode: state_in,update_in,event_in,dim_out,event_out
	 * @throws SQLException
	 * 
	 * construct the variables of the join sql  
	 * 
	 */
	private void joinTables(Map tagMap, Map tagNullMap,Map<String, Object> multiTags, String entityName,
            Map objMap,String city, String entity, String mode) throws SQLException {
		
		
        String objType = mode.split("_")[0].replace(".", "_");
        
        String joinDirection = mode.split("_")[1];
        HashMap<String, Object> joinObj = new HashMap<String, Object>();
        List<String> mainJoinTable = new ArrayList<String>();
        HashMap<String, String> subJoinTableMap = new HashMap<String, String>();
        ArrayList<String> valList = new ArrayList<String>();
        
		Set objSet = objMap.entrySet();
		Iterator objIter = objSet.iterator();  
	    while(objIter.hasNext()){
	                Map.Entry<String, Object> tmpEntry = (Map.Entry<String, Object>)objIter.next();
	                Map valueMap = (Map)tmpEntry.getValue();                 
	                if(objType.equals("update") &&
	                	!valueMap.containsKey("update") && !valueMap.get("family").equals("timestamp")){
	                		valList.add(tmpEntry.getKey());
	                		valList.add(tmpEntry.getKey()+"_before");
	                		if(valueMap.get("type").equals("boolean")){
	                			valList.add(tmpEntry.getKey()+"_delta");
	                		}
	                }else{
	                	valList.add(tmpEntry.getKey());
	                }            
	    }
	    
        ArrayList<String> tagNullList = new ArrayList<String>();
        ArrayList<String> multiTagList = new ArrayList<String>();
        if( tagMap!=null )
        	valList.addAll(tagMap.keySet());
        if(tagNullMap!=null){
        	tagNullList.addAll(tagNullMap.keySet());
        	joinObj.put("tagNullList", tagNullList);
        }
        
        if(joinDirection.equals("out")){
            Collection<Object> tmp = multiTags.values();
            for(int i = 0; i < tmp.size();i++){
                valList.addAll(((Map)tmp.toArray()[i]).keySet());
            }
        }else{
            Collection<Object> tmp = multiTags.values();
            for(int i = 0; i < tmp.size();i++){
                multiTagList.addAll(((Map)tmp.toArray()[i]).keySet());
            }
        }
        if(joinDirection.equals("out")){   
            for (Map.Entry<String, Object> entry : multiTags.entrySet()) {
                Map.Entry<String, Object> tmpEntry = (Entry<String, Object>) ((Map)entry.getValue()).entrySet().toArray()[0];
                if(!objType.equals("event")){ 
                	mainJoinTable.add(city + "." + entry.getKey().substring(this.defaultDB.length()+1));
                	mainJoinTable.add(((Map)tmpEntry.getValue()).get("link")+":" + tmpEntry.getKey());
                }
                else{
			mainJoinTable.add(city + "." + entry.getKey().substring(this.defaultDB.length()+1));
			mainJoinTable.add(((Map)tmpEntry.getValue()).get("link")+":" + tmpEntry.getKey());
                }
            }
        }else{
            if(objType.equals("dim")){
            		;
            }else{
            	mainJoinTable.add(city +"."+ entityName.split("\\.")[1]);
            	if(tagMap != null){
            		mainJoinTable.add("sid:sid");
            	}
            }
        }
       	 
        String finalName = "";
        if (objType.equals("dim")){
        	finalName = city + "." + entityName.split("\\.")[1];	
        	joinObj.put("oriTable", entityName + this.LOCATIONSUFFIX);
            if(this.tableFlags.get(entityName + this.HBASESUFFIX)!=null){
                if(this.tableFlags.get(entityName + this.HBASESUFFIX).equals("0")){
                	return;
                }
            }
            joinObj.put("objType", "dim");
        }else if(objType.equals("state") || objType.equals("event") || objType.equals("update")){
        	finalName = city + "." + entityName.split("\\.")[1] + "_"+ objType;
        	joinObj.put("oriTable", entityName + "_" + objType + this.PERIODSUFFIX);
            joinObj.put("objType", "factor");
        }else{
        	finalName = city + "." + entityName.split("\\.")[1] + "_"+ objType;
        	joinObj.put("oriTable", entityName + "_" + objType + "_hbase");
            joinObj.put("objType", "factor");
        }
        joinObj.put("mode", mode);
        joinObj.put("insertMode", "into");
        joinObj.put("mainJoinTable", mainJoinTable);
        joinObj.put("subJoinTables", subJoinTableMap);
        joinObj.put("targetTable",finalName );
        joinObj.put("targetDB",city);
        joinObj.put("valList",valList );
        if(!joinDirection.equals("out")){
        	joinObj.put("multiTagList",multiTagList );
        }
        joinObj.put("sampletime","sampleTimestamp" );
        joinObj.put("city", this.dataDB);
        
        String cmd = "";
        String cmd2 = "";
        int cmdLength = 1;
        
        
        if(objType.equals("dim")){
            joinObj.put("joinType", " inner join ");
            joinObj.put("partition", true);
            cmd = genJoinCmd(joinObj);
        }else if(objType.startsWith("state")){
        	if(objType.startsWith("state_")){
        		joinObj.put("state_", true);
        	}
            joinObj.put("joinType", " inner join ");
            joinObj.put("partition", true);
            cmd = genJoinCmd(joinObj);
         
        }else if(objType.equals("update")){
        	joinObj.put("insertMode", "into");
            joinObj.put("sampletime","sampleTimestamp");
            joinObj.put("targetTable", finalName );
            joinObj.put("valList", valList);
            joinObj.put("joinType", " left join ");
            joinObj.put("partition", true);
            cmd2 = genJoinCmd(joinObj);
            cmdLength = 2;
        }else if(objType.equals("event")){
        	joinObj.put("insertMode", "into");
        	String targetTable = city + "." + this.DESTPREFIX + entityName.split("\\.")[1] + "_event";
        	joinObj.put("targetTable", targetTable);
            joinObj.put("joinType", " inner join ");
            joinObj.put("partition", true);
            joinObj.put("eventType", "nodest");
        	cmd = genJoinCmd(joinObj);
        	
        	targetTable = city + "." + entityName.split("\\.")[1] + "_event";
        	joinObj.put("targetTable", targetTable);
            joinObj.put("eventType", "dest");
        	cmd2 = genJoinCmd(joinObj);
        	cmdLength = 2;
        }
        if(cmdLength ==1){
        	
        	this.impalaCmd(cmd);
        }else if(cmdLength == 2){
        	this.impalaCmd(cmd);
        	this.impalaCmd(cmd2);
        }
    }

	/**
	 * 
	 * @param 	joinObj
	 * @return	final join sql
	 * 
	 */
    private String genJoinCmd(HashMap<String, Object> joinObj) {
        List<String> valList = (List<String>) joinObj.get("valList");
        List<String> tagNullList = (List<String>) joinObj.get("tagNullList");
        List<String> multiTagList = new ArrayList<String>();
        String city = ((String) joinObj.get("city"));
        
        if(joinObj.containsKey("multiTagList")){
        	multiTagList = (List<String>) joinObj.get("multiTagList");
        }
        
        List<String> mainJoinTable = (List<String>) joinObj.get("mainJoinTable");
        Map<String,Object> subJoinTables = (Map<String, Object>) joinObj.get("subJoinTables");
        String cmd = "insert "+ joinObj.get("insertMode") + " table " + joinObj.get("targetTable") +"( " ;
        if(valList.contains(this.locationID)){
        	valList.remove(this.locationID);
        }
        for(int i=0; i< valList.size();i++){
            cmd += valList.get(i) + ",";
        }
        if(tagNullList!=null){
        	for(int i=0; i< tagNullList.size();i++){
        		cmd += tagNullList.get(i) + ",";
        	}
        }
        if(joinObj.containsKey("multiTagList")){
        	for(int i=0; i< multiTagList.size();i++){
        		cmd += multiTagList.get(i) + ",";
        	}
        }
        cmd += this.locationID ;
        if(((String)joinObj.get("mode")).equals("dim_out")){
        		cmd += "," + this.offset ;
        }else{
        	if ((Boolean) joinObj.get("partition"))
        		cmd += "," + this.offset + ",day" ;
        }
        
        cmd +=	",timezone )";
        if ((Boolean) joinObj.get("partition")){
        	cmd += " partition (year, month,city)";
        }
        cmd += " select ";
        for(int i=0; i< valList.size();i++){
            cmd += joinObj.get("oriTable") + "." + valList.get(i) + ",";
        }
        if(tagNullList!=null){
        	for(int i=0; i< tagNullList.size();i++){
        		cmd +=  "A." + tagNullList.get(i) + ",";
        	}
        }
        if(joinObj.containsKey("multiTagList")){
        	for(int i=0; i< multiTagList.size();i++){
        		cmd +=  "A." + multiTagList.get(i) + ",";
        	}
        }
        for (Entry<String, Object> entry : subJoinTables.entrySet()) {
        	String subKey = ((String)entry.getValue()).split(":")[1];
        	cmd += mainJoinTable.get(0) + "." + subKey.substring(0,subKey.length()-2) + this.dimensionID+", ";
        }
        
        String dateTable = city + "." + this.DATEIDHALF;
        if(((String)joinObj.get("mode")).equals("dim_out")){
        	cmd +=  joinObj.get("oriTable") + "."+this.locationID + ",";
        	cmd += dateTable +"." + this.offset + ", ";
        	cmd += joinObj.get("oriTable") + ".timezone";
        }else{
        	cmd +=  " A."+this.locationID+", ";
        	if ((Boolean) joinObj.get("partition"))
        		cmd += dateTable +"." + this.offset + ", " + dateTable + ".day," ;
        	cmd += "A.timezone" ;
        }
        if ((Boolean) joinObj.get("partition")){
        	cmd += ", " + dateTable + ".year," + dateTable + ".month, A.city "; 
    
        	cmd += " from "+ joinObj.get("oriTable") ;
        }
        else{
        	cmd += " from " + joinObj.get("oriTable");
        }
        
        cmd += joinObj.get("joinType");
        cmd += " (select distinct sid,locationid,timezone,city,tenantid";
        if(tagNullList!=null){
        	for(int i=0; i< tagNullList.size();i++){
        		cmd +=  "," +  tagNullList.get(i) ;
        	}
        }
        if(joinObj.containsKey("multiTagList")){
        	for(int i=0; i< multiTagList.size();i++){
        		cmd +=  "," + multiTagList.get(i) ;
        	}
        }
        
        cmd += " from "+mainJoinTable.get(0) + ") as A on "
        	+ joinObj.get("oriTable") + "." + ((String)mainJoinTable.get(1)).split(":")[1] + "="
        	+  " A." + ((String)mainJoinTable.get(1)).split(":")[0] 
        	+ " and "+joinObj.get("oriTable") + ".tenantid = A.tenantid";
        
        if ((Boolean) joinObj.get("partition")){
        	cmd += " left join " + dateTable + " on "
        	    + joinObj.get("oriTable") + ".sampleTimestampid = " + dateTable + ".halfutcid and A.city = "
        	    + dateTable + ".city";
        }

        if(joinObj.keySet().contains("state_")){
        	cmd += " where "+ joinObj.get("oriTable") + ".receiveTimestamp >= " + this.startTime; 
        	cmd += " and "+ joinObj.get("oriTable") + ".receiveTimestamp <" + this.stopTime;
        }
        if(joinObj.keySet().contains("eventType")){
        	if(joinObj.get("eventType").equals("nodest")){
        		cmd += " where " + joinObj.get("oriTable") + ".destroyTimestamp = 0";
        	}else{
        		cmd += " where " + joinObj.get("oriTable") + ".destroyTimestamp != 0";
        	}
        }
        
        return cmd;    
    }

    /**
     * 
     * @param joinObj
     * @return
     * @throws SQLException
     */
    private String joinLocationCmd(HashMap<String, Object> joinObj) throws SQLException { 	
    	
        String cmd = "";
        String oriTable = (String) joinObj.get("oriTable");
        Boolean isJoinDimension = false;
        String joinSid = "";
        if(oriTable.equals("cim.WasteCollectionRidership_hbase") || oriTable.equals("cim.TransitRidership_hbase")){
        	isJoinDimension = true;
        	if(oriTable.equals("cim.WasteCollectionRidership_hbase")){
        		joinSid	= "wastecollectiontripid";
        	}else{
        		joinSid = "tripid";
        	}
        }
        

        int i = 0;
        List<String> valList = (List<String>) joinObj.get("valList");
        valList.remove("locationId");
        List<String> tagNullList = (List<String>) joinObj.get("tagNullList");
        Map<String,Object> joinTables = (Map<String, Object>) joinObj.get("joinTables");
        cmd += "insert "+joinObj.get("insertMode") + " table " + joinObj.get("targetTable") + "(";
        for(i=0; i< valList.size();i++){
            cmd += valList.get(i) + ",";
        }
        cmd += "locationId, ";
        for(String s: this.locList){
        	cmd += s + ",";
        }
        cmd += "timezone) select ";
        
        for(i=0; i< valList.size();i++){
            cmd += joinObj.get("oriTable") + "." + valList.get(i) + ",";
        }
        if(isJoinDimension){
        	cmd += "A.locationId,";
        }else{
        	cmd += oriTable + ".locationId,";
        }
        for(String s: this.locList){
        	cmd += this.defaultDB + ".locations." + s + ",";
        }
        cmd += this.defaultDB + ".locations.timezone from ";
        cmd += joinObj.get("oriTable") ;
        if(isJoinDimension){
        	cmd += " left join (select distinct sid,locationid,tenantid from "+ oriTable.replace("Ridership", "trip")+") as A"
        		+" on "+oriTable+"." + joinSid + " = A.sid";
        }
        
        cmd += " left join "+ this.defaultDB + ".locations on ";
        if(isJoinDimension){
        	cmd += "A";
        }else{
        	cmd += oriTable;
        }
        cmd += ".locationid=" +  this.defaultDB + ".locations.sid and ";
        if(isJoinDimension){
        	cmd += "A";
        }else{
        	cmd += oriTable;
        }
        cmd += ".tenantid=" +  this.defaultDB + ".locations.tenantid ";
        cmd += "where " + joinObj.get("oriTable") + ".sid is not null";
        
        return cmd;
    }
    
    private void joinLocationTable(Map tagMap, Map tagNullMap, Map<String, Object> multiTags,
            Entry<String, Object> dimEntry, String mode) throws SQLException {
        String entityName = dimEntry.getKey().substring(this.defaultDB.length()+1).replace(".", "_");
        
        HashMap<String, Object> joinObj = new HashMap<String, Object>();
        HashMap<String, String> joinTableMap = new HashMap<String, String>();
        ArrayList<String> valList = new ArrayList<String>(((Map)dimEntry.getValue()).keySet());
        if(mode.equals("main")){
        	Collection<Object> tmp = multiTags.values();
        	for(int i = 0; i < tmp.size();i++){
        		valList.addAll(((Map)tmp.toArray()[i]).keySet());
        	}
        }
        valList.addAll(tagMap.keySet());
        if(tagNullMap!=null)
        	valList.addAll(tagNullMap.keySet());
        
        joinTableMap.put(this.defaultDB + "." + this.locationTable, dimEntry.getKey().split("\\.")[1] + ":" + "sid");
        
        joinObj.put("insertMode", "overwrite");
        joinObj.put("joinTables", joinTableMap );
        joinObj.put("targetTable",this.defaultDB + "." + entityName + this.LOCATIONSUFFIX );
        joinObj.put("oriTable", this.defaultDB + "." + entityName + this.HBASESUFFIX);
        joinObj.put("valList",valList );
        String cmd = joinLocationCmd(joinObj);
        this.impalaCmd(cmd);
    }


    public void createDatabase(String dbname) throws Exception{
        boolean exist = dbm.isDBExist(dbname);
        if(!exist){
            newCityList.add(dbname);
        }
        dbm.executeHiveCmd("create database if not exists " + dbname);
    }
    
    public void dropAllDatabases() throws Exception{
        List<String> dbs = dbm.getDBList();
        for (String db : dbs) {
            if (db.startsWith(defaultDB)) {
                dbm.dropDatabase(db);
            }
        }
    }
    
    public void closeConnections() throws SQLException {
        dbm.closeConnections();
    }

    public void initLocations(Map<String, Map<String, Object>> tables) throws Exception{
        Set genSet = tables.entrySet();         
        Iterator genIter = genSet.iterator();          
        String dbname = ((Map.Entry<String, Object>)tables.entrySet().toArray()[0]).getKey().split("\\.")[0];
        this.createDatabase(this.defaultDB);
        impalaCmd("invalidate metadata");
        
        while(genIter.hasNext()){
            Map.Entry<String, Object> entry=(Map.Entry<String, Object>)genIter.next();    
            if(this.commonEntry.contains(entry.getKey().split("\\.")[1])){
            		Map dimMap =  ((Map)(((Map)entry.getValue()).get("dimension")));
            		Map tagMap =  ((Map)(((Map)entry.getValue()).get("tag")));         
            		Map tagNullMap =  ((Map)(((Map)entry.getValue()).get("tag:null")));         
                    Set dimSet =  ((Map)(((Map)entry.getValue()).get("dimension"))).entrySet();         
                    Iterator dimIter = dimSet.iterator();          
                    String entryName = entry.getKey();
                    while(dimIter.hasNext()){
                            Map.Entry<String, Object> dimEntry=(Map.Entry<String, Object>)dimIter.next(); 
                            this.initDimension(dimEntry,tagMap,tagNullMap,"single");
                    }
            }
        }
    }

    private static String getOffset(Long timeStamp,String timeZone) throws Exception{
  	  Calendar cal= Calendar.getInstance(TimeZone.getTimeZone(timeZone));
        Calendar calutc= Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(new Date(timeStamp));
        calutc.setTime(new Date(timeStamp));
        int hourDelta = (cal.get(Calendar.DAY_OF_YEAR) - calutc.get(Calendar.DAY_OF_YEAR))*24
      		  + cal.get(Calendar.HOUR_OF_DAY) - calutc.get(Calendar.HOUR_OF_DAY);
        return hourDelta*3600*1000+""; 	  
    }
}
    

