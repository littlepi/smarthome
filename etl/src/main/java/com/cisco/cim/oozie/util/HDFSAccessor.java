package com.cisco.cim.oozie.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Properties;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class HDFSAccessor {

    /**
     * Get the HDFS file system
     * @return HDFS file system
     * @throws IOException
     */
    public static FileSystem getFileSystem() throws IOException {
        Configuration aconf = new Configuration();
        String fsURI = aconf.get("fs.default.name");
        return FileSystem.get(URI.create(fsURI), aconf);
    }

    /**
     * Read all the properties from the properties file
     * @param filePath the properties file path in HDFS
     * @return all the Properties
     * @throws Exception
     */
    public Properties readProperties(String filePath) throws Exception {
        FileSystem fs = getFileSystem();
        BufferedReader br = new BufferedReader(new InputStreamReader(
                fs.open(new Path(filePath))));
        Properties props = new Properties();
        props.load(br);
        br.close();
        return props;
    }

    /**
     * Write properties to the properties file
     * @param props the Properties to write
     * @param filePath the properties file path in HDFS
     * @param comments the comments to write
     * @throws Exception
     */
    public void writeProperties(Properties props, String filePath,
            String comments) throws Exception {
        FileSystem fs = getFileSystem();
        FSDataOutputStream out = fs.create(new Path(filePath), true);
        props.store(out, comments);
        out.close();
    }

}
