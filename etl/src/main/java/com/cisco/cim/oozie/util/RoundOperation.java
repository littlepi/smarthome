package com.cisco.cim.oozie.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.security.UserGroupInformation;
import org.json.JSONArray;
import org.json.JSONObject;

public class RoundOperation {
		
	private static final String DataDB = "cimdata";
	private static final String BakDB = "cimbak";
	private RoundOperation() {
	}
	
	public static void round(int year, int month,DBManager dbm) throws Exception{
		DatabaseUtil dbutil = DatabaseUtil.getInstance();
		dbutil.setDBManager(dbm);
		dbutil.createDataBase(BakDB);
		JSONArray tableArray = dbutil.getTableList(DataDB);
		Iterator<Object> it = tableArray.iterator();
		JSONObject partObj = new JSONObject();
		partObj.put("year", year);
		partObj.put("month", month);
		while (it.hasNext()) {	
			JSONObject ob = (JSONObject) it.next();
			String tableName = ob.getString("name");
			System.out.println("TableName:"+tableName);
			String oriTable = DataDB +  "." + tableName ;
			String bakTable = BakDB + "." + tableName;
			ArrayList<String> partitionList = dbutil.getCityPartitionsByYearMonth(DataDB + "." + tableName, year, month);
			if( partitionList==null || partitionList.isEmpty()){
				continue;
			}	
			System.out.println("PartionList:"+partitionList.toString());
			dbutil.dropTable(bakTable);
			dbutil.dupTable(oriTable, bakTable);			
			dbutil.copyDataByPart(oriTable, bakTable, partObj,true);
			for(String city: partitionList){
				dbutil.deletePart(oriTable, new JSONObject(partObj.toString()).put("city", city));
			}	
			dbutil.refresh();
			dbutil.copyDataByPart(bakTable, oriTable, partObj,false);				
		}
		
	}
	
	public static void main(String[] args) throws Exception {
		System.setProperty("java.security.krb5.realm", "CIM.IVSG.AUTH");
		System.setProperty("java.security.krb5.kdc", "cm-hue-01.novalocal");
		Configuration conf = new Configuration();
		conf.set("hadoop.security.authentication", "Kerberos");
		UserGroupInformation.setConfiguration(conf);
		DBManager dbm = new DBManager(
				"jdbc:hive2://cm-hue-01.novalocal:10000/;principal=hive/cm-hue-01.novalocal@CIM.IVSG.AUTH",
				"jdbc:hive2://cm-hue-01.novalocal:21050/;principal=impala/cm-hue-01.novalocal@CIM.IVSG.AUTH",
				"/Users/chao/workspace/etl_230.keytab");
		round(2017,10,dbm);
		
	}
}
