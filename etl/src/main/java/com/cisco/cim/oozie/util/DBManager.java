package com.cisco.cim.oozie.util;

import java.security.PrivilegedExceptionAction;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

import org.apache.hadoop.security.UserGroupInformation;

public class DBManager {
    private static final Logger logger = Logger.getLogger(DBManager.class);
    private static String HIVEDRIVER = "org.apache.hive.jdbc.HiveDriver";
    private static String IMPALADRIVER = "org.apache.hive.jdbc.HiveDriver";
    private String hiveURL;
    private String impalaURL;
    private Connection hiveConn;
    private Connection impalaConn;
    private Statement hiveStmt;
    private Statement impalaStmt;
    private String keytabFile;
    private String tmpdb;
    
    /**
     * Construct a DBManager
     * @param hiveURL the URL to connect to Hive
     * @param impalaURL the URL to connect to Impala
     * @param keytabFile the key table containing principals of Hive and Impala
     */
    public DBManager(String hiveURL, String impalaURL, String keytabFile) {
        this.hiveURL = hiveURL;
        this.impalaURL = impalaURL;
        this.keytabFile = keytabFile;
    }
    
    /**
     * Utility method to parse principal from a Database URL
     * @param dbURL the database URL
     * @return parsed result
     * @throws Exception
     */
    public static String getPrincipalFromURL(String dbURL) throws Exception {
        String p = dbURL.substring(dbURL.indexOf("principal=")+10);
        if (p.length()==0) {
            throw new Exception("Principal is not found in " + dbURL);
        }
        return p;
    }
    
    /**
     * Get both Hive and Impala connections
     * @throws Exception 
     */
    public void getConnections() throws Exception {
        this.getHiveConnection();
        this.getImpalaConnection();
    }
    
    /**
     * Get Hive connection
     * @throws Exception
     */
    public void getHiveConnection() throws Exception {
        if (hiveConn == null || hiveConn.isClosed()) {
            if (!UserGroupInformation.isSecurityEnabled()) {
                throw new Exception("Kerberos is not enabled!");
            }
            UserGroupInformation.loginUserFromKeytab(getPrincipalFromURL(hiveURL), keytabFile);
            this.hiveConn = UserGroupInformation.getLoginUser().doAs(new PrivilegedExceptionAction<Connection>() {
                public Connection run() throws Exception {
                    Class.forName(HIVEDRIVER);
                    Connection hiveConn = DriverManager.getConnection(hiveURL);
                    return hiveConn;
                }
            });
            this.hiveStmt = this.hiveConn.createStatement();
        }
    }
    
    /**
     * Get Impala connection
     * @throws Exception
     */
    public void getImpalaConnection() throws Exception {
        if (impalaConn == null || impalaConn.isClosed()) {
            if (!UserGroupInformation.isSecurityEnabled()) {
                throw new Exception("Kerberos is not enabled!");
            }
            UserGroupInformation.loginUserFromKeytab(getPrincipalFromURL(impalaURL), keytabFile);
            this.impalaConn = UserGroupInformation.getLoginUser().doAs(new PrivilegedExceptionAction<Connection>() {
                public Connection run() throws Exception {
                    Class.forName(IMPALADRIVER);
                    Connection conn = DriverManager.getConnection(impalaURL);
                    return conn;
                }
            });
            this.impalaStmt = this.impalaConn.createStatement();
        }
    }

    /**
     * Execute Hive command
     * @param cmd the command String
     * @throws SQLException
     */
    public void executeHiveCmd(String cmd) throws SQLException {
	logger.info("Execute Hive command: " + cmd);
        this.hiveStmt.execute(cmd);
    }
    
    /**
     * Execute Impala command
     * @param cmd the command String
     * @throws SQLException
     */
    public void executeImpalaCmd(String cmd) throws SQLException {
        logger.info("Execute Impala command: " + cmd);
        this.impalaStmt.execute(cmd);
    }
    
    /**
     * Execute Hive query command
     * @param cmd the command String
     * @return query result
     * @throws SQLException
     */
    public ResultSet executeHiveCmdQuery(String cmd) throws SQLException {
        logger.info("Execute Hive command: " + cmd);
        return this.hiveStmt.executeQuery(cmd);
    }
    
    /**
     * Execute Impala query command
     * @param cmd the command String
     * @return query result
     * @throws SQLException
     */
    public ResultSet executeImpalaCmdQuery(String cmd) throws SQLException {
        logger.info("Execute Impala command: " + cmd);
        return this.impalaStmt.executeQuery(cmd);
    }
    
    /**
     * Close both Hive and Impala connections if they are not closed
     * @throws SQLException
     */
    public void closeConnections() throws SQLException {
        this.closeHiveConnection();
        this.closeImpalaConnection();
    }
    
    /**
     * Close Hive connection
     * @throws SQLException
     */
    public void closeHiveConnection() throws SQLException {
        if ( hiveConn != null && !hiveConn.isClosed()) {
            hiveConn.close();
        }
    }
    
    /**
     * Close Impala connection
     * @throws SQLException
     */
    public void closeImpalaConnection() throws SQLException {
        if ( impalaConn != null && !impalaConn.isClosed()) {
            impalaConn.close();
        }
    }
    
    /**
     * To drop a database
     * @param dbName the database name
     * @throws Exception
     */
    public void dropDatabase(String dbName) throws Exception {
        this.getHiveConnection();
        String cmdDropDB = "drop database if exists " + dbName + " cascade";
        this.executeHiveCmd(cmdDropDB);
    }
    
    /**
     * Get all the database name
     * @return database name list
     * @throws Exception
     */
    public List<String> getDBList() throws Exception {
        this.getHiveConnection();
        String querySQL = "show databases";
        List<String> dbs = new ArrayList<String>();
        ResultSet rs = this.hiveStmt.executeQuery(querySQL);
        while (rs.next()) {
            dbs.add(rs.getString(1));
        }
        return dbs;
    }
    
    /**
     * Check if a table is in a database
     * @param tableName the table name
     * @param dbName the database name
     * @return check result
     * @throws Exception 
     */
    public boolean isTableInDB(String tableName, String dbName) throws Exception {
        boolean exist = false;
        if (!isDBExist(dbName)) {
            return false;
        }
        this.getHiveConnection();
        String querySQL = "show table extended in " + dbName + " like " + tableName;
        ResultSet rs = this.hiveStmt.executeQuery(querySQL);
        if (rs.next()) {
            exist = true;
        }
        return exist;
    }
    
    /**
     * Check if a database exists
     * @param dbName the database name
     * @return check result
     * @throws Exception 
     */
    public boolean isDBExist(String dbName) throws Exception {
        boolean exist = false;
        this.getHiveConnection();
        String querySQL = "show databases";
        List<String> dbs = new ArrayList<String>();
        ResultSet rs = this.hiveStmt.executeQuery(querySQL);
        while (rs.next()) {
            dbs.add(rs.getString(1));
        }
        if (dbs.size() > 0) {
            for(int i=0; i<dbs.size(); i++) {
                if (dbName.equalsIgnoreCase(dbs.get(i))) {
                    exist = true;
                    break;
                }
            }
        }
        return exist;
    }
    
    /**
     * Check if a role exists
     * @param roleName the role name
     * @return check result
     * @throws Exception 
     */
    public boolean isRoleExist(String roleName) throws Exception {
        boolean exist = false;
        this.getHiveConnection();
        String querySQL = "show roles";
        List<String> dbs = new ArrayList<String>();
        ResultSet rs = this.hiveStmt.executeQuery(querySQL);
        while (rs.next()) {
            dbs.add(rs.getString(1));
        }
        if (dbs.size() > 0) {
            for(int i=0; i<dbs.size(); i++) {
                if (roleName.equalsIgnoreCase(dbs.get(i))) {
                    exist = true;
                    break;
                }
            }
        }
        return exist;
    }
    
}
